# This file collects all the startup jobs needed
# for girocco into one place.  It can be placed
# into the /etc/cron.d/ directory and then the
# system restarted.

# Alternatively the individual items could be
# placed into the repo and root per-user crontabs
# or made into a startup script that uses su, but
# it's much simpler just to put them in the global
# crontab so they're all in one place

# For systems that do not support /etc/cron.d and/or
# the @reboot notation, adjustments will need to be
# made such as making use of a startup script like
# /etc/rc.local and/or adding items directly to
# /etc/crontab.  Additionally ionice -c3 may also need
# to be replaced with something like nice -n19 instead
# on systems without ionice.

# IMPORTANT: Be sure to update the users/paths
#            below before using to match Config.pm!

#
## girocco tasks that run as root
#

# The ssh daemon for git ssh access in the chroot jail
# Note that on FreeBSD like systems these two commands (or similar)
# should be run just before the chroot command:
#   /sbin/devfs -m ~repo/j/dev ruleset 4
#   /sbin/devfs -m ~repo/j/dev rule applyset
# Note that sshd is run with -u0 to avoid unnecessary DNS lookups
@reboot		root	cd ~repo/j && /usr/sbin/chroot ~repo/j /sbin/sshd -u0

#
## girocco tasks that run as repo
#

# Update the gitweb project list cache from Girocco gitweb.list file every 2 hours
# Currently only project creation, deletion or owner change updates the gitweb.list file
47 */2 * * *	repo	/usr/bin/ionice -c3 "$HOME/repomgr/jobs/gitwebcache.sh"

# Do a git fetch origin and if any new commits are present on the branch then
# merge them in and if successful run make install (but not as root) every 15 minutes.
# The branch name to use is hard-coded in updateweb.sh and should be updated before
# enabling this item
*/15 * * * *	repo	/usr/bin/ionice -c3 "$HOME/repomgr/jobs/updateweb.sh"

# Kill stale connections every 6 hours
# This should not be necessary now that we're using git-daemon-verify
#0 */6 * * *	repo	"$HOME/repomgr/toolbox/kill-stale-daemons.pl"

# Backup the database files ($chroot/etc/passwd, $chroot/etc/group) once a day.
# Nine rotating backups are kept, run this more often if needed.
# With the `--all` option projects' metadata gets backed up too
19 5 * * *	repo	"$HOME/repomgr/toolbox/backup-db.sh" --all

# Report project disk space usage once a week with an email to admin
23 7 * * 3	repo	"$HOME/repomgr/toolbox/reports/project-disk-use.sh" -m 1000

# Report project fsck status once a week if any errors with an email to admin
23 7 * * 4	repo	"$HOME/repomgr/toolbox/reports/project-fsck-status.sh" -mE

# The job daemon and task daemon are run in a screen
# The ../screen/screenrc file needs to be installed to
# ~repo/.screenrc to support the SCREENCOMMAND functionality.
# Note that some systems may want to add the -U option to screen to
# force it to start up in UTF-8 mode.
@reboot		repo	env SCREENCOMMAND='source "$HOME/repomgr/screen/girocco"' SHELL=/bin/bash /usr/bin/screen -d -m

/*

readlink.c - readlink(2) access from command line
Copyright (C) 2017,2021 Kyle J. McKay.
All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include <stdio.h>
#include <limits.h>
#include <unistd.h>

#define MIN_PATH_MAX ((PATH_MAX) >= 4096 ? (PATH_MAX) : 4096)

static char buf[MIN_PATH_MAX];

/* readlink [-n] filename */
int main(int argc, char **argv)
{
	ssize_t len;
	int opt_n = 0;

	if (argc < 2 || argc > 3)
		return 1;
	if (argc == 3) {
		if (argv[1][0] != '-' || argv[1][1] != 'n' || argv[1][2] != '\0')
			return 1;
		opt_n = 1;
		++argv;
	}
	len = readlink(argv[1], buf, sizeof(buf) - 1);
	if (len <= 0 || len >= (ssize_t)sizeof(buf))
		return 1;
	buf[len] = 0;
	if (opt_n)
		fputs(buf, stdout);
	else
		puts(buf);
	return 0;
}

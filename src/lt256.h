/* License: public domain -or- http://www.wtfpl.net/txt/copying/ */

#ifndef LT256_H
#define LT256_H

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define SHA256_DIGEST_LENGTH 32

typedef struct SHA256_CTX {
    uint64_t length;
    uint32_t state[8], curlen;
    unsigned char buf[64];
    /*const void *data; */
} SHA256_CTX;

/* return value is non-zero/non-NULL on success */
extern int SHA256_Init(SHA256_CTX *c);
extern int SHA256_Update(SHA256_CTX *c, const void *data, size_t len);
extern int SHA256_Final(unsigned char *md, SHA256_CTX *c);
extern unsigned char *SHA256(const void *data, size_t len, unsigned char *md);

#ifdef __cplusplus
}
#endif

#endif /* LT256_H */

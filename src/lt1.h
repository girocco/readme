/* License: public domain -or- http://www.wtfpl.net/txt/copying/ */

#ifndef LT1_H
#define LT1_H

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define SHA1_DIGEST_LENGTH 20

typedef struct SHA1_CTX {
    uint64_t length;
    uint32_t state[5], curlen;
    unsigned char buf[64];
    /*const void *data; */
} SHA1_CTX;

/* return value is non-zero/non-NULL on success */
extern int SHA1_Init(SHA1_CTX *c);
extern int SHA1_Update(SHA1_CTX *c, const void *data, size_t len);
extern int SHA1_Final(unsigned char *md, SHA1_CTX *c);
extern unsigned char *SHA1(const void *data, size_t len, unsigned char *md);

#ifdef __cplusplus
}
#endif

#endif /* LT1_H */

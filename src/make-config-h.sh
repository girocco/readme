#!/bin/sh
set -e
die() { >&2 printf '%s: fatal: %s\n' "${0##*/}" "$*"; exit 1; }
warn() { >&2 printf '%s: %s\n' "${0##*/}" "$*"; }

CC="$1"
: "${CC:=cc}"

[ "${V#*[!0-9]}" = "$V" ] || V=1
V="$(( 0 + ${V:-0} ))"
NOERR='2>&1'
[ $V -le 1 ] || NOERR=

TMPD=
cleanup() {
	[ $V -le 1 ] || [ -z "$TMPD" ] || warn "retaining temp dir $TMPD"
	[ $V -ge 2 ] || [ -z "$TMPD" ] || rm -rf "$TMPD"
}
trap cleanup EXIT
trap 'exit 129' HUP
trap 'exit 130' INT
trap 'exit 131' QUIT
trap 'exit 134' ABRT
trap 'exit 141' PIPE
trap 'exit 142' ALRM
trap 'exit 143' TERM

# safe printf-based version of echo
echol() { printf '%s\n' "$*"; }

# set variable with name $1 to name of new writable temp file with .$2 extension
# if $2 omitted default is "c"
v_get_temp_ext() {
	if [ -z "$TMPD" ]; then
		TMPD="$(mktemp -d /tmp/make-config-h.XXXXXX)" && [ -n "$TMPD" ] && [ -w "$TMPD" ] ||
			die "could not make temp file directory"
	fi
	_tmpf="$(mktemp "$TMPD/source-XXXXXX")" && [ -n "$_tmpf" ] && [ -w "$_tmpf" ] ||
		die "could not make temp file"
	mv "$_tmpf" "$_tmpf.${2:-c}" || die "could not rename temp file to have .${2:-c} extension"
	eval "$1"'="$_tmpf.${2:-c}"'
}

# attempt to compile "C" source file
# on standard input with status as only result
# $1 contains eval'd extra compile args
testcomp() {
	v_get_temp_ext _src
	v_get_temp_ext _obj o
	cat >"$_src"
	eval '"$CC" -c -o "$_obj" $1 "$_src" >/dev/null '"$NOERR"
}

# attempt to compile and link "C" source file
# on standard input with status as only result
# $1 contains eval'd extra compile args
# $2 contains eval'd extra link args
testlink() {
	v_get_temp_ext _src
	v_get_temp_ext _obj o
	v_get_temp_ext _bin bin
	cat >"$_src"
	eval '"$CC" -c -o "$_obj" $1 "$_src" >/dev/null '"$NOERR" &&
	eval '"$CC" -o "$_bin" $2 "$_obj" >/dev/null '"$NOERR"
}

# $1 is base name of macro to use (i.e. HAVE_$1)
# $2 is routine to call (either testcomp or testlink)
# $3... are optional options to $2
# standard input is source to compile or compile+link
emitresult() {
	_macro="$1"
	shift
	[ $V -eq 0 ] || warn "checking for $_macro..."
	if "$@"; then
		[ $V -eq 0 ] || warn "  ...HAVE_$_macro => yes"
		echol "#define HAVE_$_macro 1"
	else
		[ $V -eq 0 ] || warn "  ...HAVE_$_macro => no"
		echol "#undef HAVE_$_macro"
	fi
}

# Check for stdint.h
emitresult STDINT_H testcomp <<-IPV6
	#include <stdint.h>
	extern uint32_t itworks(uint32_t);
IPV6

# Check for IPV6 (see RFC 3493)
emitresult IPV6 testcomp <<-IPV6
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#if defined(IN6ADDR_ANY_INIT) && defined(AF_INET6)
	extern void itworks(void);
	#else
	#error missing IPV6
	#endif
IPV6

# Check for inet_ntop (see RFC 3493)
# this is separate in case IPV6 is present but inet_ntop is not
emitresult INET_NTOP testlink <<-INET_NTOP
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
	#include <stdio.h>
	static void check(void)
	{
		const char *s;
		struct in6_addr addr;
		char str[256];
		s = inet_ntop(AF_INET6, &addr, str, (socklen_t)sizeof(str));
		if (s) puts(s);
	}
	int main() {
		check();
		return 0;
	}
INET_NTOP

# Check for snprintf
emitresult SNPRINTF testlink <<-SNPRINTF
	#include <stdio.h>
	static void check(void)
	{
		char str[256];
		int n = snprintf(str, sizeof(str), "%d", (int)42);
		printf("%n %s", n, str);
	}
	int main() {
		check();
		return 0;
	}
SNPRINTF

emitresult RLIMIT testlink <<-RLIMIT
	#include <limits.h>
	#include <sys/types.h>
	#include <sys/time.h>
	#include <sys/resource.h>
	static void check(void)
	{
		int rc;
		struct rlimit rl;
		rc = getrlimit(RLIMIT_FSIZE, &rl);
		rl.rlim_cur = (rlim_t)LONG_MAX;
		rl.rlim_max = RLIM_INFINITY;
		rc = setrlimit(RLIMIT_FSIZE, &rl);
	}
	int main() {
		check();
		return 0;
	}
RLIMIT

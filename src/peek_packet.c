/*

peek_packet.c -- peek_packet utility to peek at incoming git-daemon request
Copyright (C) 2015,2020 Kyle J. McKay.  All rights reserved.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

/*
  This utility is intended to be used by a script front end to git daemon
  running in inetd mode.  The first thing the script does is call this utility
  which attempts to peek the first incoming Git packet off the connection
  and then output contents after the initial 4-character hex length upto but
  excluding the first \0 character.

  At that point the script can validate the incoming request and if it chooses
  to allow it then exec git daemon to process it.  Since the packet was peeked
  it's still there to be read by git daemon.

  Note that there is a hard-coded timeout of 30 seconds and a hard-coded limit
  of PATH_MAX for the length of the initial packet.

  On failure a non-zero exit code is returned.  On success a 0 exit code is
  returned and peeked text is output to stdout.

  The connection to be peeked must be fd 0 and will have SO_KEEPALIVE set on it.

  This utility does not take any arguments and ignores any that are given.

  The output of a successful peek should be one of these:
  
    git-upload-pack /<...>
    git-upload-archive /<...>
    git-receive-pack /<...>

  where "<...>" is replaced with the repository path so, for example, doing
  "git ls-remote git://example.com/foo.git" would result in this output:
  
    git-upload-pack /foo.git

  Note that the first character could be a ~ instead of a /, but it's
  probably best to reject those.

  The output is guaranteed to not contain any bytes with a value less than
  %x20 except for possibly tab (%x09) characters.  Any request that does
  will produce no output and return a non-zero exit code.

  If the extra optional "host=" parameter is present, then an additional
  second line is output in the format:

    host=<hostname>

  where <hostname> is the host name only from the extra "host=" parameter
  that's been lowercased and had a trailing "." removed (but only if that
  doesn't create the empty string) and had any surrounding '[' and ']' removed
  from a literal IPv6 address.  The <hostname> is guaranteed to only contain
  bytes in the range %x21-%xFF.

  If the extra optional "host=" parameter contains a ":" <portnum> suffix
  then an additional third line will be output of the format:

    port=<portnum>

  If just the ":" was present <portnum> will be the empty string otherwise
  it's guaranteed to be a decimal number with no leading zeros in the range
  1..65535.

  For example, this git command:

    git ls-remote git://example.com/repo.git

  Will result in peek_packet producing these two lines (unless a very, very
  old version of Git was used in which case only the first line):

    git-upload-pack /repo.git
    host=example.com

  This git command:

    git ls-remote git://[::1]:8765/repo.git

  Will result in peek_packet producing these three lines (unless a very, very
  old version of Git was used in which case only the first line):

    git-upload-pack /repo.git
    host=::1
    port=8765

  If the incoming packet looks invalid (or a timeout occurs) then no output
  is produced, but a non-zero exit code is set.

  If the remote address is available (getpeername) then two lines of the form

    remote_addr=<address>
    remote_port=<port>

  will be output where <address> is either a dotted IPv4 or an IPv6
  (without brackets) and <port> is a decimal number with no leading zeros
  in the range 1..65535.

  If the local address is available (getsockname) then two lines of the form

    server_addr=<address>
    server_port=<port>

  will be output where <address> is in the same format as remote_addr and
  <port> is in the same format as remote_port.
*/

/*

;;
;; The Git packet protocol is defined as follows in RFC 5234+7405 syntax
;;

BYTE = %x00-FF

DIGIT = "0" / "1" / "2" / "3" / "4" / "5" / "6" / "7" / "8" / "9"

; Note that hexdigits are case insensitive
HEX-DIGIT = DIGIT / "a" / "b" / "c" / "d" / "e" / "f"

PKT-LENGTH = 4HEX-DIGIT ; represents a hexadecimal big-endian non-negative
                        ; value.  a length of "0002" or "0003" is invalid.
                        ; lengths "0000" and "0001" have special meaning.

PKT-DATA = *BYTE ; first 4 <BYTE>s MUST NOT be %s"ERR "

PKT = FLUSH-PKT / DELIM-PKT / ERR-PKT / PROTOCOL-PKT

FLUSH-PKT = "0000"

DELIM-PKT = "0001"

PROTOCOL-PKT = PKT-LENGTH PKT-DATA ; PKT-DATA must contain exactly
                                   ; PKT-LENGTH - 4 bytes

ERR-PKT = PKT-LENGTH ERR-DATA; ERR-DATA must contain exactly PKT-LENGTH - 4 bytes

ERR-DATA = %s"ERR " *BYTE ; Note that "ERR " *IS* case sensitive

;;
;; The first packet sent by a client connecting to a "Git Transport" server
;; has the <GIT-REQUEST-PKT> format
;;

GIT-REQUEST-PKT = PKT-LENGTH GIT-REQUEST-DATA ; GIT-REQUEST-DATA must contain
                                              ; exactly PKT-LENGTH - 4 bytes

; Normally if %x0A is present it's the final byte (very old Git versions)
; Normally if %x00 is present then %x0A is not (modern Git versions)
; But the current Git versions do parse the %x0A.00 form correctly
GIT-REQUEST-DATA = GIT-COMMAND [EXTRA-ARGS]

GIT-COMMAND = REQUEST-COMMAND %20 PATHNAME [%0A]

; these are all case sensitive
REQUEST-COMMAND = %s"git-upload-pack" /
                  %s"git-receive-pack" /
                  %s"git-upload-archive"

PATHNAME = NON-NULL-BYTES

EXTRA-ARGS = %x00 HOST-ARG-TRUNCATED /
             %x00 [HOST-ARG] [%x00 EXTRA-PARMS]

HOST-ARG-TRUNCATED = HOST-PARAM HOST-NAME [ ":" [PORTNUM] ]

HOST-ARG = HOST-ARG-TRUNCATED %x00

; "host=" is case insensitive
HOST-PARAM = "host="

HOST-NAME = NON-NULL-BYTES ; should be a valid DNS name
                           ; or IPv4 literal
                           ; or "[" IPv6 literal "]"
                           ; a ":" is only allowed between
                           ; the "[" and "]" of an IPv6 literal

; PORTNUM matches 1..65535 with no leading zeros allowed
PORTNUM = ( "1" / "2" / "3" / "4" / "5" ) *4DIGIT /
          "6" /
	  "6" ( "0" / "1" / "2" / "3" / "4" ) *3DIGIT /
          "65" ( "0" / "1" / "2" / "3" / "4" ) *2DIGIT /
          "655" ( "0" / "1" / "2" ) *1DIGIT /
          "6553" ( "0" / "1" / "2" / "3" / "4" / "5" ) /
          "6" ( "6" / "7" / "8" / "9" ) *2DIGIT /
          ( "7" / "8" / "9" ) *3DIGIT

EXTRA-PARAMS = *EXTRA-PARAM [EXTRA-PARAM-TRUNCATED]

EXTRA-PARAM-TRUNCATED = NON-NULL-BYTES

EXTRA-PARAM = EXTRA-PARAM-TRUNCATED %x00

NON-NULL-BYTES = *NON-NULL-BYTE

NON-NULL-BYTE = %x01-FF

*/

#define HAVE_STDINT_H 1
#define HAVE_IPV6 1
#define HAVE_INET_NTOP 1
#define HAVE_SNPRINTF 1
#ifdef CONFIG_H
#include CONFIG_H
#endif

#include <stddef.h>
#ifdef HAVE_STDINT_H
#include <stdint.h>
#endif
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <time.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#ifdef HAVE_IPV6
#include <net/if.h>
#endif
#ifndef HAVE_STDINT_H
typedef unsigned int uint32_t;
typedef unsigned short uint16_t;
#endif

#ifdef HAVE_IPV6
#define IPTOASIZE (INET6_ADDRSTRLEN+IF_NAMESIZE+INET6_ADDRSTRLEN+IF_NAMESIZE+1)
#else
#define IPTOASIZE (15+1) /* IPv4 only */
#endif
static const char *iptoa(const struct sockaddr *ip, char *outstr, size_t s);
static uint16_t xsockport(const struct sockaddr *ip);

/* Note that mod_reqtimeout has a default configuration of 20 seconds
 * maximum to wait for the first byte of the initial request line and
 * then no more than 40 seconds total, but after the first byte is
 * received the rest must arrive at 500 bytes/sec or faster.  That
 * means 10000 bytes minimum in 40 seconds.  We do not allow the
 * initial Git packet to be longer than PATH_MAX (which is typically
 * either 1024 or 4096).  And since 20 + 1024/500 = 22.048 and
 * 20 + 4096/500 = 28.192 using 30 seconds for a total timeout is
 * quite reasonable in comparison to mod_reqtimeout's default conf.
 */

#define TIMEOUT_SECS 30 /* no more than 30 seconds for initial packet */

#define POLL_QUANTUM 100000U /* how often to poll in microseconds */

#if !defined(PATH_MAX) || PATH_MAX+0 < 4096
#define BUFF_MAX 4096
#else
#define BUFF_MAX PATH_MAX
#endif

/* avoid requiring C99 library */
static size_t xstrnlen(const char *s, size_t maxlen)
{
	size_t l = 0;
	if (!s) return l;
	while (l < maxlen && *s) { ++s; ++l; }
	return l;
}

/* avoid requiring C99 library */
#ifdef HAVE_IPV6
static char *xstrncat(char *s1, const char *s2, size_t n)
{
	char *p;
	if (!s1 || !s2 || !n) return s1;
	p = s1 + strlen(s1);
	while (n-- && *s2) { *p++ = *s2++; }
	*p = '\0';
	return s1;
}
#endif /* HAVE_IPV6 */

#define LC(c) (((c)<'A'||(c)>'Z')?(c):((c)+('a'-'A')))

/* returns >0 if m1 and m2 are NOT equal comparing first len bytes
** returns 0 if m1 and m2 ARE equal but ignoring case (POSIX locale)
** essentially the same as the non-existent memcasecmp except that only
** a 0 or >0 result is possible and a >0 result only means not-equal */
static size_t xmemcaseneql(const char *m1, const char *m2, size_t len)
{
	for (; len; --len, ++m1, ++m2) {
		char c1 = *m1;
		char c2 = *m2;
		c1 = LC(c1);
		c2 = LC(c2);
		if (c1 != c2) break;
	}
	return len;
}

static int xdig(char c)
{
	if ('0' <= c && c <= '9')
		return c - '0';
	if ('a' <= c && c <= 'f')
		return c - 'a' + 10;
	if ('A' <= c && c <= 'F')
		return c - 'A' + 10;
	return -1;
}

static char buffer[BUFF_MAX];
static time_t expiry;

/* Ideally we could just use MSG_PEEK + MSG_WAITALL, and that works nicely
 * on BSD-type distros.  Unfortunately very bad things happen on Linux with
 * that combination -- a CPU core runs at 100% until all the data arrives.
 * So instead we omit the MSG_WAITALL and poll every POLL_QUANTUM interval
 * to see if we've satisfied the requested amount yet.
 */
static int recv_peekall(int fd, void *buff, size_t len)
{
	int ans;
	while ((ans = recv(fd, buff, len, MSG_PEEK)) > 0 && (size_t)ans < len) {
		if (time(NULL) > expiry)
			exit(2);
		usleep(POLL_QUANTUM);
	}
	return ans < 0 ? -1 : (int)len;
}

static void handle_sigalrm(int s)
{
	(void)s;
	_exit(2);
}

static void clear_alarm(void)
{
	alarm(0);
}

static int parse_host_and_port(char *ptr, size_t zlen, char **host,
                               size_t *hlen, const char **port, size_t *plen);

static size_t has_controls(const void *_ptr, size_t zlen);

typedef union {
	struct sockaddr sa;
	struct sockaddr_in in;
#ifdef HAVE_IPV6
	struct sockaddr_in6 in6;
#endif
	char padding[128];
} sockaddr_univ_t;

int main(int argc, char *argv[])
{
	int len;
	int xvals[4];
	char hexlen[4];
	size_t pktlen, zlen, gitlen, hlen=0, plen=0;
	char *ptr, *gitcmd, *host=NULL;
	const char *pktend, *port=NULL;
	int optval;
	sockaddr_univ_t sockname;
	socklen_t socknamelen;
	char ipstr[IPTOASIZE];

	(void)argc;
	(void)argv;

	/* Ideally calling recv with MSG_PEEK would never, ever hang.  However
	 * even with MSG_PEEK, recv still waits for at least the first message
	 * to arrive on the socket (unless it's non-blocking).  For this reason
	 * we set an alarm timer at TIMEOUT_SECS + 2 to make sure we don't
	 * remain stuck in the recv call waiting for the first message.
	 */
	signal(SIGALRM, handle_sigalrm);
	alarm(TIMEOUT_SECS + 2); /* Some slop as this shouldn't be needed */
	atexit(clear_alarm);  /* Probably not necessary, but do it anyway */

	expiry = time(NULL) + TIMEOUT_SECS;

	optval = 1;
	if (setsockopt(0, SOL_SOCKET, SO_KEEPALIVE, &optval, sizeof(optval)))
		return 1;

	len = recv_peekall(0, hexlen, 4);
	if (len != 4)
		return 1;

	if ((xvals[0]=xdig(hexlen[0])) < 0 ||
		(xvals[1]=xdig(hexlen[1])) < 0 ||
		(xvals[2]=xdig(hexlen[2])) < 0 ||
		(xvals[3]=xdig(hexlen[3])) < 0)
		return 1;
	pktlen = ((unsigned)xvals[0] << 12) |
		((unsigned)xvals[1] << 8) |
		((unsigned)xvals[2] << 4) |
		(unsigned)xvals[3];
	if (pktlen < 22 || pktlen > sizeof(buffer))
		return 1;

	len = recv_peekall(0, buffer, pktlen);
	if (len != (int)pktlen)
		return 1;

	/* skip over 4-byte <PKT-LENGTH> */
	pktend = buffer + pktlen;
	ptr = buffer + 4;

	/* thanks to check above, pktend - ptr always >= 18 */
	if (memcmp(ptr, "git-", 4)) /* quick sanity check */
		return 1;

	/* validate the entire packet format now */

	/* find length of <GIT-COMMAND> */
	gitlen = xstrnlen(ptr, pktend - ptr);
	/* thanks to the quick sanity check, gitlen always >= 4 */
	gitcmd = ptr;
	/* skip over <GIT-COMMAND> */
	ptr += gitlen + 1; /* not a problem if ptr > pktend */
	if (gitcmd[gitlen-1] == '\n') {
		/* strip trailing \n from <GIT-COMMAND> */
		gitcmd[--gitlen] = '\0';
	}
	if (has_controls(gitcmd, gitlen))
		return 1; /* bad bytes in command */

	/* now comes the optional <HOST-ARG> */
	if (ptr < pktend && (pktend - ptr) >= 5 &&
	    !xmemcaseneql(ptr, "host=", 5)) {
		/* skip over <HOST-PARAM> part */
		ptr += 5;
		zlen = xstrnlen(ptr, pktend - ptr);
		if (!parse_host_and_port(ptr, zlen, &host, &hlen, &port, &plen))
			/* failed to parse rest of <HOST-ARG-TRUNCATED> */
			return 1;
		/* skip over rest of <HOST-ARG>, okay if ptr ends up > pktend */
		ptr += zlen + 1;
	}

	if (ptr < pktend && *ptr)
		return 1; /* invalid, missing required %x00 before <EXTRA-PARMS> */
	++ptr; /* skip over %x00 */

	/* now skip over the rest of the extra args with minimal validation */
	while (ptr < pktend) {
		zlen = xstrnlen(ptr, pktend - ptr);
		/* if (zlen) process_arg(ptr, zlen); */
		ptr += zlen + 1; /* okay if ptr ends up > pktend */
	}

	if (ptr < pktend)
		return 1; /* not a valid <GIT-REQUEST-PKT> */

	printf("%.*s\n", (int)gitlen, gitcmd);
	if (host != NULL)
		printf("host=%.*s\n", (int)hlen, host);
	if (port != NULL)
		printf("port=%.*s\n", (int)plen, port);

	socknamelen = (socklen_t)sizeof(sockname);
	if (!getpeername(0, &sockname.sa, &socknamelen) &&
	    iptoa(&sockname.sa, ipstr, sizeof(ipstr)) && ipstr[0]) {
		uint16_t p = xsockport(&sockname.sa);
		printf("remote_addr=%s\n", ipstr);
		if (p)
			printf("remote_port=%u\n", (unsigned)p);
	}
	socknamelen = (socklen_t)sizeof(sockname);
	if (!getsockname(0, &sockname.sa, &socknamelen) &&
	    iptoa(&sockname.sa, ipstr, sizeof(ipstr)) && ipstr[0]) {
		uint16_t p = xsockport(&sockname.sa);
		printf("server_addr=%s\n", ipstr);
		if (p)
			printf("server_port=%u\n", (unsigned)p);
	}

	return 0;
}

static size_t has_controls_or_spaces(const void *ptr, size_t zlen);

static int parse_host_and_port(char *ptr, size_t zlen, char **host,
                               size_t *hlen, const char **port, size_t *plen)
{
	const char *colon = NULL;
	if (!ptr) return 0; /* bogus ptr argument */
	if (has_controls_or_spaces(ptr, zlen)) return 0; /* bogus host= value */
	if (zlen >= 1 && *ptr == '[') {
		/* IPv6 literal */
		const char *ebrkt = (const char *)memchr(ptr, ']', zlen);
		if (!ebrkt) return 0; /* missing closing ']' */
		*host = ptr + 1;
		*hlen = ebrkt - ptr - 1; /* yes, could be 0 */
		if ((size_t)(++ebrkt - ptr) < zlen) {
			if (*ebrkt != ':') return 0; /* missing ':' after ']' */
			colon = ebrkt;
		}
	} else {
		colon = (const char *)memchr(ptr, ':', zlen);
		*host = ptr;
		*hlen = colon ? ((size_t)(colon - ptr)) : zlen;
		if (*hlen > 1 && ptr[*hlen - 1] == '.')
			--*hlen;
	}
	if (colon) {
		zlen = (ptr + zlen) - ++colon;
		if (zlen > 5) return 0; /* invalid port number */
		if (zlen == 0) {
			/* empty port */
			*port = colon;
			*plen = 0;
		} else {
			unsigned pval;
			const char *pptr;
			size_t pl;
			while (zlen > 1 && *colon == '0') {
				++colon;
				--zlen;
			}
			pptr = colon;
			pl = zlen;
			pval = 0;
			while (zlen) {
				if (*colon < '0' || *colon > '9')
					return 0; /* invalid port number */
				pval *= 10;
				pval += (*colon++) - '0';
				--zlen;
			}
			if (!pval || pval > 65535)
				return 0; /* invalid port number */
			*port = pptr;
			*plen = pl;
		}
	} else {
		*port = NULL;
		*plen = 0;
	}
	ptr = *host;
	zlen = *hlen;
	while (zlen) {
		char c = *ptr;
		c = LC(c);
		*ptr++ = c;
		--zlen;
	}
	return 1;
}

/* the tab character %x09 is not considered a control here */
static size_t has_controls(const void *_ptr, size_t zlen)
{
	const unsigned char *ptr = (const unsigned char *)_ptr;
	if (!ptr) return 0;
	while (zlen && (*ptr >= ' ' || *ptr == '\t')) {
		++ptr;
		--zlen;
	}
	return zlen;
}

static size_t has_controls_or_spaces(const void *_ptr, size_t zlen)
{
	const unsigned char *ptr = (const unsigned char *)_ptr;
	if (!ptr) return 0;
	while (zlen && *ptr > ' ') {
		++ptr;
		--zlen;
	}
	return zlen;
}

static const char *iptoa(const struct sockaddr *ip, char *outstr, size_t s)
{
	if (outstr)
		*outstr = '\0';
	if (ip && outstr && s) {
		if (ip->sa_family == AF_INET) {
			const struct sockaddr_in *sin = (const struct sockaddr_in *)ip;
			inet_ntop(AF_INET, &sin->sin_addr, outstr, (socklen_t)s);
		}
#if defined(HAVE_IPV6) && defined(HAVE_INET_NTOP)
		else if (ip->sa_family == AF_INET6) {
			const struct sockaddr_in6 *sin6 = (const struct sockaddr_in6 *)ip;
			inet_ntop(AF_INET6, sin6->sin6_addr.s6_addr, outstr, (socklen_t)s);
			if (sin6->sin6_scope_id) {
				size_t outlen = strlen(outstr);
#if IF_NAMESIZE < 9
#define XIF_NAMESIZE 9
#else
#define XIF_NAMESIZE IF_NAMESIZE
#endif
				char scope[XIF_NAMESIZE+1];
				char *ifname = if_indextoname(sin6->sin6_scope_id, scope+1);
				if (ifname) {
					scope[0] = '%';
				} else {
					/* This can happen on odd systems */
#ifdef HAVE_SNPRINTF
					snprintf(scope, sizeof(scope), "%%%d", (int)sin6->sin6_scope_id);
#else
					/* 0xFFFFFFFF only requires 9 digits and XIF_NAMESIZE is >= 9
					 * therefore it's guaranteed to fit and not overflow */
					sprintf(scope, "%%%u", (unsigned)(sin6->sin6_scope_id & 0xFFFFFFFF));
#endif
				}
				scope[sizeof(scope)-1] = 0;
				if (outlen+1 < s)
					xstrncat(outstr, scope, s-outlen-1);
			}
		}
#endif /* HAVE_IPV6 */
	}
	return outstr;
}

static uint16_t xsockport(const struct sockaddr *ip)
{
	if (ip->sa_family == AF_INET) {
		const struct sockaddr_in *sin = (const struct sockaddr_in *)ip;
		return (uint16_t)ntohs(sin->sin_port);
	}
#ifdef HAVE_IPV6
	if (ip->sa_family == AF_INET6) {
		const struct sockaddr_in6 *sin6 = (const struct sockaddr_in6 *)ip;
		return (uint16_t)ntohs(sin6->sin6_port);
	}
#endif
	return 0;
}

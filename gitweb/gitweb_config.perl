# Pull Girocco config
use lib "__BASEDIR__";
use Girocco::Config;
use Girocco::Util qw(url_path git_add_config is_readonly);
use Digest::MD5 qw(md5_hex);
BEGIN {
	eval { require HTML::Email::Obfuscate; 1 } or
	eval {
		require Girocco::Email::Obfuscate;
		$INC{'HTML/Email/Obfuscate.pm'} = $INC{'Girocco/Email/Obfuscate.pm'}; # mwahaha
	}
}
BEGIN {
	# Redirect use of the FCGI/CGI::Fast module to the pure Perl substitute
	# unless both the FCGI and CGI::Fast modules are actually present.
	# However, if one or both of FCGI/CGI::Fast has already been use'd don't
	# bother doing anything since it's too late to redirect at that point.
	my $hasinc = sub {
		do {my $f = $_."/".$_[0]; return $f if -f $f}
		foreach grep(!ref($_),@INC);
		return undef;
	};
	my $gd = sub {my $l=1; return sub {$_=$l?"1;":"";$l--}};
	my $pre = sub {
		if ($_[1] eq 'CGI/Fast.pm' && !$INC{'FCGI.pm'} or
		    $_[1] eq 'FCGI.pm' && !$INC{'CGI/Fast.pm'}) {
			if (!&$hasinc('CGI/Fast.pm') || !&$hasinc('FCGI.pm')) {
				require 'Girocco/extra/CGI/FCGI.pm';
				return &$gd;
			}
		}
		return ();
	};
	unshift(@INC, $pre);
}

## For the complete overview of available configuration options,
## see git.git/gitweb/gitweb.perl file beginning (git.git/gitweb/README
## may miss some custom patches, in theory).

# make sure these are available to make this config file "use"able outside
# gitweb.cgi -- everything else in this file already has an explicit "our".
our %feature;
our %html_cache_actions;

# rename detection options for git-diff and git-diff-tree (default is '-M')
our @diff_opts = ('-B', '-C');

# Whether to include project list on the gitweb front page; 0 means yes,
# 1 means no list but show tag cloud if enabled (all projects still need
# to be scanned, unless the info is cached), 2 means no list and no tag cloud
# (very fast)
our $frontpage_no_project_list = 1;

## projects list cache for busy sites with many projects;
## if you set this to non-zero, it will be used as the cached
## index lifetime in minutes
our $projlist_cache_lifetime = 525600; # 1y

## default charset for text/plain blob
our $default_text_plain_charset = 'utf-8';

# Comment out to disable ctags or set to 1 for display only
$feature{'ctags'}{'default'}=["@{[url_path($Girocco::Config::webadmurl)]}/tagproj.cgi"];
#$feature{'ctags'}{'default'}=[1]; # display-only setting (no add tag box on summary page)

$feature{'blame'}{'default'}=[1];
$feature{'blame_incremental'}{'default'}=[1];

$feature{'snapshot'}{'default'} = ['tgz', 'zip'];

# Enable override to turn off snapshots on a per-repository basis with config gitweb.snapshot=none
$feature{'snapshot'}{'override'} = 1;

# Change this from 1 to 0 to disable all searching
# (it's enabled by default so commenting this out will still leave it enabled)
# Note that project-specific override is NOT supported for this option!
$feature{'search'}{'default'}=[1];

# Change this from 0 to 1 to enable git regular expression searches
# (they can be CPU/memory intensive when malicious regular expressions are used)
# (they're enabled by default so commenting this out will leave them enabled)
$feature{'regexp'}{'default'}=[0];

# Enable override to control regexp on a per-repository basis with config gitweb.regexp=<bool>
#$feature{'regexp'}{'override'} = 1;

# Change this from 0 to 1 to enable grep searches (they can be CPU intensive)
# (it's enabled by default so commenting this out will leave it enabled)
# Note that it will be automatically disabled if 'search' is disabled.
$feature{'grep'}{'default'}=[0];

# Enable override to control grep on a per-repository basis with config gitweb.grep=<bool>
#$feature{'grep'}{'override'} = 1;

# Change this from 0 to 1 to enable pickaxe searches (they can be CPU intensive)
# (it's enabled by default so commenting this out will leave it enabled)
# Note that it will be automatically disabled if 'search' is disabled.
$feature{'pickaxe'}{'default'}=[0];

# Enable override to control pickaxe on a per-repository basis with config gitweb.pickaxe=<bool>
#$feature{'pickaxe'}{'override'} = 1;

# Enable this to highlight sources if highlight is available
$feature{'highlight'}{'default'} = [1];

# Set this if highlight is enabled and not available as 'highlight' in $PATH
# You should set this to the full absolute path to highlight whenever highlight
# cannot be found in any of the `getconf PATH` directories.
#our $highlight_bin = "highlight";

# the width (in characters) of the projects list "Description" column
# (the default is only 25 if this is not set)
our $projects_list_description_width = 40;

###
### You probably don't really want to tweak anything below.
###

# Base web full url
our $my_url = $Girocco::Config::gitweburl;

# Base web path absolute url except "/" should be ""
our $my_uri = url_path($Girocco::Config::gitweburl);

# Base web path absolute url
our $base_url = url_path($Girocco::Config::gitweburl,1);

## git base URL used for URL to fetch bundle information page
## i.e. full URL is "$git_base_bundles_url/$project/bundles"
our $git_base_bundles_url = url_path($Girocco::Config::bundlesurl);

# https hint html inserted right after any https push URL (undef for none)
# e.g. "<a href="https_push_instructions.html">https push instructions</a>"
our $https_hint_html = undef;
$https_hint_html = substr(<<HINT,0,-1) if $Girocco::Config::httpspushurl;
<sup class="sup"><span><a href="@{[url_path($Girocco::Config::htmlurl)]}/httpspush.html">(learn&#160;more)</a></span></sup>
HINT

# owner link hook given owner name (full and NOT obfuscated)
# should return full URL-escaped link to attach to owner, for example:
#    sub { return "/showowner.cgi?owner=".CGI::Util::escape($_[0]); }
our $owner_link_hook = undef;
$owner_link_hook = sub { url_path($Girocco::Config::webadmurl)."/projlist.cgi?name=".md5_hex(lc($_[0])); };

# Path to a POSIX shell.  Needed to run $highlight_bin and a snapshot compressor.
# Only used when highlight is enabled or snapshots with compressors are enabled.
our $posix_shell_bin;
$posix_shell_bin = $Girocco::Config::posix_sh_bin if $Girocco::Config::posix_sh_bin;

## core git executable to use
## this can just be "git" if your webserver has a sensible PATH
our $GIT = $Girocco::Config::git_bin;

## path to automatic README.html utility
our $git_automatic_readme_html = $Girocco::Config::basedir . '/bin/run-format-readme.sh';

## absolute fs-path which will be prepended to the project path
our $projectroot = $Girocco::Config::reporoot;

# source of projects list
our $projects_list = $Girocco::Config::projlist_cache_dir."/gitweb.list";

## target of the home link on top of all pages (absolute url)
our $home_link = url_path($Girocco::Config::gitweburl,1);

## string of the home link on top of all pages
our $home_link_str = $Girocco::Config::name;

## name of your site or organization to appear in page titles
## replace this with something more descriptive for clearer bookmarks
our $site_name = $Girocco::Config::title;

## html text to include at home page
our $home_text = "$Girocco::Config::basedir/gitweb/indextext.html";

## read-only version of text, but only when in read-only mode and this exists
## note that when running in a fastcgi mode, changes to read-only mode will
## not show up with regards to this text until the next fastcgi instance
## spawns -- use a "graceful" web server restart to force an immediate effect
our $home_text_ro = "$Girocco::Config::basedir/gitweb/indextext_readonly.html";

## URI of stylesheets
our @stylesheets = ("@{[url_path($Girocco::Config::gitwebfiles)]}/gitweb.css");

## URI of GIT logo (72x27 size)
our $logo = "@{[url_path($Girocco::Config::gitwebfiles)]}/git-logo.png";

## URI of GIT favicon, assumed to be image/png type
our $favicon = "@{[url_path($Girocco::Config::gitwebfiles)]}/git-favicon.png";

## URI of gitweb.js
our $javascript = "@{[url_path($Girocco::Config::gitwebfiles)]}/gitweb.js";

## URL Hints
##
## Any of the urls in @git_base_url_list, @git_base_mirror_urls or
## @git_base_push_urls may be an array ref instead of a scalar in which
## case ${}[0] is the url and ${}[1] is an html fragment "hint" to display
## right after the URL.

## list of git base URLs used for URL for where to fetch project from,
## i.e. full URL is "$git_base_url/$project"
our @git_base_url_list = ();
$Girocco::Config::gitpullurl and push @git_base_url_list, $Girocco::Config::gitpullurl;
$Girocco::Config::httppullurl and push @git_base_url_list, $Girocco::Config::httppullurl;

## For push projects (a .nofetch file exists OR gitweb.showpush is true)
## @git_base_url_list entries are shown as "mirror URL" and @git_base_push_urls
## are shown as "push URL" and @git_base_mirror_urls are ignored.
## For non-push projects, @git_base_url_list and @git_base_mirror_urls are shown
## as "mirror URL" and @git_base_push_urls are ignored.

## list of extra git base URLs used for mirrors for where to fetch project from,
## i.e. full URL is "$git_base_mirror_url/$project"
our @git_base_mirror_urls = ();
$Girocco::Config::httpspushurl && $Girocco::Config::httpspushurl ne ($Girocco::Config::httppullurl||'') and
	push @git_base_mirror_urls, [$Girocco::Config::httpspushurl,
		"<sup class=\"sup\"><span><a href=\"@{[url_path($Girocco::Config::htmlurl)]}/rootcert.html\">(learn&#160;more)</a></span></sup>"];
$Girocco::Config::pushurl && $Girocco::Config::pushurl =~ m|^ssh://|i and
	push @git_base_mirror_urls, substr($Girocco::Config::pushurl, 0, 6) .
		'git@' . substr($Girocco::Config::pushurl, 6);

## list of git base URLs used for URL to push project to,
## i.e. full URL is "$git_base_push_url/$project"
our @git_base_push_urls = ();
$Girocco::Config::pushurl and push @git_base_push_urls, $Girocco::Config::pushurl;
$Girocco::Config::httpspushurl and push @git_base_push_urls, $Girocco::Config::httpspushurl;

our $cache_grpshared = 1;
our $cache_dir = $Girocco::Config::projlist_cache_dir;

our $lastactivity_file = "info/lastactivity";

$html_cache_actions{'summary'} = 1;

our $per_request_config = 0;

our $auto_fcgi = 1;

$feature{'pathinfo'}{'default'}=[1];

$feature{'forks'}{'default'}=[1];

$feature{'actions'}{'default'}=[
	('graphiclog1', "@{[url_path($Girocco::Config::gitwebfiles)]}/git-browser/by-commit.html?r=%n", 'log'),
	('graphiclog2', "@{[url_path($Girocco::Config::gitwebfiles)]}/git-browser/by-date.html?r=%n", 'graphiclog1'),
	('edit', "@{[url_path($Girocco::Config::webadmurl)]}/editproj.cgi?name=%e", 'refs'),
	('fork', "@{[url_path($Girocco::Config::webadmurl)]}/regproj.cgi?fork=%e", 'edit')
];

# Prevent any fast CGI instance from holding on to a possibly now-invalid
# current working directory.  The "root" directory will always be valid (or you
# have bigger problems) even if it's the "root" of a chroot or other kind of
# jail.  gitweb.cgi does not need or require any particular cwd to function and
# as all "use"s and "require"s that may depend on the current working directory
# have already been completed by now it's safe to "cd /" at this point.
chdir "/";

# If in read-only mode and $home_text_ro exists, set $home_text to it
defined($home_text_ro) && $home_text_ro ne "" && is_readonly() && -f $home_text_ro and
	$home_text = $home_text_ro;

# Stuff extra Git configuration options into GIT_CONFIG_PARAMETERS
# This mirrors what shlib.sh does (mostly)
# Only the options that are appropriate for gitweb are included here
git_add_config("core.ignoreCase", "false");
git_add_config("core.pager", "cat");
# see comments in shlib.sh about packedGitWindowSize
if ($Girocco::Config::git_no_mmap) {
	git_add_config("core.packedGitWindowSize", "1m");
} else {
	git_add_config("core.packedGitWindowSize", "32m");
}
git_add_config("core.packedGitLimit", "256m");
# We explicitly set core.bigFileThreshold to only 16m as the
# actual value is computed and is not available here and should
# only affect pack generation.  Since gitweb SHOULD NOT be doing
# anything that would generate any packs, this value should not
# matter.  We set it anyway, just in case.
git_add_config("core.bigFileThreshold", "16m");
git_add_config("gc.auto", "0");

# Since we actually want to display "replaced" commits in
# gitweb just the same as git-browser does, make sure that
# GIT_NO_REPLACE_OBJECTS is NOT set in the environment for gitweb.
delete $ENV{'GIT_NO_REPLACE_OBJECTS'};

# This should not be needed, but just in case, make the last expression true
1;

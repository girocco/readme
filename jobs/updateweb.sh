#!/bin/sh

. @basedir@/shlib.sh

set -e

REPO_DIR="/home/repo/repo"
LOCK_FILE="/tmp/updateweb-$cfg_tmpsuffix.lock"
REMOTE="origin"
BRANCH="rorcz"

# Make sure we don't run twice.
if [ -s "$LOCK_FILE" ] && kill -0 "$(cat "$LOCK_FILE")" 2>/dev/null; then
	echo "Already running updateweb.sh (stuck?) with pid $(cat "$LOCK_FILE")" >&2
	exit 1
fi
trap 'rm -f "$LOCK_FILE"' EXIT
trap 'exit 130' INT
trap 'exit 143' TERM
echo $$ >"$LOCK_FILE"

cd "$REPO_DIR"
git fetch origin
if [ -n "$(git rev-list $BRANCH..$REMOTE/$BRANCH)" ]; then
	case "$(git describe --always --dirty=..dirty)" in *..dirty)
		echo "updateweb.sh: refusing to update because worktree is dirty" >&2
		rm -f "$LOCK_FILE"
		exit 1
	esac
	git merge --ff-only "$REMOTE/$BRANCH" &&
	git submodule update --init --recursive &&
	make -s &&
	make install &&
	/usr/bin/sudo -n /usr/sbin/apache2ctl graceful 2>/dev/null || :
	# would be good to run update-all-projects and clear-all-htmlcache now
	# but they don't really complete quickly enough to be done here
fi

rm -f "$LOCK_FILE"
trap - EXIT INT TERM

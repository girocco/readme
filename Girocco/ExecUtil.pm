# Girocco::ExecUtil.pm -- utility to assist with re-exec'ing oneself
# Copyright (C) 2016 Kyle J. McKay.  All rights reserved.

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, download a copy from
# http://www.gnu.org/licenses/gpl-2.0.html
# or write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

package Girocco::ExecUtil;

use strict;
use warnings;
use Cwd;
use POSIX qw(_exit);
use File::Spec ();

use base qw(Exporter);
use vars qw(@EXPORT @EXPORT_OK $VERSION);

BEGIN {
	@EXPORT = qw(daemon);
	@EXPORT_OK = qw();
	*VERSION = \'1.0';
}

sub new {
	my $class = shift || __PACKAGE__;
	$class = ref($class) if ref($class);
	my $program = shift;
	defined($program) or $program = $0;
	$program = $1 if $program =~ m|^(/.+)$|;
	my $argv0 = shift;
	$argv0 = $1 if defined($argv0) && $argv0 =~ /^(.+)$/;
	defined($argv0) or $argv0 = $program;
	my $self = {};
	%{$self->{ENV}} = %ENV;
	$self->{program} = $program;
	$self->{argv0} = $argv0;
	@{$self->{ARGV}} = ();
	m|^(.*)$|s && push(@{$self->{ARGV}}, $1) foreach @ARGV;
	$self->{cwd} = getcwd;
	-d $self->{cwd} or die __PACKAGE__ . "::new: fatal: unable to getcwd\n";
	$self->{cwd} = $1 if $self->{cwd} =~ m|^(/.*)$|;
	return bless $self, $class;
}

# similar to exec except forks first and then _exit(0)'s on success
# if first arg is CODE ref, call that after successful fork and exec
# if first arg is HASH ref, cleanup member may be CODE ref, samepid boolean
# means reexec in this pid with cleanup in other pid
# cleanup routine receives 2 args, $oldpid, $newpid which will be same if
# samepid option set
# first arg is program second and following are argv[0], argv[1], ...
sub _forkexec {
	my $opts = {};
	if (ref($_[0]) eq 'HASH') {
		$opts = shift;
	} elsif (ref($_[0]) eq 'CODE') {
		$opts = { cleanup => shift };
	}
	my $program = shift;
	my ($read, $write, $read2, $write2);
	my $oldpid = $$;
	my $needfork = !$opts->{samepid} || ref($opts->{cleanup}) eq 'CODE';
	pipe($read, $write) or return undef if $needfork;
	select((select($write),$|=1)[0]) if $needfork;
	my $oldsigchld = $SIG{'CHLD'};
	defined($oldsigchld) or $oldsigchld = sub {};
	if ($needfork && $opts->{samepid} && !pipe($read2, $write2)) {
		local $!;
		close $write;
		close $read;
		return undef;
	}
	select((select($write2),$|=1)[0]) if $needfork && $opts->{samepid};
	$SIG{'CHLD'} = sub {} if $needfork;
	my $retries = 3;
	my $child;
	while ($needfork && !defined($child) && $retries--) {
		$child = fork;
		sleep 1 unless defined($child) || !$retries;
	}
	if ($needfork && !defined($child)) {
		local $!;
		if ($needfork) {
			close $write;
			close $read;
			if ($opts->{samepid}) {
				close $write2;
				close $read2;
			}
		}
		$SIG{'CHLD'} = $oldsigchld;
		return undef;
	}
	if ($needfork && $opts->{samepid}) {
		# child must fork again and the parent get reaped by $$
		if (!$child) {
			close $read2;
			my $retries2 = 3;
			my $child2;
			while (!defined($child2) && $retries2--) {
				$child2 = fork;
				sleep 1 unless defined($child2) || !$retries2;
			}
			if (!defined($child2)) {
				my $ec = 0 + $!;
				$ec = 255 unless $ec;
				print $write2 ":$ec";
				close $write2;
				_exit 127;
			}
			if ($child2) {
				# pass new child pid up to parent and exit
				print $write2 $child2;
				close $write2;
				_exit 0;
			} else {
				# this is the grandchild
				close $write2;
			}
		} else {
			close $write2;
			my $result = <$read2>;
			close $read2;
			chomp $result if defined($result);
			if (!defined($result) || $result !~ /^:?\d+$/) {
				# something's wrong with the child -- kill it
				kill(9, $child) && waitpid($child, 0);
				my $oldsigpipe = $SIG{'PIPE'};
				# make sure the grandchild, if any,
				# doesn't run the success proc
				$SIG{'PIPE'} = sub {};
				print $write 1;
				close $write;
				close $read;
				$SIG{'PIPE'} = defined($oldsigpipe) ?
					$oldsigpipe : 'DEFAULT';
				$! = 255;
				$SIG{'CHLD'} = $oldsigchld;
				return undef;
			}
			if ($result =~ /^:(\d+)$/) {
				# fork failed in child, there is no grandchild
				my $ec = $1;
				waitpid($child, 0);
				close $write;
				close $read;
				$! = $ec;
				$SIG{'CHLD'} = $oldsigchld;
				return undef;
			}
			# reap the child and set $child to grandchild's pid
			waitpid($child, 0);
			$child = $result;
		}
	}
	if (!$opts->{samepid}) {
		if (!$child) {
			# child
			close $read;
			{ exec({$program} @_) };
			my $ec = 0 + $!;
			$ec = 255 unless $ec;
			print $write $ec;
			close $write;
			_exit 127;
		}
		close $write;
		my $result = <$read>;
		close $read;
		chomp $result if defined($result);
		waitpid($child, 0);
		if (defined($result) && $result != 0) {
			$! = $result;
			$SIG{'CHLD'} = $oldsigchld;
			return undef;
		}
		&{$opts->{cleanup}}($oldpid, $child)
			if ref($opts->{cleanup}) eq 'CODE';
		_exit 0;
	} else {
		if ($needfork && !$child) {
			# grandchild
			close $write;
			my $result = <$read>;
			close $read;
			chomp $result if defined($result);
			_exit 127 if $result && $result != 0;
			&{$opts->{cleanup}}($oldpid, $oldpid);
			_exit 0;
		}
		close $read if $needfork;
		my $result;
		{ $result = exec({$program} @_) };
		my $ec = 0 + $!;
		$ec = 255 unless $ec;
		print $write $ec if $needfork;
		close $write if $needfork;
		$SIG{'CHLD'} = $oldsigchld;
		return $result;
	}
}

sub reexec {
	my $self = shift;
	my $opts = {};
	$opts->{cleanup} = shift if ref($_[0]) eq 'CODE';
	$opts->{samepid} = shift;
	ref($self->{ENV}) eq 'HASH' &&
	defined($self->{program}) &&
	defined($self->{argv0}) &&
	ref($self->{ARGV}) eq 'ARRAY' &&
	defined($self->{cwd}) or die __PACKAGE__ . "::reexec: fatal: invalid instance\n";
	my %envsave = %ENV;
	my $cwdsave = eval { no warnings; getcwd; };
	$cwdsave = $1 if defined($cwdsave) && $cwdsave =~ m|^(/.*)$|;
	my $result = chdir($self->{cwd});
	return $result unless $result;
	%ENV = %{$self->{ENV}};
	$result = _forkexec($opts, $self->{program}, $self->{argv0}, @{$self->{ARGV}});
	%ENV = %envsave;
	chdir($cwdsave) if defined($cwdsave);
	return $result;
}

sub getenv {
	my $self = shift;
	my $result = undef;
	if (exists($self->{ENV}->{$_[0]})) {
		$result = $self->{ENV}->{$_[0]};
		defined($result) or $result = "";
	}
	$result;
}

sub setenv {
	my $self = shift;
	my ($k, $v) = @_;
	if (defined($v)) {
		$self->{ENV}->{$k} = $v;
	} else {
		delete $self->{ENV}->{$k};
	}
}

sub delenv {
	my $self = shift;
	$self->setenv($_[0], undef);
}

sub daemon {
	use POSIX qw(_exit setpgid setsid dup2 :fcntl_h);
	my ($nochdir, $noclose) = @_;
	my $devnull = File::Spec->devnull unless $noclose;
	my $oldsigchld = $SIG{'CHLD'};
	defined($oldsigchld) or $oldsigchld = sub {};
	my ($read, $write, $read2, $write2);
	pipe($read, $write) or return 0;
	select((select($write),$|=1)[0]);
	if (!pipe($read2, $write2)) {
		local $!;
		close $write;
		close $read;
		return 0;
	}
	select((select($write2),$|=1)[0]);
	$SIG{'CHLD'} = sub {};
	my $retries = 3;
	my $child;
	while (!defined($child) && $retries--) {
		$child = fork;
		sleep 1 unless defined($child) || !$retries;
	}
	if (!defined($child)) {
		local $!;
		close $write2;
		close $read2;
		close $write;
		close $read;
		$SIG{'CHLD'} = $oldsigchld;
		return 0;
	}
	# double fork the child
	if (!$child) {
		close $read2;
		my $retries2 = 3;
		my $child2;
		while (!defined($child2) && $retries2--) {
			$child2 = fork;
			sleep 1 unless defined($child2) || !$retries2;
		}
		if (!defined($child2)) {
			my $ec = 0 + $!;
			$ec = 255 unless $ec;
			print $write2 ":$ec";
			close $write2;
			_exit 127;
		}
		if ($child2) {
			# pass new child pid up to parent and exit
			print $write2 $child2;
			close $write2;
			_exit 0;
		} else {
			# this is the grandchild
			close $write2;
		}
	} else {
		close $write2;
		my $result = <$read2>;
		close $read2;
		chomp $result if defined($result);
		if (!defined($result) || $result !~ /^:?\d+$/) {
			# something's wrong with the child -- kill it
			kill(9, $child) && waitpid($child, 0);
			$! = 255;
			$SIG{'CHLD'} = $oldsigchld;
			return 0;
		}
		if ($result =~ /^:(\d+)$/) {
			# fork failed in child, there is no grandchild
			my $ec = $1;
			waitpid($child, 0);
			close $write;
			close $read;
			$! = $ec;
			$SIG{'CHLD'} = $oldsigchld;
			return 0;
		}
		# reap the child and set $child to grandchild's pid
		waitpid($child, 0);
		$child = $result;
	}
	if (!$child) {
		# grandchild that actually becomes the daemon
		close $read;
		my $exitfail = sub {
			my $ec = 0 + $!;
			print $write $ec;
			close $write;
			_exit 255;
		};
		&$exitfail unless ($nochdir || chdir("/"));
		unless ($noclose) {
			my ($ufd, $wrfd);
			defined($ufd = POSIX::open($devnull, O_RDWR)) or &$exitfail;
			defined(dup2($ufd, 0)) or &$exitfail unless $ufd == 0;
			defined(dup2($ufd, 1)) or &$exitfail unless $ufd == 1;
			defined(dup2($ufd, 2)) or &$exitfail unless $ufd == 2;
			POSIX::close($ufd) or &$exitfail unless $ufd == 0 || $ufd == 1 || $ufd == 2;
		}
		&$exitfail unless POSIX::setsid || $$ == POSIX::getpgrp;
		&$exitfail unless POSIX::setpgid(0, $$) || $$ == POSIX::getpgrp;
		close $write;
		$SIG{'CHLD'} = $oldsigchld;
		return 1; # success we are now the daemon
	}
	close $write;
	my $result = <$read>;
	close $read;
	chomp $result if defined $result;
	$SIG{'CHLD'} = $oldsigchld;
	_exit(0) unless $result;
	$! = $result;
	return 0; # daemon attempt failed
}

1;

__END__

=head1 NAME

Girocco::ExecUtil - Re-execution utility

=head1 SYNOPSIS

    use Girocco::ExecUtil;
    
    my $exec_state = Girocco::ExecUtil->new;
    daemon or die "daemon failed: $!";
    # do some stuff and run for a while
    $exec_state->reexec;

=head1 DESCRIPTION

This module provides a re-exec function for long-running processes that
may want to re-start themselves at a later point in time if they have been
updated.  As a convenience, it also includes a daemon function to assist with
running processes in the background,

The C<Girocco::ExecUtil> instance records various information about the current
state of the process when it's called (C<@ARGV>, C<%ENV>, current working directory,
C<$0> process name) for subsequent use by the C<reexec> function.

When the C<reexec> function is called, it restores C<%ENV> and the current working
directory to the previously saved state and then C<exec>'s the previously saved
C<$0> using the previously saved C<@ARGV> in a new process and C<_exit>'s the
old one.

The following functions are provided:

=over 4

=item daemon(I<nochdir>, I<noclose>)

Attempt to become a background daemon by double-forking, redirecting STDIN,
STDOUT and STDERR to C</dev/null>, doing C<chdir> to C</> and then calling
setsid and setpgid.  Returns true on success in which case the original process
has been C<_exit(0)>'d.  Otherwise C<$!> contains the failure and STDIN,
STDOUT, STDERR and the cwd are unchanged on return.

If I<nochdir> is true then the C<chdir> is skipped.  If I<noclose>
is true then STDIN, STDOUT and STDERR are left unchanged.  Note that when
STDIN, STDOUT and STDERR are redirected (i.e. I<noclose> is false), it is the
underlying file handles 0, 1 and 2 that are modified -- if the Perl filehandles
are pointing somewhere else (such as being C<tied>) they will be unaffected.

=item Girocco::ExecUtil->new

=item Girocco::ExecUtil->new(I<program>)

=item Girocco::ExecUtil->new(I<program>, I<argv0>)

Create and return a new instance recording the current environment (C<%ENV>),
program (C<$0>), arguments (C<@ARGV>) and working directory (C<getcwd>) for
later use by the reexec function.  If I<program> is passed it is used in place
of C<$0> for both the program to execute and C<argv[0]>.  If I<program> and
I<argv0> are passed then I<program> will be executed but passed I<argv0> as its
C<argv[0]>.

=item $instance->reexec(I<samepid>)

=item $instance->reexec(I<coderef>, I<samepid>)

Restore the saved environment and current working directory recorded in
the instance and then fork and exec the program and arguments recorded in the
instance and if successful call I<coderef>, if provided, and then _exit(0).

Only returns if the chdir, fork or exec call fails (in which case I<coderef> is
NOT called and the current working directory may have been restored to its
saved value).

Note that I<coderef>, if provided, is called B<after> the successful fork and
exec so the newly exec'd process may already be running by then (I<coderef> is
B<never> called if the C<reexec> fails).  I<coderef> receives two arguments,
the first is the old pid (the one calling C<reexec>) and the second is the
new pid (the one the C<exec> call runs in).

If the I<samepid> argument is omitted or is false (recommended) all behaves as
described above.   However, if I<samepid> is true then the C<exec> call runs
from the current pid and I<coderef>, if present, is called (with both arguments
the same) from a double C<fork>'d process to avoid a spurious C<SIGCHLD> (but
still I<only> if the C<reexec> succeeds) otherwise (I<samepid> is true but no
I<coderef> is present) no fork occurs at all, just the C<exec>.  Re-exec'ing
oneself (i.e. keeping the same pid) may result in difficult to diagnose
failures on systems where some kinds of initialization can only be performed
once for any given pid during its lifetime which is why setting I<samepid> to
true is not recommended.

=item $instance->getenv(I<name>)

Return the value of environment variable I<name> saved in the instance or
C<undef> if it's not part of the saved environment.  If the environment
variable I<name> is present in the saved environment then C<undef> will
B<never> be returned.

=item $instance->setenv(I<name>, I<value>)

=item $instance->setenv(I<name>, undef)

=item $instance->delenv(I<name>)

Set the value of environment variable I<name> saved in the instance.  If
I<value> is C<undef> or omitted or C<delenv> used then remove the environment
variable named I<name> from the environment saved in the instance -- it is
no error to remove an environment variable that is not present in the instance.

=back

=head1 COPYRIGHT

Copyright (C) 2016 Kyle J. McKay.  All rights reserved.

License GPLv2+: GNU General Public License version 2 or later.
See comments at top of source file for details.

L<http://www.gnu.org/licenses/gpl-2.0.html>

This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

=cut

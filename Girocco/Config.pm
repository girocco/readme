package Girocco::Config;

use strict;
use warnings;


#
##  --------------
##  Basic settings
##  --------------
#

# Name of the service (typically a single word or a domain name)
# (no spaces allowed)
our $name = "repo.or.cz";

# Nickname of the service (undef for initial part of $name upto first '.')
# (no spaces allowed)
our $nickname = "rorcz";

# Title of the service (as shown in gitweb)
# (may contain spaces)
our $title = "Public Git Hosting";

# Path to the Git binary to use (you MUST set this, even if to /usr/bin/git!)
our $git_bin = '/home/repo/bin/git';

# Path to the git-daemon binary to use (undef to use default)
# If $gitpullurl is undef this will never be used (assuming no git inetd
# service has been set up in that case).
# The default if this is undef is `$git_bin --exec-path`/git-daemon
our $git_daemon_bin = undef;

# Path to the git-http-backend binary to use (undef to use default)
# If both $httppullurl and $httpspushurl are undef this will never be used
# The default if this is undef is `$git_bin --exec-path`/git-http-backend
our $git_http_backend_bin = undef;

# Name (if in $PATH) or full path to netcat executable that accepts a -U option
# to connect to a unix socket.  This may simply be 'nc' on many systems.
# See the ../src/dragonfly/README file for a DragonFly BSD nc with -U support.
# For a Linux-like system try installing the 'netcat-openbsd' package.
our $nc_openbsd_bin = 'nc.openbsd';

# Path to POSIX sh executable to use.  Set to undef to use /bin/sh
our $posix_sh_bin = undef;

# Path to Perl executable to use.  Set to undef to use Perl found in $PATH
our $perl_bin = undef;

# Path to gzip executable to use.  Set to undef to use gzip found in $PATH
our $gzip_bin = undef;

# Path to OpenSSL/LibreSSL executable to use.
# Set to undef to use openssl found in $PATH
# Not used unless $httpspushurl is defined
our $openssl_bin = undef;

# Path to the sendmail instance to use.  It should understand the -f <from>, -i and -t
# options as well as accepting a list of recipient addresses in order to be used here.
# You MUST set this, even if to '/usr/sbin/sendmail'!
# Setting this to 'sendmail.pl' is special and will automatically be expanded to
# a full path to the ../bin/sendmail.pl executable in this Girocco installation.
# sendmail.pl is a sendmail-compatible script that delivers the message directly
# using SMTP to a mail relay host.  This is the recommended configuration as it
# minimizes the information exposed to recipients (no sender account names or uids),
# can talk to an SMTP server on another host (eliminating the need for a working
# sendmail and/or SMTP server on this host) and avoids any unwanted address rewriting.
# By default it expects the mail relay to be listening on localhost port 25.
# See the sendmail.pl section below for more information on configuring sendmail.pl.
our $sendmail_bin = 'sendmail.pl';

# E-mail of the site admin
our $admin = 'admin@repo.or.cz';

# Sender of emails
# This is the SMTP 'MAIL FROM:' value
# It will be passed to $sendmail_bin with the -f option
# Some sites may not allow non-privileged users to pass the -f option to
# $sendmail_bin.  In that case set this to undef and no -f option will be
# passed which means the 'MAIL FROM:' value will be the user the mail is
# sent as (either $cgi_user or $mirror_user depending on the activity).
# To avoid having bounce emails go to $admin, this may be set to something
# else such as 'admin-noreply@example.org' and then the 'admin-noreply' address
# may be redirected to /dev/null.  Setting this to '' or '<>' is not
# recommended because that will likely cause the emails to be marked as SPAM
# by the receiver's SPAM filter.  If $sendmail_bin is set to 'sendmail.pl' this
# value must be acceptable to the receiving SMTP server as a 'MAIL FROM:' value.
# If this is set to undef and 'sendmail.pl' is used, the 'MAIL FROM:' value will
# be the user the mail is sent as (either $cgi_user or $mirror_user).
our $sender = 'admin-noreply@repo.or.cz';

# Copy $admin on failure/recovery messages?
our $admincc = 0;

# Girocco branch to use for html.cgi view source links (undef for HEAD)
our $giroccobranch = 'rorcz';

# PATH adjustments
# If the PATH needs to be customized to find required executables on
# the system, it can be done here.
# IMPORTANT: If PATH is NOT set here,
#            it *will* be set to `/usr/bin/getconf PATH`!
# To keep whatever PATH is in effect when Girocco is installed use:
#$ENV{PATH} = $ENV{PATH};
# To add /usr/local/bin to the standard PATH, use something like this:
#use Girocco::Dumper qw(GetConfPath);
#$ENV{PATH} = GetConfPath().":/usr/local/bin";


#
##  ----------------------
##  Git user agent strings
##  ----------------------
#

# Git clients (i.e. fetch/clone) always send a user agent string when fetching
# over HTTP.  Since version 1.7.12.1 an 'agent=' capability string is included
# as well which affects git:, smart HTTP and ssh: protocols.

# These settings allow the default user agent string to be changed independently
# for fetch/clone operations (only matters if $mirror is true) and server
# operations (some other Git client fetching from us).  Note that it is not
# possible to suppress the capability entirely although it can be set to an
# empty string.  If these values are not set, the default user agent string
# will be used.  Typically (unless Git was built with non-standard options) the
# default is "git/" plus the version.  So for example "git/1.8.5.6" or
# "git/2.1.4" might be seen.

# One might want to change the default user agent strings in order to prevent
# an attacker from learning the exact Git version being used to avoid being
# able to quickly target any version-specific vulnerabilities.  Note that
# no matter what's set here, an attacker can easily determine whether a server
# is running JGit, libgit2 or Git and for Git whether it's version 1.7.12.1 or
# later.  A reasonable value to hide the exact Git version number while
# remaining compatible with servers that require a "Git/" user agent string
# would be something like "git/2" or even just "git/".

# The GIT_USER_AGENT value to use when acting as a client (i.e. clone/fetch)
# This value is only used if $mirror is true and at least one mirror is set up.
# Setting this to the empty string will suppress the HTTP User-Agent header,
# but will still include an "agent=" capability in the packet protocol.  The
# empty string is not recommended because some servers match on "git/".
# Leave undef to use the default Git user agent string
# IMPORTANT: some server sites will refuse to serve up Git repositories unless
# the client user agent string contains "Git/" (matched case insensitively)!
our $git_client_ua = undef;

# The GIT_USER_AGENT value to use when acting as a server (i.e. some Git client
# is fetching/cloning from us).
# Leave undef to use the default Git user agent string
our $git_server_ua = undef;


#
##  -------------
##  Feature knobs
##  -------------
#

# Enable mirroring mode if true (see "Foreign VCS mirrors" section below) 
our $mirror = 1;

# Enable push mode if true
our $push = 1;

# If both $mirror and $push are enabled, setting this to 'mirror' pre-selects
# mirror mode on the initial regproj display, otherwise 'push' mode will be
# pre-selected.  When forking the initial mode will be 'push' if $push enabled.
our $initial_regproj_mode = 'mirror';

# Enable user management if true; this means the interface for registering
# user accounts and uploading SSH keys. This implies full chroot.
our $manage_users = 1;

# Minimum key length (in bits) for uploaded SSH RSA/DSA keys.
# If this is not set (i.e. undef) keys as small as 512 bits will be allowed.
# Nowadays keys less than 2048 bits in length should probably not be allowed.
# Note, however, that versions of OpenSSH starting with 4.3p1 will only generate
# DSA keys of exactly 1024 bits in length even though that length is no longer
# recommended.  (OpenSSL can be used to generate DSA keys with lengths > 1024.)
# OpenSSH does not have any problem generating RSA keys longer than 1024 bits.
# This setting is only checked when new keys are added so setting it/increasing it
# will not affect existing keys.  For maximum compatibility a value of 1024 may
# be used however 2048 is recommended.  Setting it to anything other than 1024,
# 2048 or 3072 may have the side effect of making it very difficult to generate
# DSA keys that satisfy the restriction (but RSA keys should not be a problem).
# Note that no matter what setting is specified here keys smaller than 512 bits
# will never be allowed via the reguser.cgi/edituser.cgi interface.
# RECOMMENDED VALUE: 2048 (ok) or 3072 (better)
our $min_key_length = 2048;

# Disable DSA public keys?
#
# If this is set to 1, adding DSA keys at reguser.cgi/edituser.cgi time will be
# prohibited.  If $pushurl is undef then this is implicitly set to 1 since DSA
# keys are not usable with https push.
#
# OpenSSH will only generate 1024 bit DSA keys starting with version 4.3p1.
# Even if OpenSSL is used to generate a longer DSA key (which can then be used
# with OpenSSH), the SSH protocol itself still forces use of SHA-1 in the DSA
# signature blob which tends to defeat the purpose of going to a longer key in
# the first place.  So it may be better from a security standpoint to simply
# disable DSA keys especially if $min_key_length and $rsakeylength have been set
# to something higher such as 3072 or 4096.
#
# This setting is only checked when new keys are added so changing it will not
# affect existing keys.  There is no way to disable DSA keys in the sshd_config
# file of older versions of the OpenSSH server, but newer versions of OpenSSH
# WILL DISABLE DSA KEYS BY DEFAULT!
#
# IMPORTANT: If you do enable DSA keys ($disable_dsa is set to 0) and you are
#            using a more recent version of the OpenSSH server software in the
#            chroot jail, you MUST manually ADD the following line
#            (the "+" IS REQUIRED) to the $chroot/j/etc/ssh/sshd_config file
#            otherwise dsa keys WILL NOT BE ACCEPTED!
#
#            PubkeyAcceptedKeyTypes +ssh-dss
#
# If this is set to 1, no ssh_host_dsa_key will be generated or used with the
# sshd running in the jail (but if the sshd_config has already been generated
# in the jail, it must be removed and 'sudo make install' run again or otherwise
# the sshd_config needs to be edited by hand for the change to take effect).
#
# RECOMMENDED VALUE: 1
our $disable_dsa = 1;

# Enable the special 'mob' user if set to 'mob'
our $mob = "mob";

# Let users set admin passwords; if false, all password inputs are assumed empty.
# This will make new projects use empty passwords and all operations on them
# unrestricted, but you will be able to do no operations on previously created
# projects you have set a password on.
our $project_passwords = 1;

# How to determine project owner; 'email' adds a form item asking for their
# email contact, 'source' takes realname of owner of source repository if it
# is a local path (and empty string otherwise). 'source' is suitable in case
# the site operates only as mirror of purely local-filesystem repositories.
our $project_owners = 'email';

# Which project fields to make editable, out of 'shortdesc', 'homepage', 'README',
# 'cleanmirror', 'notifymail', 'reverseorder', 'summaryonly', 'notifytag' and 'notifyjson'
# 'notifycia' was used by the now defunct CIA service and while allowing it to
# be edited does work and the value is saved, the value is totally ignored by Girocco
our @project_fields = qw(cleanmirror homepage shortdesc README notifymail reverseorder summaryonly notifytag notifyjson);

# Which project fields to protect -- they will first require the project
# password to be entered before they can even be viewed on the editproj page
our $protect_fields = {map({$_ => 1} qw(notifymail notifytag notifyjson))};

# Registration/Edit expiration time
# The registration form must be completed within this amount of time
# or it will time out and require starting over.  The project edit page
# must be submitted within this amount of time or it will time out and
# require starting over.
our $project_edit_timeout = 1800; # 30 minutes

# Minimal number of seconds to pass between two updates of a project.
our $min_mirror_interval = 3600; # 1 hour

# Minimal number of seconds to pass between two garbage collections of a project.
our $min_gc_interval = 604800; # 1 week

# Minimal number of seconds to pass after first failure before sending failure email.
# A mirror update failed message will not be sent until mirror updates have been
# failing for at least this long.  Set to 0 to send a failure message right away
# (provided the $min_mirror_failure_message_count condition has been met).
our $min_mirror_failure_message_interval = 345600; # 4 days

# Minimal number of consecutive failures required before sending failure email.
# A mirror update failed message will not be sent until mirror updates have failed
# for this many consecutive updates.  Set to 0 to send a failure message right away
# (provided the $min_mirror_failure_message_interval condition has been met).
our $min_mirror_failure_message_count = 10;

# Maximum window memory size when repacking.  If this is set, it will be used
# instead of the automatically computed value if it's less than that value.
# May use a 'k', 'm', or 'g' suffix otherwise value is in bytes.
our $max_gc_window_memory_size = undef;

# Maximum big file threshold size when repacking.  If this is set, it will be
# used instead of the automatically computed value if it's less than that value.
# May use a 'k', 'm', or 'g' suffix otherwise value is in bytes.
our $max_gc_big_file_threshold_size = undef;

# Whether or not to run the ../bin/update-pwd-db script whenever the etc/passwd
# database is changed.  This is typically needed (i.e. set to a true value) for
# FreeBSD style systems when using an sshd chroot jail for push access.  So if
# $pushurl is undef or the system Girocco is running on is not like FreeBSD
# (e.g. a master.passwd file that must be transformed into pwd.db and spwd.db), then
# this setting should normally be left false (i.e. 0).  See comments in the
# provided ../bin/update-pwd-db script about when and how it's invoked.
our $update_pwd_db = 0;

# Port the sshd running in the jail should listen on
# Be sure to update $pushurl to match
# Not used if $pushurl is undef
our $sshd_jail_port = 22;

# If this is true then host names used in mirror source URLs will be checked
# and any that are not DNS names (i.e. IPv4 or IPv6) or match one of the DNS
# host names in any of the URL settings below will be rejected.
our $restrict_mirror_hosts = 1;

# If $restrict_mirror_hosts is enabled this is the minimum number of labels
# required in a valid dns name.  Normally 2 is the correct value, but if
# Girocco is being used internally where a common default or search domain
# is set for everyone then this should be changed to 1 to allow a dns name
# with a single label in it.  No matter what is set here at least 1 label
# is always required when $restrict_mirror_hosts is enabled.
our $min_dns_labels = 2;

# If defined, pass this value to format-readme as its `-m` option
# When format-readme is formatting an automatic readme, it will skip
# anything larger than this.  The default is 32768 if unset.
# See `bin/format-readme -h` for details.
our $max_readme_size = 350000;

# Maximum size of any single email sent by mail.sh in K (1024-byte) units
# If message is larger it will be truncated with a "...e-mail trimmed" line
# RECOMMENDED VALUE: 256 - 5120 (.25M - 5M)
our $mailsh_sizelimit = 512;


#
##  ----------------------
##  Miscellaneous Settings
##  ----------------------
#

# When creating a push project the initial branch will, by default, be
# set to refs/heads/$initial_branch even on Git versions that do not have
# support for `git init --initial-branch=$initial_branch`.
# If this value is unset or invalid the default initial branch will always
# be "master".  Note that this only applies to newly created "push" projects;
# mirror projects and "adopted" projects ignore this setting.
# Set the $empty_commit_message setting to make this setting truly take effect.
# RECOMMENDED VALUE: whatever-your-favored-initial-branch-name-is
#our $initial_branch = "supercalifragilisticexpialidocious";
#our $initial_branch = "frabjous";
our $initial_branch = undef;

# When creating a new push project, if this is DEFINED to any value (including
# the empty string), then an initial empty commit will be added to the newly
# created push project that has an empty tree and contains the $empty_commit_message
# as its commit message.  By doing so, the initial branch will NOT be unborn and
# as a result, when cloning such a project the clone WILL respect the $initial_branch
# setting and set up its HEAD symbolic-ref to match.  Since the initial commit will
# have an empty tree it should be no less convenient than cloning a project with an
# unborn initial branch.  In fact, it should be more convenient as the expected
# $initial_branch will be checked out rather than whatever random initial branch the
# client might otherwise be inclined to set up for a newly initialized empty project.
# Defining this will also suppress the "You appear to have cloned an empty repository."
# message that would otherwise be generated by the Git client when cloning a newly
# created push project since the project will no longer technically be "empty".
# RECOMMENDED VALUE: defined if the $initial_branch setting should be respected
#our $empty_commit_message = "create project";
#our $empty_commit_message = "initial empty commit";
#our $empty_commit_message = "initial commit\n\ngit add ...\ngit commit\ngit push";
#our $empty_commit_message = "";
our $empty_commit_message = undef;


#
##  -------------------
##  Foreign VCS mirrors
##  -------------------
#

# Note that if any of these settings are changed from true to false, then
# any pre-existing mirrors using the now-disabled foreign VCS will stop
# updating, new mirrors using the now-disabled foreign VCS will be disallowed
# and attempts to update ANY project settings for a pre-existing project that
# uses a now-disabled foreign VCS source URL will also be disallowed.

# If $mirror is true and $mirror_svn is true then mirrors from svn source
# repositories will be allowed (and be converted to Git).  These URLs have
# the form svn://... or svn+http://... or svn+https://...
# Since this functionality makes use of the "git svn" command and is maintained
# with Git, it tends to be kept up-to-date and highly usable.
# Note that for this to work the "svn" command line command must be available
# in PATH and the "git svn" commands must work (which generally requires both
# Perl and the subversion perl bindings be installed).
# RECOMMENDED VALUE: 1 (if the necessary prerequisites are installed)
our $mirror_svn = 1;

# Prior to Git v1.5.1, git-svn always used a log window size of 1000.
# Starting with Git v1.5.1, git-svn defaults to using a log window size of 100
# and provides a --log-window-size= option to change it.  Starting with Git
# v2.2.0, git-svn disconnects and reconnects to the server every log window size
# interval to attempt to reduce memory use by git-svn.  If $svn_log_window_size
# is undefined, Girocco will use a log window size of 250 (instead of the
# the default 100).  If $svn_log_window_size is set, Girocco will use that
# value instead.  Beware that setting it too low (i.e. < 50) will almost
# certainly cause performance issues if not failures.  Unless there are concerns
# about git-svn memory use on a server with extremely limited memory, the
# value of 250 that Girocco uses by default should be fine.  Obviously if
# $mirror or $mirror_svn is false this setting is irrelevant.
our $svn_log_window_size = undef;

# If $mirror is true and $mirror_darcs is true then mirrors from darcs source
# repositories will be allowed (and be converted to Git).  These URLs have
# the form darcs+http://... darcs+https://... (and deprecated darcs://...)
# Note that for this to work the "darcs" command line command must be available
# in PATH and so must python (required to run the darcs-fast-export script).
# This support depends on items updated separately from Git and which may easily
# become out-of-date or incompatible (e.g. new python version).
# NOTE: If this is set to 0, the girocco-darcs-fast-export.git
#       submodule need not be initialized or checked out.
# RECOMMENDED VALUE: 0 (unless you have a need to mirror darcs repos)
our $mirror_darcs = 0;

# If $mirror is true and $mirror_bzr is true then mirrors from bzr source
# repositories will be allowed (and be converted to Git).  These URLs have
# the form bzr://...
# Note that for this to work the "bzr" command line command must be available
# in PATH (it's a python script so python is required as well).
# This support depends on items updated separately from Git and which may easily
# become out-of-date or incompatible (e.g. new python version).
# RECOMMENDED VALUE: 0 (unless you have a need to mirror bzr repos)
our $mirror_bzr = 0;

# If $mirror is true and $mirror_hg is true then mirrors from hg source
# repositories will be allowed (and be converted to Git).  These URLs have
# the form hg+http://... or hg+https://...
# Note that for this to work the "hg" command line command must be available
# in PATH and so must python (required to run the hg-fast-export.py script).
# Note that if the PYTHON environment variable is set that will be used instead
# of just plain "python" to run the hg-fast-export.py script (which needs to
# be able to import from mercurial).  Currently the hg-fast-export.py script
# used for this feature is likely incompatible with python 3 or later.
# Repositories created via this facility may need to be "reset" if the upstream
# hg repository moves the tip revision backwards and creates "unnamed heads".
# This support depends on items updated separately from Git and which may easily
# become out-of-date or incompatible (e.g. new python version).
# NOTE: If this is set to 0, the girocco-hg-fast-export.git
#       submodule need not be initialized or checked out.
# RECOMMENDED VALUE: 0 (unless you have a need to mirror hg repos)
our $mirror_hg = 0;


#
##  -----
##  Paths
##  -----
#

# Path where the main chunk of Girocco files will be installed
# This will get COMPLETELY OVERWRITTEN by each make install!!!
# MUST be an absolute path
our $basedir = '/home/repo/repomgr';

# Path where the automatically generated non-user certificates will be stored
# (The per-user certificates are always stored in $chroot/etc/sshcerts/)
# This is preserved by each make install and MUST NOT be under $basedir!
# The secrets used to generate TimedTokens are also stored in here.
# MUST be an absolute path
our $certsdir = '/home/repo/certs';

# The repository collection
# "$reporoot/_recyclebin" will also be created for use by toolbox/trash-project.pl
# MUST be an absolute path
our $reporoot = "/srv/git";

# The repository collection's location within the chroot jail
# Normally $reporoot will be bind mounted onto $chroot/$jailreporoot
# Should NOT start with '/'
our $jailreporoot = "srv/git";

# The chroot for ssh pushing; location for project database and other run-time
# data even in non-chroot setups
# MUST be an absolute path
our $chroot = "/home/repo/j";

# An installation that will never run a chrooted sshd should set this
# to a true value (e.g. 1) to guarantee that jailsetup for a chrooted
# sshd never takes place no matter what user runs `make install`.
# Note that the "jailsetup.sh" script will still run to do the database
# setup that's stored in $chroot regardless of this setting, it will just
# always run in "dbonly" mode when this setting is true.
our $disable_jailsetup = 0;

# The gitweb files web directory (corresponds to $gitwebfiles)
# Note that it is safe to place this under $basedir since it's set up after
# $basedir is completely replaced during install time.  Be WARNED, however,
# that normally the install process only adds/replaces things in $webroot,
# but if $webroot is under $basedir then it will be completely removed and
# rebuilt each time "make install" is run.  This will make gitweb/git-browser
# web services very briefly unavailable while this is happening.
# MUST be an absolute path
our $webroot = "/home/repo/www";

# The CGI-enabled web directory (corresponds to $gitweburl and $webadmurl)
# This will not be web-accessible except that if any aliases point to
# a *.cgi file in here it will be allowed to run as a cgi-script.
# Note that it is safe to place this under $basedir since it's set up after
# $basedir is completely replaced during install time.  Be WARNED, however,
# that normally the install process only adds/replaces things in $cgiroot,
# but if $cgiroot is under $basedir then it will be completely removed and
# rebuilt each time "make install" is run.  This will make gitweb/git-browser
# web services very briefly unavailable while this is happening.
# MUST be an absolute path
our $cgiroot = "/home/repo/cgibin";

# A web-accessible symlink to $reporoot (corresponds to $httppullurl, can be undef)
# If using the sample apache.conf (with paths suitably updated) this is not required
# to serve either smart or non-smart HTTP repositories to the Git client
# MUST be an absolute path if not undef
our $webreporoot = "/home/repo/www/r";

# The location to store the project list cache, gitweb project list and gitweb
# cache file.  Normally this should not be changed.  Note that it must be in
# a directory that is writable by $mirror_user and $cgi_user (just in case the
# cache file is missing).  The directory should have its group set to $owning_group.
# Again, this setting should not normally need to be changed.
# MUST be an absolute path
our $projlist_cache_dir = "$chroot/etc";


#
##  ----------------------------------------------------
##  Certificates (only used if $httpspushurl is defined)
##  ----------------------------------------------------
#

# path to root certificate (undef to use automatic root cert)
# this certificate is made available for easy download and should be whatever
# the root certificate is for the https certificate being used by the web server
our $rootcert = undef;

# The certificate to sign user push client authentication certificates with (undef for auto)
# The automatically generated certificate should always be fine
our $clientcert = undef;

# The private key for $clientcert (undef for auto)
# The automatically generated key should always be fine
our $clientkey = undef;

# The client certificate chain suffix (a pemseq file to append to user client certs) (undef for auto)
# The automatically generated chain should always be fine
# This suffix will also be appended to the $mobusercert before making it available for download
our $clientcertsuffix = undef;

# The mob user certificate signed by $clientcert (undef for auto)
# The automatically generated certificate should always be fine
# Not used unless $mob is set to 'mob'
# The $clientcertsuffix will be appended before making $mobusercert available for download
our $mobusercert = undef;

# The private key for $mobusercert (undef for auto)
# The automatically generated key should always be fine
# Not used unless $mob is set to 'mob'
our $mobuserkey = undef;

# Server alt names to embed in the auto-generated girocco_www_crt.pem certificate.
# The common name (CN) in the server certificate is the host name from $httpspushurl.
# By default no server alt names are embedded (not even the host from $httpspushurl).
# If the web server configuration is not using this auto-generated server certificate
# then the values set here will have no impact and this setting can be ignored.
# To embed server alternative names, list each (separated by spaces).  The names
# may be DNS names, IPv4 addresses or IPv6 addresses (NO surrounding '[' ']' please).
# If ANY DNS names are included here be sure to also include the host name from
# the $httpspushurl or else standards-conforming clients will fail with a host name
# mismatch error when they attempt to verify the connection.
#our $wwwcertaltnames = 'example.com www.example.com git.example.com 127.0.0.1 ::1';
our $wwwcertaltnames = undef;

# The key length for automatically generated RSA private keys (in bits).
# These keys are then used to create the automatically generated certificates.
# If undef or set to a value less than 2048, then 2048 will be used.
# Set to 3072 to generate more secure keys/certificates.  Set to 4096 (or higher) for
# even greater security.  Be warned that setting to a non-multiple of 8 and/or greater
# than 4096 could negatively impact compatibility with some clients.
# The values 2048, 3072 and 4096 are expected to be compatible with all clients.
# Note that OpenSSL has no problem with > 4096 or non-multiple of 8 lengths.
# See also the $min_key_length setting above to restrict user key sizes.
# This value is also used when generating the ssh_host_rsa_key for the chroot jail sshd.
# RECOMMENDED VALUE: 3072
our $rsakeylength = undef;


#
##  -------------
##  URL addresses
##  -------------
#

# URL of the gitweb.cgi script (must be in pathinfo mode).  If the sample
# apache.conf configuration is used, the trailing "/w" is optional.
our $gitweburl = "https://repo.or.cz";

# URL of the extra gitweb files (CSS, .js files, images, ...)
our $gitwebfiles = "https://repo.or.cz";

# URL of the Girocco CGI web admin interface (Girocco cgi/ subdirectory)
# e.g. reguser.cgi, edituser.cgi, regproj.cgi, editproj.cgi etc.
our $webadmurl = "https://repo.or.cz";

# URL of the Girocco CGI bundles information generator (Girocco cgi/bundles.cgi)
# If the sample apache.conf configuration is used, the trailing "/b" is optional.
# This is different from $httpbundleurl.  This URL lists all available bundles
# for a project and returns that as an HTML page.
our $bundlesurl = "https://repo.or.cz";

# URL of the Girocco CGI html templater (Girocco cgi/html.cgi)
# If mod_rewrite is enabled and the sample apache.conf configuration is used,
# the trailing "/h" is optional when the template file name ends in ".html"
# (which all the provided ones do).
our $htmlurl = "https://repo.or.cz";

# HTTP URL of the repository collection (undef if N/A)
# If the sample apache.conf configuration is used, the trailing "/r" is optional.
our $httppullurl = "https://repo.or.cz";

# HTTP URL of the repository collection when fetching a bundle (undef if N/A)
# Normally this will be the same as $httppullurl, but note that the bundle
# fetching logic is located in git-http-backend-verify so whatever URL is
# given here MUST end up running the git-http-backend-verify script!
# For example, if we're fetching the 'clone.bundle' for the 'girocco.git'
# repository, the final URL will be "$httpbundleurl/girocco.git/clone.bundle"
# If the sample apache.conf configuration is used, the trailing "/r" is optional.
# This is different from $bundlesurl.  This URL fetches a single Git-format
# .bundle file that is only usable with the 'git bundle' command.
our $httpbundleurl = "https://repo.or.cz";

# HTTPS push URL of the repository collection (undef if N/A)
# If this is defined, the openssl command must be available
# The sample apache.conf configuration requires mod_ssl, mod_authn_anon and
# mod_rewrite be enabled to support https push operations.
# Normally this should be set to $httppullurl with http: replaced with https:
# If the sample apache.conf configuration is used, the trailing "/r" is optional.
our $httpspushurl = "https://repo.or.cz";

# Git URL of the repository collection (undef if N/A)
# (You need to set up git-daemon on your system, and Girocco will not
# do this particular thing for you.)
our $gitpullurl = "git://repo.or.cz";

# Pushy SSH URL of the repository collection (undef if N/A)
# Note that the "/$jailreporoot" portion is optional and will be automatically
# added if appropriate when omitted by the client so this URL can typically
# be made the same as $gitpullurl with git: replaced with ssh:
our $pushurl = "ssh://repo.or.cz";

# URL of gitweb of this Girocco instance (set to undef if you're not nice
# to the community)
our $giroccourl = "$Girocco::Config::gitweburl/girocco.git";


#
##  -------------------
##  Web server controls
##  -------------------
#

# If true then non-smart HTTP access will be disabled
# There's normally no reason to leave non-smart HTTP access enabled
# since downloadable bundles are provided.  However, in case the
# non-smart HTTP access is needed for some reason, this can be set to undef or 0.
# This affects access via http: AND https: and processing of apache.conf.in.
# Note that this setting does not affect gitweb, ssh: or git: URL access in any way.
# RECOMMENDED VALUE: 1
our $SmartHTTPOnly = 1;

# If true, the https <VirtualHost ...> section in apache.conf.in will be
# automatically enabled when it's converted to apache.conf by the conversion
# script.  Do NOT enable this unless the required Apache modules are present
# and loaded (mod_ssl, mod_rewrite, mod_authn_anon) AND $httpspushurl is
# defined above otherwise the server will fail to start (with various errors)
# when the resulting apache.conf is used.
our $TLSHost = 1;

# If true, the information about configuring a Git client to trust
# a Girocco-generated TLS root will be suppressed presuming that some other
# means (such as LetsEncrypt.org) has been used to generate a TLS web
# certificate signed by a pre-trusted root.  This does NOT affect the
# information on how to configure https push certificates as those are still
# required in order to push over https regardless of what web server certificate
# may be in use.
# RECOMMENDED VALUE: 0 (for girocco-generated root & server certificates)
# RECOMMENDED VALUE: 1 (for LetsEncrypt etc. generated server certificates)
our $pretrustedroot = 1;


#
##  ------------------------
##  Lighttpd server controls
##  ------------------------
#

# Of course, the lighttp.conf.in file can be edited directly, but these
# settings allow it to contain conditional sections that show how the
# various configurations can be achieved.

# If lighttpd will not be used, these settings can be ignored.

# N.B. The lighttpd.conf.in file MUST be edited if lighttpd should listen
#      on ports other than 80 (http) and 443 (https)

# If true, the lighttpd.conf.in file will be processed into a lighttpd.conf
# file that tries very hard to be a complete, standalone configuration file for
# a lighttpd server.  In other words, it will set things in the lighttpd global
# configuration that would not be needed (or safe) if it were being included
# to provide only a "virtual host" configuration.
# RECOMMENDED VALUE: 0 (for use as an included "virtual host" configuration)
# RECOMMENDED VALUE: 1 (for use as a standalone configuration file)
our $lighttpd_standalone = 0;

# Only applicable if $lighttpd_standlone has been set to a true value,
# otherwise this setting has no effect.
# If true, the parts of the standalone lighttpd configuration that would
# require privileges (e.g. log file, pid file, etc.) will be redirected to
# "unprivileged" locations and neither the username nor groupname settings
# will be set.  Otherwise "standard" locations and so forth will be used
# (such as /var/run, /var/log etc.).  Note that this will NOT change the
# ports lighttpd attempts to listen on -- edit lighttpd.conf.in to do that
# and note that the port numbers will likely need to be changed in order to
# run in unprivileged mode (e.g. to 8080 and 8443).
# RECOMMENDED_VALUE: 0 (if running privileged as $lighttpd_standalone)
# RECOMMENDED_VALUE: 1 (if running unprivileged as $lighttpd_standalone)
our $lighttpd_unprivileged = 0;

# If true, listen only to the loopback interface (i.e. 127.0.0.1/::1)
# Otherwise allow incoming connections from anywhere
# RECOMMENDED_VALUE: 0 (for an externally accessible girocco web interface)
# RECOMMENDED_VALUE: 1 (for a localhost-accessible-only girocco web interface)
our $lighttpd_loopback_only = 0;

# This will be ignored unless $lighttpd_standalone is a false value
# See the copious comments in lighttpd.conf.in (search for TLSHost)
# RECOMMENDED_VALUE: 0 (if !$lighttpd_standalone but no other TLS hosts)
# RECOMMENDED_VALUE: 1 (if !$lighttpd_standalone and other TLS hosts are present)
our $lighttpd_tls_virtualhost = 1;


#
##  ------------------------
##  Some templating settings
##  ------------------------
#

# Legal warning (on reguser and regproj pages)
our $legalese = <<EOT;
<p>By submitting this form, you are confirming that you will mirror or push
only free software and redistributing it will not violate any law
of Czech Republic, Germany or the European Union in general.
<sup class="sup"><span><a href="/about.html">(more details)</a></span></sup>
</p>
EOT

# Pre-configured mirror sources (set to undef for none)
# Arrayref of name - record pairs, the record has these attributes:
#	label: The label of this source
# 	url: The template URL; %1, %2, ... will be substituted for inputs
#	desc: Optional VERY short description
#	link: Optional URL to make the desc point at
#	inputs: Arrayref of hashref input records:
#		label: Label of input record 
#		suffix: Optional suffix
#	If the inputs arrayref is undef, single URL input is shown,
#	pre-filled with url (probably empty string).
our $mirror_sources = [
	{
		label => 'Anywhere',
		url => '',
		desc => 'Any HTTP/Git/rsync pull URL - bring it on!',
		inputs => undef
	},
	{
		label => 'GitHub',
		url => 'https://github.com/%1/%2.git',
		desc => 'GitHub Social Code Hosting',
		link => 'https://github.com/',
		inputs => [ { label => 'User:' }, { label => 'Project:', suffix => '.git' } ]
	},
	{
		label => 'GitLab',
		url => 'https://gitlab.com/%1/%2.git',
		desc => 'Engulfed the Green and Orange Boxes',
		link => 'https://gitlab.com/',
		inputs => [ { label => 'User:' }, { label => 'Project:', suffix => '.git' } ]
	},
	{
		label => 'Bitbucket',
		url => 'https://bitbucket.org/%1/%2.git',
		desc => 'Embraced the best DVCS',
		link => 'https://bitbucket.org/',
		inputs => [ { label => 'User:' }, { label => 'Project:', suffix => '.git' } ]
	}
];

# You can customize the gitweb interface widely by editing
# gitweb/gitweb_config.perl


#
##  -------------------
##  Permission settings
##  -------------------
#

# Girocco needs some way to manipulate write permissions to various parts of
# all repositories; this concerns three entities:
# - www-data: the web interface needs to be able to rewrite few files within
# the repository
# - repo: a user designated for cronjobs; handles mirroring and repacking;
# this one is optional if not $mirror
# - others: the designated users that are supposed to be able to push; they
# may have account either within chroot, or outside of it

# There are several ways how to use Girocco based on a combination of the
# following settings.

# (Non-chroot) UNIX user the CGI scripts run on; note that if some non-related
# untrusted CGI scripts run on this account too, that can be a big security
# problem and you'll probably need to set up suexec (poor you).
# This must always be set.
our $cgi_user = 'www-data';

# (Non-chroot) UNIX user performing mirroring jobs; this is the user who
# should run all the daemons and cronjobs and
# the user who should be running make install (if not root).
# This must always be set.
our $mirror_user = 'repo';

# (Non-chroot) UNIX group owning the repositories by default; it owns whole
# mirror repositories and at least web-writable metadata of push repositories.
# If you undefine this, all the data will become WORLD-WRITABLE.
# Both $cgi_user and $mirror_user should be members of this group!
our $owning_group = 'repo';

# Whether to use chroot jail for pushing; this must be always the same
# as $manage_users.
# TODO: Gitosis support for $manage_users and not $chrooted?
our $chrooted = $manage_users;

# How to control permissions of push-writable data in push repositories:
# * 'Group' for the traditional model: The $chroot/etc/group project database
#   file is used as the UNIX group(5) file; the directories have gid appropriate
#   for the particular repository and are group-writable. This works only if
#   $chrooted so that users are put in the proper groups on login when using
#   SSH push.  Smart HTTPS push does not require a chroot to work -- simply
#   run "make install" as the non-root $mirror_user user, but leave
#   $manage_users and $chrooted enabled.
# * 'ACL' for a model based on POSIX ACL: The directories are coupled with ACLs
#   listing the users with push permissions. This works for both chroot and
#   non-chroot setups, however it requires ACL support within the filesystem.
#   This option is BASICALLY UNTESTED, too. And UNIMPLEMENTED. :-)
# * 'Hooks' for a relaxed model: The directories are world-writable and push
#   permission control is purely hook-driven. This is INSECURE and works only
#   when you trust all your users; on the other hand, the attack vectors are
#   mostly just DoS or fully-traceable tinkering.
our $permission_control = 'Group';

# Path to alternate screen multiuser acl file (see screen/README, undef for none)
our $screen_acl_file = undef;

# Reserved project name and user name suffixes.
#
# Note that with personal mob branches enabled, a user name can end up being a
# file name after having a 'mob.' prefix added or a directory name after having
# a 'mob_' prefix added.  If there is ANY possibility that a file with a
# .suffix name may need to be served by the web server, lc(suffix) SHOULD be in
# this hash!  Pre-existing project names or user names with a suffix in this
# table can continue to be used, but no new projects or users can be created
# that have a suffix (case-insensitive) listed here.
our %reserved_suffixes = (
	# Entries must be lowercase WITHOUT a leading '.'
	bmp => 1,
	bz2 => 1,
	cer => 1,
	cgi => 1,
	crt => 1,
	css => 1,
	dmg => 1,
	fcgi => 1,
	gif => 1,
	gz => 1,
	htm => 1,
	html => 1,
	ico => 1,
	jp2 => 1,
	jpeg => 1,
	jpg => 1,
	jpg2 => 1,
	js => 1,
	mp3 => 1,
	mp4 => 1,
	pdf => 1,
	pem => 1,
	php => 1,
	png => 1,
	sig => 1,
	shtml => 1,
	svg => 1,
	svgz => 1,
	tar => 1,
	text => 1,
	tgz => 1,
	tif => 1,
	tiff => 1,
	txt => 1,
	xbm => 1,
	xht => 1,
	xhtml => 1,
	xz => 1,
	zip => 1,
);


#
##  -------------------
##  Size limit settings
##  -------------------
#

# If this is set to a non-zero value, whenever a receive-pack, mirror fetch
# or clone runs, git will be run with a UL_SETFSIZE value set to this value.
#
# The limit is not active while performing garbage collection or other
# maintenance tasks.
#
# If git attempts to create a file larger than this limit, it will receive a
# SIGXFSZ signal which will cause git to terminate.
#
# Note that if the actual value of UL_GETFSIZE at runtime is already less than
# the value set here, then that value will be silently used instead.
#
# The value represents the maximum file size allowed in units of 512-byte blocks
# and must be <= 2147483647 (which represents a size of 1 TiB less 512 bytes).
#
our $max_file_size512 = 8388608; # 4 GiB

# If this is set to a non-zero value, after an otherwise successful clone,
# if the repository contains more than this many objects, the clone will
# be considered to fail.
#
# This setting only takes effect after an otherwise successful clone which
# means that if $max_file_size512 is non-zero that the resulting clone did
# not exceed the file size limit if it fails this check.
#
our $max_clone_objects = 9999999;


#
##  -------------------------
##  sendmail.pl configuration
##  -------------------------
#

# Full information on available sendmail.pl settings can be found by running
# ../bin/sendmail.pl -v -h

# These settings will only be used if $sendmail_bin is set to 'sendmail.pl'

# sendmail.pl host name
#$ENV{'SENDMAIL_PL_HOST'} = 'localhost'; # localhost is the default

# sendmail.pl port name
#$ENV{'SENDMAIL_PL_PORT'} = '25'; # port 25 is the default

# sendmail.pl nc executable
#$ENV{'SENDMAIL_PL_NCBIN'} = "$chroot/bin/nc.openbsd"; # default is nc found in $PATH

# sendmail.pl nc options
# multiple options may be included, e.g. '-4 -X connect -x 192.168.100.10:8080'
#$ENV{'SENDMAIL_PL_NCOPT'} = '-4'; # force IPv4, default is to allow IPv4 & IPv6


#
##  -------------------------
##  Obscure Tuneable Features
##  -------------------------
#

# Throttle classes configured here override the defaults for them that
# are located in taskd/taskd.pl.  See comments in that file for more info.
our @throttle_classes = ();

# Any tag names listed here will be allowed even if they would otherwise not be.
# Note that @allowed_tags takes precedence over @blocked_tags.
our @allowed_tags = (qw( ));

# Any tag names listed here will be disallowed in addition to the standard
# list of nonsense words etc. that are blocked as tags.
our @blocked_tags = (qw( ));

# Case folding tags
# If this setting is true, then tags that differ only in case will always use
# the same-cased version.  If this setting is enabled and the tag is present in
# @allowed_tags (or the embedded white list in Util.pm) then the case of the
# tag will match the white list entry otherwise it will be all lowercased.
# If this setting is disabled (false) tags are used with their case left as-is.
# RECOMMENDED VALUE: 1 (true)
our $foldtags = 1;

# If there are no more than this many objects, then all deltas will be
# recomputed when gc takes place.  Note that this does not affect any
# fast-import created packs as they ALWAYS have their deltas recomputed.
# Also when combining small packs, if the total object count in the packs
# to be combined is no more than this then the new, combined pack will have
# its deltas recomputed during the combine operation.
# Leave undef to use the default (which should generally be fine).
# Lowering this from the default can increase disk use.
# Values less than 1000 * number of CPU cores will be silently ignored.
# The "girocco.redelta" config item can be used to modify this behavior on
# a per-repository basis.  See the description of it in gc.sh.
our $new_delta_threshold = undef;

# This setting is irrelevant unless foreign vcs mirrors that use git fast-import
# are enabled (e.g. $mirror_darcs, $mirror_bzr or $mirror_hg -- $mirror_svn does
# NOT use git fast-import and is not affected by this setting).
# The packs generated by git fast-import are very poor quality.  For this reason
# they ALWAYS have their deltas recomputed at some point.  Normally this is
# delayed until the next full (or mini) gc takes place.  For this reason a full
# gc is always scheduled immediately after a fresh mirror clone completes.
# However, in the case of normal mirror updates, several git fast-import created
# packs may exist as a result of changes fetched during the normal mirror update
# process.  These packs will persist (with their git fast-import poor quality)
# until the next full (or mini) gc triggers.  The bad deltas in these update
# packs could be sent down to clients who fetch updates before the next gc
# triggers.  To reduce (i.e. practically eliminate) the likelihood of this
# occurring, this setting can be changed to a false (0 or undef) value in which
# case after each mirror update of a git fast-import mirror, any newly created
# git fast-import packs (as a result of the mirror update running) will have
# their deltas recomputed shortly thereafter instead of waiting for the next gc.
# Recomputing deltas immediately (almost immediately) will result in an extra
# redeltification step (with associated CPU cost) that would otherwise not
# occur and, in some cases (mainly large repositories), could ultimately result
# in slightly less efficient deltas being retained.
# RECOMMENDED VALUE: 1
our $delay_gfi_redelta = 1;

# If this is set to a true value, then core.packedGitWindowSize will be set
# to 1 MiB (the same as if Git was compiled with NO_MMAP set).  If this is NOT
# set, core.packedGitWindowSize will be set to 32 MiB (even on 64-bit) to avoid
# memory blowout.  If your Git was built with NO_MMAP set and will not work
# without NO_MMAP set, you MUST set this to a true value!
our $git_no_mmap = undef;

# If set to a true value, the "X-Girocco: $gitweburl" header included in all
# Girocco-generated emails will be suppressed.
our $suppress_x_girocco = undef;

# Number of days to keep reflogs around
# May be set to a value between 1 and 30 (inclusive)
# The default of one day should normally suffice unless there's a need to
# support a "Where's the undo?  WHERE IS THE UNDO?!!!" option ;)
our $reflogs_lifetime = 7;

# The pack.window size to use with git upload-pack
# When Git is creating a pack to send down to a client, if it needs to send
# down objects that are deltas against objects it is not sending and that it
# does not know the client already has, it must undelta and recompute deltas
# for those objects.  This is the remote's "Compressing objects" phase the
# client sees during a fetch or clone.  If this value is unset, the normal
# Git default of 10 will be used for the window size during these operations.
# This value may be set to a number between 2 and 50 inclusive to change the
# window size during upload pack operations.  A window size of 2 provides the
# fastest response at the expense of less efficient deltas for the objects
# being recompressed (meaning more data to send to the client).  A window
# size of 5 typically reduces the compression time by almost half and is
# usually nearly as fast as a window size of 2 while providing better deltas.
# A window size of 50 will increase the time spent in the "Compressing objects"
# phase by as much as 5 times but will produce deltas similar to those that
# Girocco generates when it performs garbage collection.
# RECOMMENDED VALUE: undef or 5
our $upload_pack_window = 5;

# If this is true then remote fetching of refs/stash and refs/tgstash will
# be allowed.  Git does not allow single-level ref names to be pushed so the
# only way they could get in there is if a linked working tree (or, gasp, a
# non-bare Girocco repository) created them or they arrived via a non-clean
# mirror fetch.  The client almost certainly does not want to see them.
# Unless this config item is true they will also be left out of the bundle too.
# Since both stash and tgstash are used with their ref logs and there's no way
# for a remote to fetch ref logs, the "log --walk-reflogs" feature could not be
# used with them by a remote that fetched them anyway.
#
# NOTE: The reason this doesn't just control all single-level refs is that the
# "hideRefs" configuration mechanism isn't flexible enough to hide all
# single-level refs without knowing their names.  In addition, it hides the
# entire refs hierarchy so refs/stash/foo will also be hidden along with
# refs/stash, but Git doesn't actually support ref names that introduce a
# directory/file confict (aka D/F conflict) and "refs/stash" represents an
# official Git ref name therefore any refs/stash/... names really aren't
# allowed in the first place so it's no problem if they're incidentally hidden
# along with refs/stash itself.
#
# NOTE: Git 1.8.2 or later is required to actually hide the refs from fetchers
# over the "git:" protocol and Git 2.3.5 or later is required to properly hide
# them over the smart "http:" protocol (Girocco will not attempt to "hide" them
# on a smart HTTP fetch if Git is < 2.3.5 to avoid Git bugs.)  They will never
# be hidden via the non-smart HTTP fetch or any other non-smart protocols that
# also make use of the $gitdir/info/refs file as they are not excluded from it.
# Nor will they be hidden when accessed via any non-Girocco mechanism.
# They will, however, always be excluded from the primary (aka .bitmap) pack
# and bundle no matter what version of Git is used unless this is set to a
# true value.  It's only the server's Git version that matters when hiding refs.
#
# RECOMMENDED VALUE: undef or 0
our $fetch_stash_refs = undef;

# When set to a true value, Girocco will attempt to pick up ref changes made
# outside of Girocco itself and process them using the usual Girocco
# notification mechanism.  Git lacks any "post-ref-change" hook capability that
# could facilitate this prior to the introduction of the reference-transation
# hook in Git v2.28.0.  This feature is primarily intended to detect running
# of "git fetch" in linked working trees of a Girocco repository.  In many
# cases after running a command Git runs "git gc --auto".  With the correct
# encouragement we can always induce Git to run our pre-auto-gc hook at that
# time.  "git fetch" invokes "git gc --auto" after the fetch.  Girocco needs
# to do additional maintenance to make this work properly so do not enable this
# unless it's really needed.  Additionally, there are a number of commands
# (such as "git commit") that do not invoke "git gc --auto".  Even with this
# enabled, additional hooks for post-rewrite and post-checkout
# would really be needed to catch more things and even then there are some
# Git commands that would never be caught ("git filter-branch",
# "git update-ref", "git reset", etc.) so this is hardly a complete solution.
# But it WILL catch "git fetch" changes although the hashes it uses for the
# "old" ref values may not be all that recent, the new ref values will be.
# When this is false, the hack is completely disabled.
# When this is true, the hack is enabled by default for all repositories,
# but can be controlled on an individual repository basis by setting the
# girocco.autogchack value explicitly to true (enable) or false (disable).
# If this is set to the special value "mirror" then it will behave as true
# for mirrors and false for non-mirrors thereby completely eliminating any
# overhead for push projects but detecting external "git fetch"s for mirrors.
# If this is enabled for a project, any third party script/tool can trigger
# the Girocco ref notification mechanism simply by making a ref change and
# then running "git gc --auto --quiet" on the project.  In a capitulation to
# use of linked working trees, Girocco installs a post-commit hook that will
# trigger these notifications as well when this is enabled.
our $autogchack = 0;

# When set to a true value the initial setting for core.hooksPath will point
# to the repository's own hooks directory instead of $reporoot/_global/hooks.
# Due to the unfortunate implementation of core.hooksPath, Girocco must always
# ensure the value gets set in each repository's config file.  Normally it
# just sets it to $reporoot/_global/hooks and that's that.  However, the
# update-all-config script will also tolerate it pointing at the repository's
# own hooks directory -- Girocco maintains symbolic links in there to the
# global hooks to make sure they get run when using older versions of Git;
# therefore that setting is basically equivalent.  The difference is that
# repository-specific hooks can be added when hooksPath is pointing at the
# repository's hooks directory but not when it's pointing at _global/hooks.
# A repository's setting can be changed manually (and it will stick), but
# sometimes it may be desirable to always just default to pointing at the
# repository's own hooks directory from the start.  Perhaps linked working
# trees will be in use and software that needs to set repository-specific hooks
# will be in use.  If $autogchack has been set to true this may very likely be
# the case.
our $localhooks = 0;

# If this is set to a true value changes to single-level refs (e.g. refs/stash)
# will be passed through to the notification machinery.
# Usually this is NOT wanted, especially when linked working trees are being
# used with the repository.
# However, in the unlikely event that changes to such ref names should NOT be
# ignored, this value may be set to any true value.
# RECOMMENDED VALUE: 0
our $notify_single_level = 0;

# If this is set to a non-empty value it will become the default value for
# all repositories' girocco.notifyHook value.
# Whenever taskd.pl receives a batch of ref changes for processing, it first
# sends them off to any configured "girocco.notifyHook" (same semantics as
# a post-receive hook except it also gets four command-line arguments like
# so: cat ref-changes | notifyhook $projname $user $linecount $contextlinecount
# There is no default notify hook, but each repository may set its own by
# setting the `girocco.notifyHook` config value which will be eval'd by the
# shell (like $GIT_EDITOR is) with the current directory set to the
# repository's git-dir and the changes on standard input.
# Note that normal notification processing does not take place until after
# this command (if it's not null) gets run (regardless of its result code).
our $default_notifyhook = undef;

# UNIX group owning the repositories' htmlcache subdirectory
# If not defined defaults to $owning_group
# If gitweb access is provided but only on a read-only basis, then setting
# this to a group to which Both $cgi_user and $mirror_user belong will still
# allow summary page caching.
# $mirror_user should always belong to this group
our $htmlcache_owning_group = undef;

# UNIX group owning the repositories' ctags subdirectory
# If not defined defaults to $owning_group
# If gitweb access is provided but only on a read-only basis, then setting
# this to a group to which Both $cgi_user and $mirror_user belong will still
# allow tags to be added to the repository in gitweb (provided that feature
# is enabled in gitweb/gitweb_config.perl).
# $mirror_user should always belong to this group
our $ctags_owning_group = undef;

# When using pack bitmaps and computing data to send to clients over a fetch,
# having pack.writeBitmapHashCache set to true produces better deltas (thereby
# potentially reducing the amount of data that needs to be sent).  However, old
# JGit does not understand this extra data, so if JGit needs to use the bitmaps
# generated when Girocco runs Git, this setting needs to be set to a true value
# so that the hash cache is excluded when Git generates the bitmaps thereby
# making them compatible with JGit prior to JGit v3.5.0 released in late 2014.
# Note that changes to this setting will not take effect until the next time
# gc is scheduled to run on a project and then only if gc actually takes place.
# Use the $basedir/toolbox/make-all-gc-eligible.sh script to force all projects
# to actually do a gc the next time they are scheduled for one.
# (Note that Git version 2.22.0, released 2019-06-07, enabled the HashCache by
# default since it post dates the JGit v3.5.0 release by approximately 5 years
# [that corresponds to a setting of undef or 0 here].)
# RECOMMENDED VALUE: undef or 0
our $jgit_compatible_bitmaps = 0;

# Set the default value of receive.maxInputSize
# This is only effective for receives (aka an incoming push) and causes the
# push to abort if the incoming pack (which is generally thin and does not
# have any index) exceeds this many bytes in size (a 'k', 'm' or 'g' suffix
# may be used on the value).  If undef or set to 0 there is no limit.  This
# limit is only effective when Girocco is running Git v2.11.0 or later.
our $max_receive_size = undef;

# Suppress git: and ssh: log messages
# Access via http: and/or https: provides logging of the project being
# cloned/fetched/pushed to.  There is normally no such logging for access
# via ssh: and/or git: protocols.  However, Girocco intercepts those
# accesses to perform sanity and permision checks and also logs the request
# to the system log at that time.  By setting this value to any true
# value, that logging of git: and ssh: git activity will be suppressed.
# RECOMMENDED VALUE: 0
our $suppress_git_ssh_logging = 0;

# Select the sshd to be installed into the chroot
# If set this MUST be an absolute path
# Ignored unless a chroot is actually being created
# Leaving this undef will find sshd in "standard" system locations and
# is the recommended value.  Only set this if you need to override the
# "standard" sshd for some reason.
# RECOMMENDED VALUE: undef
our $sshd_bin = undef;

# Allow any git-daemon host
# If set to a true value, then the extra "host=" parameter received
# by git-daemon (if present) will be ignored.  If the $gitpullurl value
# is undefined or does not start with "git://<hostname>" then any host
# will be allowed by default.
# RECOMMENDED VALUE: undef
our $git_daemon_any_host = undef;

# Restrict git-daemon host names
# If $git_daemon_any_host is any false value (or undef) AND this
# value is set to a space-separated list of host names WITHOUT any
# port numbers, then the "host=" parameter MUST be provided by
# a git daemon client AND it must match one of the names in this
# all-lowercase, space-separated list.  Note that IPv6 literal
# addresses MUST NOT be enclosed in brackets.  If this value is
# empty or undef it will default to the hostname extracted from
# $gitpullurl if that is set plus several variants of localhost.
# Note, do NOT terminate DNS names with a final "." or they will
# never match!
# EXAMPLE:
#   our $git_daemon_host_list = "foo.example.com localhost ::1 127.0.0.1";
our $git_daemon_host_list = undef;

# Shared repository setting
# This is the value to set the core.sharedRepository value to in
# each repository.  If this is changed, running update-all-config
# will update each project to the new value.
# By default (if this is left undef) it will be set to 'all' (which
# corresponds to a core.sharedRepository value of '2').
# This makes it so repositories are group-writable and world readable.
# If set to the more restrictive 'group' (which corresponds to a
# core.sharedRepository value of '1'), repositories will still typically
# be world readable as umask 002 is used in most places.  However, if
# worktrees are in use and the user has a umask that excludes other
# read permissions, repositories might become only partially world
# readable.  This only matters when they are being served by a user
# (such as git-daemon running as 'nobody') that does not have group
# or owner read permission to the repository.
# See the `git help init` output for the possible values of the
# `--shared` option for allowed settings of this value.
# Note that if an explicit octal value is used, any execute bits will
# be removed and u+rw will always be added and there are no promises
# that files will not end up group readable/writable anyway.
# RECOMMENDED VALUE: undef
our $git_shared_repository_setting = undef;

#
##  ------------------------
##  Sanity checks & defaults
##  ------------------------
#

# A separate, non-installed module handles the checks and defaults
require Girocco::Validator;

1;

# Girocco::Dumper.pm -- Installation Utility Dumper Functions
# Copyright (C) 2020,2021 Kyle J. McKay.  All rights reserved.

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

{package _Girocco::Dumper::HashLog;

use strict;
use warnings;

use Tie::Hash ();
use base qw(Tie::ExtraHash);

sub TIEHASH {
	my $class = shift;
	my $hr = $_[0];
	ref($hr) eq 'HASH' or $hr = {};
	my $self = bless([$hr, {}], $class);
	return $self;
}

sub STORE {
	my $self = shift;
	${$$self[1]}{$_[0]} = [$_[1]];
	$self->SUPER::STORE(@_);
}

sub DELETE {
	my $self = shift;
	${$$self[1]}{$_[0]} = [];
	$self->SUPER::DELETE(@_);
}

}

package Girocco::Dumper;

use strict;
use warnings;

use base qw(Exporter);
our ($VERSION, @EXPORT, @EXPORT_OK);

BEGIN {
	@EXPORT = qw();
	@EXPORT_OK = qw(RequireENV Dump DumpENV Boilerplate Scalars FreezeConfig GetConfPath);
	*VERSION = \'1.0';
}

use B ();
use Data::Dumper ();

my $_sortkeys;
BEGIN {$_sortkeys = sub {
	package _Girocco::Dumper;
	sort({uc($a) cmp uc($b) || $a cmp $b} keys(%{$_[0]}));
}}

my $_dumpit;
BEGIN {$_dumpit = sub {
	my $d = Data::Dumper->new([$_[0]],[$_[1]]);
	$d->Purity(1)->Indent(1)->Useqq(1)->Quotekeys(0)->Sortkeys(1)->Deparse($_[2]||0);
	$d->Dump;
}}

my $_dumpstr;
BEGIN {$_dumpstr = sub {
	my $d = Data::Dumper->new([$_[0]]);
	$d->Purity(1)->Indent(0)->Useqq(1)->Quotekeys($_[1]||0)->Sortkeys(1)->Terse(1);
	$d->Dump;
}}

my $_dumpenv;
BEGIN {$_dumpenv = sub {
	my ($k, $v) = @_;
	return 'delete $ENV{'.&$_dumpstr($k)."};\n" unless @$v;
	return '$ENV{'.&$_dumpstr($k).'} = '.&$_dumpstr($$v[0],1).";\n";
}}

my $_scalarexists;
BEGIN {$_scalarexists = sub {
	# *foo{SCALAR} returns a reference to an anonymous scalar if $foo
	# has not been used yet thereby creating the scalar and "using" it.
	# Later perls (5.9+) assign a special value that can be detected.
	B::svref_2object($_[0])->SV->isa("B::SV") # isa will be false if it's "special"
	# additionally ...SV->can("object_2svref") will be undef if it's "special"
}}

sub Boilerplate {
	return 'umask(umask() & ~0770);'."\n";
}

sub Scalars {
	my $ns = shift;
	my $hr = eval '\%'.$ns.'::';
	my @result = ();
	foreach (&$_sortkeys($hr)) {
		my $gr = eval '\$'.$ns.'::{$_}';
		push(@result, $_) if &$_scalarexists($gr) && !ref($$$gr);
	}
	return @result;
}

sub Dump {
	my $ns = shift;
	my $hr = eval '\%'.$ns.'::';
	my $result = "";
	foreach (&$_sortkeys($hr)) {
		my $gr = eval '\$'.$ns.'::{$_}';
		if (&$_scalarexists($gr)) {
			$result .= "our ".&$_dumpit($$$gr, $_, ref($$$gr) eq 'CODE');
		}
		if (defined(*$$gr{ARRAY})) {
			$result .= "our ".&$_dumpit(\@$$gr, '*'.$_);
		}
		if (defined(*$$gr{HASH})) {
			$result .= "our ".&$_dumpit(\%$$gr, '*'.$_);
		}
		#if (defined(*$$gr{CODE})) {
		#	$result .= &$_dumpit(\&$$gr, '*'.$_, 1);
		#}
	}
	return $result;
}

sub RequireENV {
	my $inmod = shift;
	!ref($inmod) and $inmod = [ $inmod ];
	ref($inmod) eq 'ARRAY' or die "invalid argument to RequireENV - must be scalar or ARRAY ref";
	my @mod = ();
	do {
		/^(\w+(?:::\w+)*)$/ or die "invalid module name: '$_'";
		push(@mod, $1);
	} foreach @$inmod;
	do {
		my $modname = $_;
		my $modkey = $modname;
		$modkey =~ s{::}{/}gs;
		$modkey .= '.pm';
		!exists($INC{$modkey}) or die "RequireENV('$modname') called but '$modname' already in \%INC!\n";
	} foreach @mod;
	my $envlog;
	my %env = %ENV;
	{
		my $logobj = tie(%ENV, '_Girocco::Dumper::HashLog', \%env);
		eval 'require '.$_.';1' or die $@ foreach @mod;
		$envlog = $$logobj[1];
	}
	untie(%ENV);
	%ENV = %env;
	return $envlog;
}

sub DumpENV {
	my $envlog = shift;
	my $result = "";
	$result .= &$_dumpenv($_, $envlog->{$_}) foreach &$_sortkeys($envlog);
	return $result;
}

sub FreezeConfig {
	my ($usemod, $conf, $sub) = @_;
	defined($conf) && $conf ne "" or $conf = 'Girocco::Config';
	defined($usemod) && $usemod ne "" or $usemod = [$conf, 'Girocco::Validator'];
	my $env = RequireENV($usemod);
	# To avoid problems with taint mode, these four are always removed
	$env->{IFS} = [];
	$env->{CDPATH} = [];
	$env->{ENV} = [];
	$env->{BASH_ENV} = [];
	# Always make sure PATH gets set explicitly to something
	exists($env->{PATH}) && @{$env->{PATH}} or $env->{PATH} = [GetConfPath()];
	if (exists($env->{PATH}) && @{$env->{PATH}}) {
		# special handling for $ENV{PATH}
		my $pval = ${$env->{PATH}}[0];
		eval '$'.$conf.'::path = $pval;1' or die $@;
	}
	&$sub($conf) if ref($sub) eq 'CODE';
	return "package $conf;\n" . Dump($conf) . DumpENV($env) .
		Boilerplate() . "1;\n";
}

sub GetConfPath {
	local ($ENV{IFS}, $ENV{CDPATH}, $ENV{ENV}, $ENV{BASH_ENV});
	local $ENV{PATH} = "/usr/bin:/bin";
	my $cs_path = qx(getconf PATH 2>/dev/null);
	defined($cs_path) or $cs_path = "";
	chomp $cs_path; $cs_path =~ s/^\s+//; $cs_path =~ s/\s+$//;
	$cs_path = join(":", grep(!m{/[.]}, grep(m{^/.},
		split(/\s*:+\s*/, $cs_path))));
	$cs_path =~ m{^(/.+)$} and $cs_path = $1;
	$cs_path ne "" or $cs_path = $ENV{PATH};
	return $cs_path;
}

1;

mkdir -p /home/repo/certs/acme &&
./acme.sh --install-cert -d repo.or.cz \
--cert-file /home/repo/certs/acme/girocco_www_crt.pem \
--key-file /home/repo/certs/acme/girocco_www_key.pem \
--fullchain-file /home/repo/certs/acme/girocco_www_fullchain.pem &&
sed '1,/^-----END/d' < /home/repo/certs/acme/girocco_www_fullchain.pem \
> /home/repo/certs/acme/girocco_www_chain.pem

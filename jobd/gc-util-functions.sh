#!/bin/sh
#
# This is a shell library for common gc related functions
# used by various Girocco scripts.

# shlib.sh always sets this, it's an error to source
# this script without having already sourced shlib.sh
[ -n "$var_git_exec_path" ] || exit 2

# include functions used by both update and gc
. @basedir@/jobd/updategc-util-functions.sh

# default packing options
packopts="--depth=50 --window=50 --window-memory=${var_window_memory:-1g}"
quiet="-q"; [ "${show_progress:-0}" = "0" ] || quiet=

# Create a gc.pid lockfile
# $1 => name of variable to store result in
# On success:
#   variable named by $1 will contain the name of the newly create lockfile (i.e. "gc.pid")
# On failure:
#   variable named by $1 will contain the failure reason
v_lock_gc() {
	v_lock_file "$1" "gc.pid"
}

# make sure combine-packs uses the correct Git executable
run_combine_packs() {
	PATH="$var_git_exec_path:$cfg_basedir/bin:$PATH" @basedir@/jobd/combine-packs.sh "$@"
}

# combine the input pack(s) into a new pack (or possibly packs if packSizeLimit set)
# input pack names are read from standard input one per line delimited by the first
# ':', ' ' or '\n' character on the line (which allows gfi-packs to be read directly)
# all arguments, if any, are passed to pack-objects as additional options
# first removes any pre-existing "*.zap*" sentinels that may be leftover from any
# previously aborted "--replace" operations
# returns non-zero on failure
combine_packs_std() {
	find -L objects/pack -maxdepth 1 -type f -name '*.zap*' -exec rm -f '{}' + || :
	run_combine_packs --replace "$@" $packopts --all-progress-implied $quiet --non-empty
}

# current directory must already be set to the $GIT_DIR
# see if there are "lotsa" loose objects
# "lotsa" is defined as the 17, 68, 71 and 86 object directories existing
# and there being at least 5 total objects between them which corresponds
# to an approximate average of 320 loose objects before this function starts
# returning true and triggering a "mini" gc to pack up loose objects
lotsa_loose_objects() {
	[ -d objects/17 ] && [ -d objects/68 ] && [ -d objects/71 ] && [ -d objects/86 ] || return 1
	_objs=$(( $(find -L objects/17 objects/68 objects/71 objects/86 -maxdepth 1 -name "$octet19*" -type f -print 2>/dev/null | LC_ALL=C wc -l) ))
	[ ${_objs:-0} -ge 5 ]
}

# same as lotsa_loose_objects but first runs `git prune-packed` if it can get a gc lock
lotsa_loose_pruned_objects() {
	lotsa_loose_objects || return $?
	v_lock_gc _gclock || return 0
	git prune-packed --quiet
	rm -f "$_gclock"
	lotsa_loose_objects
}

# a "single object" pack is either a pack containing just one object
# or a pack containing zero objects (which is a degenerate case that
# normally can never happen).  And it must have a name matching
# the pack-<sha1>*.pack pattern where either the "infix" suffix is "_l"
# or the name does not contain any "_" characters at all (and, obviously,
# must have a matching .idx file).  Any .keep, .bundle, or .bitmap
# associated packs are automatically excluded from the count.
# "lotsa" here is defined as 20 or more.
lotsa_single_object_packs() {
	__lpo="--exclude-no-idx --exclude-keep --exclude-bitmap --exclude-bndl"
	_lpo01="$__lpo --exclude-limit 2"
	# "$0=substr($0,19,length-23)" strips the leading "objects/pack/pack-" and trailing ".pack"
	_sopacks="$(
		list_packs --quiet $_lpo01 objects/pack 2>/dev/null |
		LC_ALL=C awk 'BEGIN {c=0} {$0=substr($0,19,length-23)} !/_/ || /^[0-9a-f]*_l$/ {c+=1} END {print c}'
	)" || :
	[ ${_sopacks:-0} -ge 20 ]
}

# returns true if either lotsa_loose_pruned_objects or lotsa_single_object_packs is true
lotsa_loose_objects_or_sopacks() {
	lotsa_single_object_packs || lotsa_loose_pruned_objects
}

# pack any existing, non-packed loose objects into a new _l.pack file then run prune-packed
# note that prune-packed is NOT run beforehand -- the caller must do that if needed
# loose objects need not be part of complete commits/trees as --weak-naming is used
# if there end up being too many loose packs, attempt to combine the packs too
pack_incremental_loose_objects() {
	_lpacks="$(run_combine_packs </dev/null --names --loose --weak-naming --incremental --non-empty --all-progress-implied ${quiet:---progress} $packopts)"
	if [ -n "$_lpacks" ]; then
		# We need to identify these packs later so we don't combine_packs them
		for _objpack in $_lpacks; do
			rename_pack "objects/pack/pack-$_objpack" "objects/pack/pack-${_objpack}_l" || :
		done
		git prune-packed $quiet
	fi
	_packs=
	__lpo="--exclude-no-idx --exclude-keep --exclude-bitmap --exclude-bndl"
	_lpo01="$__lpo --exclude-limit 2"
	_lpol="$__lpo --exclude-no-sfx _l"
	list_packs --quiet $_lpo01 objects/pack 2>/dev/null |
	while read -r _apack && _apack="${_apack%.pack}" && [ -n "$_apack" ]; do
		case "$_apack" in *_*);;*)
			rename_pack "$_apack" "${_apack}_l" || :
		esac
	done || :
	{ _packs="$(list_packs --quiet --count $_lpol objects/pack || :)" || :; } 2>/dev/null
	[ "${_packs:-0}" -lt 20 ] || {
		combine_small_incremental_loose_packs
		_packs=
		{ _packs="$(list_packs --quiet --count $_lpol objects/pack || :)" || :; } 2>/dev/null
		[ "${_packs:-0}" -lt 20 ] || combine_large_incremental_loose_packs
	}
}

# same as pack_incremental_loose_objects except
# returns true if locked and packed and unlocked or
# false if could not lock (with err in $lockerr)
pack_incremental_loose_objects_if_lockable() {
	if v_lock_gc _gclock; then
		pack_incremental_loose_objects || :
		rm -f "$_gclock"
		return 0
	else
		lockerr="$_gclock"
		return 1
	fi
}

# combine small _l packs into larger pack(s) using --weak-naming
# we avoid any non _l, keep, bndl or bitmap packs
combine_small_incremental_loose_packs() {
	_lpo="--exclude-no-idx --exclude-keep --exclude-bitmap --exclude-bndl"
	_lpo="$_lpo --exclude-no-sfx _l"
	_lpo="$_lpo --quiet --object-limit $var_redelta_threshold objects/pack"
	while
		_cnt="$(list_packs --count $_lpo)" || :
		test "${_cnt:-0}" -ge 2
	do
		_newp="$(list_packs $_lpo | combine_packs_std --names --weak-naming --no-reuse-delta)"
		# We need to identify these packs later so we don't combine_packs them
		for _objpack in $_newp; do
			rename_pack "objects/pack/pack-$_objpack" "objects/pack/pack-${_objpack}_l" || :
		done
		v_cnt _newc $_newp
		# be paranoid and exit the loop if we haven't reduced the number of packs
		[ $_newc -lt $_cnt ] || break
	done
	return 0
}

# combine large[ish] _l packs into larger pack(s) using --weak-naming
# we avoid any non _l, keep, bndl or bitmap packs
combine_large_incremental_loose_packs() {
	_lpo="--exclude-no-idx --exclude-keep --exclude-bitmap --exclude-bndl"
	_lpo="$_lpo --exclude-no-sfx _l"
	_lpo="$_lpo --quiet --exclude-limit -$(( ( $var_redelta_threshold / 2 ) + 1 )) objects/pack"
	while
		_cnt="$(list_packs --count $_lpo)" || :
		test "${_cnt:-0}" -ge 2
	do
		_newp="$(list_packs $_lpo | combine_packs_std --names --weak-naming)"
		# We need to identify these packs later so we don't combine_packs them
		for _objpack in $_newp; do
			rename_pack "objects/pack/pack-$_objpack" "objects/pack/pack-${_objpack}_l" || :
		done
		v_cnt _newc $_newp
		# be paranoid and exit the loop if we haven't reduced the number of packs
		[ $_newc -lt $_cnt ] || break
	done
	return 0
}

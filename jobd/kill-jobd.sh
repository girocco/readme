#!/bin/sh
#
# kill-jobd - kill -9 jobd.sh and remove the lockfile
#
# Simply adapt this for your crontab:
# * * * * * /usr/bin/find /tmp/jobd.lock -amin +15 -exec /home/repo/repomgr/jobd/kill-jobd.sh \;

echo "Sorry." | mail -s "Killing jobd on $HOSTNAME automagically. It has been hanging for 15 minutes, now." admin@repo.or.cz
kill -9 $(cat /tmp/jobd.lock) && rm /tmp/jobd.lock

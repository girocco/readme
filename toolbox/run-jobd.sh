#!/bin/sh

DEFAULTOPTS="-q -P --kill-after 1000"

test -n "$PATH" || PATH="$(/usr/bin/getconf PATH)"
export PATH
cd /
shbin=@shbin@

exec_login_shell() {
	# set arg 0 to have a leading '-' to force a login shell since
	# the -l option is shell-specific.  All shells must understand -i.
	exec @perlbin@ -e 'my $b=shift; exec $b @ARGV or die "Can\047t exec \"$b\": $!\n"' "${SHELL:-$shbin}" "-$(basename "${SHELL:-$shbin}")" -i
}

if [ "$1" = "--shell" ]; then
	shift
	trap 'trap - HUP INT QUIT ABRT PIPE ALRM TERM TSTP TTIN TTOU BUS ILL SEGV FPE TRAP XCPU XFSZ; exec_login_shell' EXIT
	trap '' HUP INT QUIT ABRT PIPE ALRM TERM TSTP TTIN TTOU BUS ILL SEGV FPE TRAP XCPU XFSZ
	(
		trap - HUP INT QUIT ABRT PIPE ALRM TERM TSTP TTIN TTOU BUS ILL SEGV FPE TRAP XCPU XFSZ
		/usr/bin/nice -n 18 @perlbin@ -I@basedir@/jobd @basedir@/jobd/jobd.pl --same-pid $DEFAULTOPTS "$@"
	)
	exit
fi
exec /usr/bin/nice -n 18 @perlbin@ -I@basedir@/jobd @basedir@/jobd/jobd.pl --same-pid $DEFAULTOPTS "$@"

#!/bin/sh
# Checking for inactive projects (empty repositories)

echo "Not ready for usage yet; have to figure out locking and check conditions --jast"
exit 99

. @basedir@/shlib.sh

PRETEND=
OUT="$cfg_reporoot/.girocco-remove-list"
if [ "$1" = "-n" ]; then
	shift
	PRETEND=yes
	OUT=/dev/stdout
fi

if [ -z "$1" ]; then
	cat <<-EOM
	Dear person running this, please pass 'Your Name <email@address>' as the
	first argument.
	EOM
	exit 1
fi

if [ -z $PRETEND ]; then
	{sackfd}>$OUT
	flock -n $sackfd || {
		echo "Another cleanup process is running; aborting!"
		exit 1
	}
fi

cd "$cfg_reporoot"

NOW=`date +%s`
TOTAL=0

for i in $(find -L . -mindepth 3 -maxdepth 3 -name heads -type d); do
	# any branches?
	[ "$(ls $i)" ] && continue
	fb="${i##*/}"; fb="${fb%.git}"
	# any forks?
	[ -d "$i/../../../$fb" ] && continue
	[ -e "$i/../../packed-refs" ] && continue
	# less than 60 days old?
	[ $(($NOW - `stat -c %Y $i/../../branches`)) -le 5184000 ] && continue
	echo "${i%/refs/heads} $(git --git-dir="$i/../.." config gitweb.owner)"
	TOTAL=$(($TOTAL + 1))
done >$OUT

[ $PRETEND ] && exit

CURRENT=0

while read x y; do
	CURRENT=$(($CURRENT + 1))
	echo -en "\rSending notification mails [$CURRENT/$TOTAL]..."

continue

	mail -s "[$cfg_name] Automated project removal" $y <<-EOM
	Hello,
	
	I have removed your project '$x' from $cfg_name since you set it up
	quite some time ago and nothing has ever been pushed in (I run this job
	once every few months and it removes all unused projects older than two
	months). If you want to publish anything on $cfg_name in the future,
	feel free to create the project again. If you experienced any technical
	difficulties with publishing on $cfg_name, please let me know,
	hopefully we can solve them together (sometimes I don't reply for some
	time, in that case it's best to harass me again, and repeatedly ;-). In
	case the automated check went wrong, the repository is still backed
	up...
	
	Thanks for using $cfg_name!
	$1
	
	(You may also contact our support team at <$cfg_admin> which will
	probably get you faster answers.)
EOM
done <$OUT
echo " done."

exit

#for i in $(find -L . -name heads -type d); do [ "$(ls $i)" ] && continue; mv ${i%/refs/heads} "$cfg_reporoot/_recyclebin/"; done
printf "Moving repositories to recycle bin..."
while read x y; do
	f="${x%/*}"
	x="${x##*/}"
	[ "$f" != "$x" ] || f=""
	echo "$f .. $x"
	mkdir -p "$cfg_reporoot/_recyclebin/$f"
	mv "$cfg_reporoot/$f/$x" "$cfg_reporoot/_recyclebin/$f"
done <$OUT
echo " done."

printf "Deregistering projects..."
for i in `cut -d ' ' -f 1 $OUT`; do
	echo $i
	j=${i#./}
	j=${j%.git}
	perl -Icgi -MGirocco::Project -e 'Girocco::Project->ghost("'$j'", 0)->_group_remove()'
done
echo " done."

rm -f $OUT


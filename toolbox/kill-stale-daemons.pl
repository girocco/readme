#!/usr/bin/perl

use strict;
use warnings;
use lib "__BASEDIR__";

use Girocco::Config;
use Proc::ProcessTable;

use Data::Dumper;

my $week_ago = time-60*60*24*7;
my $t = new Proc::ProcessTable;

my $reporoot = $Girocco::Config::reporoot;
foreach my $p ( @{$t->table} ) {
	if ($p->start() < $week_ago &&
	    ($p->cmndline() =~ /git[- ]daemon --inetd --verbose --export-all --enable=upload-archive --base-path=\Q$reporoot\E/o ||
	     $p->cmndline() =~ /git[- ]upload-pack --strict --timeout=0 \./ ||
	     $p->cmndline() =~ /peek_packet/)) {
		print Dumper $p if $ENV{'PERL_DEBUG'};
		kill 9, $p->pid;
	}
}

#!/bin/sh

# Repack in reverse order, so that forked repositories get repacked first.
# That prevents objects from missing, since repack -l will automagically
# put the now-going-away objects into the forked repository's object store.

. @basedir@/shlib.sh

cd "$cfg_reporoot"
cut -d : -f 1 <"$cfg_chroot/etc/group" | grep -v "^_repo" |
	while read dir; do
		echo "* $dir" >&2
		if [ -e "$dir.git"/.nofetch ]; then
#			echo "+l $(date) $dir"
#			(cd "$dir.git/objects" && [ "$(ls ?? 2>/dev/null)" ] && GIT_DIR=".." git repack -A -d -q)
#			GIT_DIR="$dir.git" git update-server-info
#			echo "-l $(date) $dir"
:;
		else
			echo "+r $(date) $dir"
			"$cfg_basedir/jobd/update.sh" "$dir"
			echo "-r $(date) $dir"
		fi
	done 2>&1

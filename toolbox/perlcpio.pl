#!/usr/bin/env perl

# perlcpio.pl - write ASCII cpio archive

# Copyright (C) 2020 Kyle J. McKay.
# All rights reserved.
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

# Version 1.0.0

use strict;
use warnings;
use bytes;

use File::Basename qw(basename);
use Getopt::Long;
use Pod::Usage;
BEGIN {
	eval 'require Pod::Text::Termcap; 1;' and
	@Pod::Usage::ISA = (qw( Pod::Text::Termcap ));
	defined($ENV{PERLDOC}) && $ENV{PERLDOC} ne "" or
	$ENV{PERLDOC} = "-oterm -oman";
}
my $hascrc32;
BEGIN { $hascrc32 = eval 'use Compress::Zlib qw(crc32); 1;' }
my $me = basename($0);
close(DATA) if fileno(DATA);

exit(&main(@ARGV)||0);

my $die_nl; BEGIN { $die_nl = 0 }
sub dodie {
	my $msg = join(" ", @_);
	substr($msg, -1, 1) eq "\n" or $msg .= "\n";
	$die_nl and $msg = "\n" . $msg;
	$die_nl = 0;
	die $msg;
}
sub dowarn {
	my $msg = join(" ", @_);
	substr($msg, -1, 1) eq "\n" and substr($msg, -1, 1) = "";
	$die_nl and $msg = "\n" . $msg;
	!$die_nl and $msg .= "\n";
	print STDERR $msg;
}
sub dofn {
	my $msg = join(" ", @_);
	substr($msg, -1, 1) eq "\n" and substr($msg, -1, 1) = "";
	if ($die_nl) {
		print STDERR "\n", $msg;
	} else {
		$die_nl = 1;
		print STDERR $msg;
	}
}
sub donefn {
	my $msg = join(" ", @_);
	$msg eq "" || substr($msg, -1, 1) eq "\n" or $msg .= "\n";
	$die_nl and $msg = "\n" . $msg;
	$die_nl = 0;
	print STDERR $msg;
}

my (
	$opt_0, $opt_a, $opt_b, $opt_c, $opt_e, $opt_G, $opt_g, $opt_k, $opt_q,
	$opt_r, $opt_U, $opt_u, $opt_v, $opt_w, $opt_z
);

my $totalbyteswritten; BEGIN { $totalbyteswritten = 0 }
my $crc32; BEGIN { $crc32 = undef }
my $ugid_mask; BEGIN { $ugid_mask = 0177777 }

sub is_uint18 {
	my $opt = '-'.$_[0];
	$_[1] =~ /^[0-9]+$/ or die "$opt requires an unsigned integer argument\n";
	my $uint18 = 0 + $_[1];
	0 <= $uint18 && $uint18 <= 0777777 or die "$opt value must be in range 0..262143\n";
	return $uint18;
}

sub main {
	local *ARGV = \@_;
	select((select(STDERR),$|=1)[0]); # just in case
	Getopt::Long::Configure('bundling');
	GetOptions(
		'h' => sub {pod2usage(-verbose => 0, -exitval => 0)},
		'help' => sub {pod2usage(-verbose => 2, -exitval => 0)},
		'0' => \$opt_0,
		'a' => \$opt_a,
		'b' => \$opt_b,
		'c' => \$opt_c,
		'e' => \$opt_e,
		'G=s' => sub {$opt_G = is_uint18(@_)},
		'g=s' => sub {$opt_g = is_uint18(@_)},
		'k' => \$opt_k,
		'q|quiet' => \$opt_q,
		'r' => \$opt_r,
		'U=s' => sub {$opt_U = is_uint18(@_)},
		'u=s' => sub {$opt_u = is_uint18(@_)},
		'v' => \$opt_v,
		'w' => \$opt_w,
		'z' => \$opt_z,
	) && !@ARGV or pod2usage(-verbose => 0, -exitval => 2);
	$opt_w and $ugid_mask = 0777777;
	$opt_c && $opt_a and
		pod2usage(-msg => "$me: error: '-a' and '-c' may not be used together",
			-verbose => 0, -exitval => 2);
	$opt_b && $opt_r and
		pod2usage(-msg => "$me: error: '-b' and '-r' may not be used together",
			-verbose => 0, -exitval => 2);
	$opt_c && !$hascrc32 and
		die("$me: error: required Compress::Zlib module not found (needed for '-c')\n");

	binmode(STDIN);
	binmode(STDOUT);

	$opt_0 and $/ = "\0";
	while (my $line = <STDIN>) {
		chomp $line;
		$line =~ /\000/ and
			die "$me: error: NUL encountered in file name, did you forget the '-0'?\n";
		process_filename($line);
	}

	process_trailer() unless $opt_a;

	return 0;
}

my @filetypes; BEGIN { @filetypes = (
	0, # 0000000
	1, # 0010000 FIFO
	1, # 0020000 Char dev
	0, # 0030000
	1, # 0040000 Dir
	0, # 0050000
	1, # 0060000 Blk dev
	0, # 0070000
	1, # 0100000 File
	0, # 0110000 Reserved (C_ISCTG)
	1, # 0120000 Sym link
	0, # 0130000
	1, # 0140000 Socket
	0, # 0150000
	0, # 0160000
	0, # 0170000
)}

my $filenum; { $filenum = 0 }
my %hardlinks; { %hardlinks = () }

# NOTE: 077777777777 == 8589934591
# On a 32-bit system both bitand (&) and sprintf (%o) are
# limited to 32 bits.  The result of this next bitand (&)
# will be 4294967295 on a 32-bit system, NOT 8589934591
my $maxfsize; BEGIN { $maxfsize = 8589934591 & 8589934591 }

sub nextfilenum($) {
	++$filenum;
	$filenum <= 68719476735 or # 0777777777777 is 68719476735
		die "$me: error: $_[0] counter overflowed 68719476735 limit\n";
	return $filenum;
}

sub process_filename {
	my $fn = shift;
	if (length($fn) >= 0777777) { # need to leave one for the NUL
		die "$me: error: pathname too long (>262142 chars): $fn\n" unless $opt_k;
		warn "$me: warning: pathname too long (>262142 chars), skipping: $fn\n" unless $opt_q;
		return; # skip it
	}
	my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,
	$atime,$mtime,$ctime,$blksize,$blocks);
	{
		no warnings 'newline';
		($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,
		$atime,$mtime,$ctime,$blksize,$blocks) = lstat($fn);
	}
	if (!defined($mtime)) {
		die "$me: error: could not lstat \"@{[vis($fn)]}\": $!\n" unless $opt_k;
		warn "$me: warning: could not lstat \"@{[vis($fn)]}\", skipping: $!\n" unless $opt_q;
		return; # skip it
	}
	if (($mode & ~07777) == 0100000) {
		no warnings 'newline';

		# There is, conceivably, a miniscule window after the lstat
		# where a regular file could be replaced with something else
		# that we then open here.  There are various possible
		# scenarios.  The only one we currently handle is when what
		# we successfully opened is not a regular file and the open
		# succeeded (in which case we just close the handle).

		open FILEDATA, '<', $fn or do {
			die "$me: error: unable to read-only open \"$fn\": $!\n" unless $opt_k;
			warn "$me: error: unable to read-only open \"$fn\", skipping: $!\n" unless $opt_q;
			return; # skip it
		};
		binmode(FILEDATA);
		($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,
		$atime,$mtime,$ctime,$blksize,$blocks) = stat(FILEDATA);
		defined($mtime) or do {
			die "$me: error: could not stat read-only opened \"@{[vis($fn)]}\": $!\n" unless $opt_k;
			warn "$me: error: could not stat read-only opened \"@{[vis($fn)]}\", skipping: $!\n" unless $opt_q;
			close(FILEDATA);
			return; # skip it
		};
		close(FILEDATA) if ($mode & ~07777) != 0100000; # it changed out from under us
	}
	my $ftype = $mode & ~07777;
	my $data = undef;
	if ($ftype == 0120000) {
		$size <= $maxfsize or
			die "$me: error: symlink size too large ($size > $maxfsize): $fn\n";
		defined($data = readlink($fn)) or do {
			die "$me: error: unable to read symlink \"$fn\": $!\n" unless $opt_k;
			warn "$me: warning: unable to read symlink \"$fn\", skipping: $!\n" unless $opt_q;
			return; # skip it
		};
		length($data) == $size or do {
			die "$me: error: unable to correctly read symlink: \"$fn\"\n" unless $opt_k;
			warn "$me: warning: unable to correctly read symlink: \"$fn\", skipping\n" unless $opt_q;
			return; # skip it
		};
		$rdev = 0;
		$nlink = 1;
	} elsif ($ftype == 0100000) {
		# leaving $data undef means write the contents of the file
		$size <= $maxfsize or
			die "$me: error: file size too large ($size > $maxfsize): $fn\n";
		$rdev = 0;
	} elsif ($ftype == 0040000) {
		$nlink = 2 if $opt_e;
		$rdev = 0;
		$data = ""; # no data for directories
	} else {
		die "$me: error: unrecognizable file mode type for \"$fn\": " .
			sprintf("0%06o\n", $ftype) if
			$ftype != ($ftype & 0170000) || !$filetypes[$ftype >> 12];
		$data = ""; # no data to write
	}
	$opt_z && $ftype != 0100000 and $dev = $ino = 0;
	if (!$opt_z || $ftype == 0100000) {
		if ($opt_b) {
			nextfilenum("hard link breakage");
			$dev = $filenum >> 18;
			$ino = $filenum & 0777777;
			$nlink = 1 if $ftype == 0100000;
		} elsif ($opt_r) {
			($ino & 0777777) == $ino or do {
				die "$me: error: st_ino value (@{[sprintf('0x%X',$ino)]}) outside 18-bit limit: $fn\n" unless $opt_k;
				warn "$me: warning: st_ino value (@{[sprintf('0x%X',$ino)]}) outside 18-bit limit, truncated: $fn\n" unless $opt_q;
			};
			($dev & 0777777) == $dev or do {
				die "$me: error: st_dev value (@{[sprintf('0x%X',$dev)]}) outside 18-bit limit: $fn\n" unless $opt_k;
				warn "$me: warning: st_dev value (@{[sprintf('0x%X',$dev)]}) outside 18-bit limit, truncated: $fn\n" unless $opt_q;
			};
		} else {
			if ($ftype != 0100000 || $nlink <= 1) {
				nextfilenum("unique file counter");
				$dev = $filenum >> 18;
				$ino = $filenum & 0777777;
				$nlink = 1 if $ftype == 0100000;
			} else {
				my $key = '' . $dev . ',' . $ino;
				if (!exists($hardlinks{$key})) {
					nextfilenum("unique file counter");
					$dev = $filenum >> 18;
					$ino = $filenum & 0777777;
					$hardlinks{$key} = [ $filenum, $size ];
				} else {
					my ($hlfnum, $hlsize) = @{$hardlinks{$key}};
					if ($size == $hlsize) {
						$dev = $hlfnum >> 18;
						$ino = $hlfnum & 0777777;
					} else {
						die "$me: error: hard-linked file size ($size) does not match original ($hlsize) for: $fn\n" unless $opt_k;
						warn "$me: warn: hard-linked file size ($size) does not match original ($hlsize), breaking hard-link for: $fn\n" unless $opt_q;
						$nlink = 1;
						nextfilenum("unique file counter");
						$dev = $filenum >> 18;
						$ino = $filenum & 0777777;
					};
				}
			}
		}
	}
	defined($data) and $size = length($data);
	$mtime = $maxfsize if $mtime > $maxfsize;
	$mtime = 0 if $mtime < 0;
	$nlink = 07777777 if $nlink > 07777777;
	if (defined($opt_U)) {
		$uid = $opt_U;
	} else {
		$uid = defined($opt_u) ? $opt_u : ($uid & $ugid_mask)
			if ($uid & $ugid_mask) != $uid;
	}
	if (defined($opt_G)) {
		$gid = $opt_G;
	} else {
		$gid = defined($opt_g) ? $opt_g : ($gid & $ugid_mask)
			if ($gid & $ugid_mask) != $gid;
	}
	my $hdr = sprintf("070707%06o%06o%06o%06o%06o%06o%06o%011o%06o%011o%s\0",
		($dev & 0777777), ($ino & 0777777), ($mode & 0777777),
		($uid & 0777777), ($gid & 0777777), $nlink,
		($rdev & 0777777), $mtime, (length($fn) + 1), $size, $fn);
	dofn($fn) if $opt_v;
	writeall($hdr) or
		dodie "$me: error: writeall failed writing cpio file header: $!\n";
	$crc32 = crc32($hdr, $crc32) if $opt_c;
	$totalbyteswritten += length($hdr);
	if (defined($data) && $data ne "") {
		writeall($data) or
			dodie "$me: error: writeall failed writing cpio file data: $!\n";
		$crc32 = crc32($data, $crc32) if $opt_c;
	} elsif (!defined($data)) {
		# we already "grabbed" a hold of the file's data earlier by opening FILEDATA
		# to make sure that it couldn't disappear on us after writing the "cpio Header"
		my $fsize = 0;
		my $bytes;
		do {
			my $buf = '';
			$bytes = sysread(FILEDATA, $buf, 32768);
			defined($bytes) or
				dodie "$me: error: error reading \"$fn\": $!\n";
			$bytes == 0 || writeall($buf) or
				dodie "$me: error: writeall failed writing file data: $!\n";
			$crc32 = crc32($buf, $crc32) if $opt_c && $bytes;
			$fsize += $bytes;
		} while ($bytes != 0);
		close FILEDATA;
		$size == $fsize or
			dodie "$me: error: file stat size $size != actual data size $fsize for: $fn\n";
	}
	$totalbyteswritten += $size;
	donefn if $opt_v;
	return 1;
}

sub writeall {
	my $offset = 0;
	my $towrite = length($_[0]);
	while ($towrite) {
		my $bytes = syswrite(STDOUT, $_[0], $towrite, $offset);
		defined($bytes) or return 0;
		last if $bytes == 0;
		$offset += $bytes;
		$towrite -= $bytes;
	}
	!$towrite or die "$me: error: EOF encountered writing STDOUT\n";
	return 1;
}

sub vis {
	my $s = shift;
	$s =~ s/\n/\\n/gs;
	$s =~ s/\0/\\000/gs;
	return $s;
}

sub process_trailer {
	my $hdr = sprintf("070707%06o%06o%06o%06o%06o%06o%06o%011o%06o%011o%s\0",
		0, 0, 0, 0, 0, 1, 0, 0, 11, 0, "TRAILER!!!");
	$totalbyteswritten += length($hdr);
	if ($opt_c) {
		my $llen = '';
		my $size = $totalbyteswritten;
		while ($size) {
			$llen .= pack('C', $size & 0xff);
			$size = int($size / 256);
		}
		$hdr .= $llen;
		$crc32 = crc32($hdr, $crc32);
		$crc32 ^= 0xffffffff;
		for (my $i = 4; $i; --$i) {
			$hdr .= pack('C', $crc32 & 0xff);
			$crc32 >>= 8;
		}
		$totalbyteswritten += length($llen) + 4;
	}
	my $needtowrite = 512 * int(($totalbyteswritten + 511) / 512);
	$hdr .= pack('C', 0) x ($needtowrite - $totalbyteswritten) if
		$needtowrite > $totalbyteswritten;
	writeall($hdr) or
		die "$me: error: writeall failed writing cpio file trailer: $!\n";
	return 1;
}

__DATA__

=head1 NAME

perlcpio.pl - write ASCII cpio archive

=head1 SYNOPSIS

B<perlcpio.pl> [options] > I<< <cpio-output-file> >>

 Options:
   -h                           show short usage help
   --help                       show long detailed help
   -0                           use \0 instead of \n reading input
   -a                           omit trailer from output
   -b                           break all hard links in output
   -c                           append checksum to end
   -e                           empty directory link count
   -G <gid>                     always set c_gid value to <gid>
   -g <gid>                     set c_gid value to <gid> on overflow
   -k                           keep going (when possible) after errors
   -q                           quiet all non-fatal messages
   -r                           record raw st_dev and st_ino values
   -U <uid>                     always set c_uid value to <uid>
   -u <uid>                     set c_uid value to <uid> on overflow
   -v                           be verbose while writing archive
   -w                           allow wider st_uid and st_gid values
   -z                           zero non-file st_dev and st_ino values

=head1 DESCRIPTION

B<perlcpio.pl> reads a list of file names from standard input and then
writes one "POSIX.1-2001 IEEE Std 1003.1-2001 SUSv3 cpio Interchange Format"
"cpio Header" followed by the contents of the file for each file name read
from standard input.

The blocking size is fixed at 512 bytes (the minimum allowed).

Essentially B<perlcpio.pl> is the equivalent of this command:

 pax -wd -b 512 -x cpio

However, uid and gid values are truncated to 16-bits rather than producing
an error.

=head1 OPTIONS

=over

=item B<-a>

Leave the output "appendable" by omitting the trailer and any needed
following zero padding.  May I<NOT> be used together with B<-c>.

=item B<-b>

Break all hard links in the output stream.  In other words, the
"c_nlink" field will be forced to 1 for files and no two files will
share the same pair of "c_dev" and "c_ino" values in the output.

Even if two files are hard-linked to one another and that hard
linkage is recorded properly in a cpio archive (option B<-b> was
I<NOT> used), the entire file's data will be I<duplicated> in the
archive for each hard link!

It is only during extraction that an attempt will be made to re-create
the hard linkages (which may, or may not, succeed) and eliminate
the redundancy at that time.  The archive will always remain bloated
with multiple copies of the duplicated data!

Best to archive symbolic links instead of hard links when feasible
to reduce unnecessary bloating of the size of the output archive.

The B<-b> option is not compatible with the B<-r> option.

=item B<-c>

Append the minimum number of bytes needed to represent the little-endian
length of the data written up through the NUL byte in the "TRAILER!!!"
string and then the appropriate 32-bit CRC value plus zero padding up to
the next 512-byte boundary.

When such a file is passed through zlib's crc32 checksum routine, the output
(if the file remains uncorrupted) is always 0xffffffff.

This option may I<NOT> be used together with B<-a>.

=item B<-e>

Make the "c_nlink" count of all directories in the output "empty".
In other words, make it have the value 2.  (For the "." and ".."
entries of an empty directory.)

This can be useful when the number of entries in a directory being
archived changes but the actual contents being written to the archive
do not.

=item B<-G> I<< <gid> >>

Force all items in the output archive to have "c_gid" set to I<< <gid> >>.
The given I<< <gid> >> value must be in the range 0..0777777 (262143).

Always overrides any B<-g> option.

=item B<-g> I<< <gid> >>

Instead of truncating "st_gid" values that would overflow, substitute
I<< <gid> >> instead.   That means than any "st_gid" value less than 0
or greater than 65535 (withOUT B<-w>) or 262143 (WITH B<-w>) will be
replaced with I<< <gid> >> in the "c_gid" field of the item in the
output archive.

This option only affects values that would otherwise be truncated, any
"st_gid" values that would not be truncated are left unchanged by this
option.

=item B<-k>

Normally certain kinds of errors immediately abort production of an
output archive (leaving the partial result quite possibly unusable).

With B<-k>, certain errors are turned into warnings by performing an
action that allows the archival process to continue.  In particular:

=over

=item hard-linked file size mismatch

When NEITHER B<-r> NOR B<-b> is in effect, if a file to be archived
with a hard link to an earlier file in the archive has a different
size than that earlier file it's hard-linked to, a fatal error would
normally occur and abort the archival process.  With B<-k> the
hard-link is broken for the size-mismatched file and archiving
continues with a warning (but see B<-q>).

=item raw (B<-r>) mode value outside 18-bit limit

Normally when B<-r> is in effect, any "st_dev" or "st_ino" value
that does not fit into 18 bits causes an abort of the archival
process.  With B<-k> the value will be truncated with a warning and
archiving continues (but see B<-q>).

=item file not found or file/symlink unreadable

Normally a file not found error immediately aborts the archival
process.  With B<-k>, the file will be skipped instead (with a
warning, but see B<-q>).  Similarly for a symlink that cannot be
read or a file that cannot be opened for read access.

Note that it is not possible to keep going when a read error occurs
while copying file data into the archive nor when the file data
copied into the archive is a different length than the original
stat of that file.  (The "cpio Header" will have already been written
by then and it cannot just be taken back in order to skip the file.)

=back

=item B<-q>

Quiet non-fatal error messages.  Any warning message that is not fatal
will be suppressed with this option.

If B<-k> is also in effect, the messages that would have been fatal
errors without B<-k> are suppressed as well.

=item B<-r>

Record raw "st_dev" and "st_ino" values in the output archive.  A
failure will occur if either value does not fit in 18 bits.

The default (if neither the B<-b> nor the B<-r> option is given)
is to re-map the "st_dev" and "st_ino" values to new values in order
to preserve hard links while avoiding any overflow that might result
in an unintentional collision with other files.

The B<-r> option is not compatible with the B<-b> option.

=item B<-U> I<< <uid> >>

Force all items in the output archive to have "c_uid" set to I<< <uid> >>.
The given I<< <uid> >> value must be in the range 0..0777777 (262143).

Always overrides any B<-u> option.

=item B<-u> I<< <uid> >>

Instead of truncating "st_uid" values that would overflow, substitute
I<< <uid> >> instead.   That means than any "st_uid" value less than 0
or greater than 65535 (withOUT B<-w>) or 262143 (WITH B<-w>) will be
replaced with I<< <uid> >> in the "c_uid" field of the item in the
output archive.

This option only affects values that would otherwise be truncated, any
"st_uid" values that would not be truncated are left unchanged by this
option.

=item B<-v>

Write each filename to STDERR as its entry is being written to
STDOUT.

=item B<-w>

Normally the "st_uid" and "st_gid" values are truncated to 16 bits.
With B<-w> they will be truncated to fit which means 18 bits instead
of 16 bits.

=item B<-z>

Write a 0 value for the "c_dev" and "c_ino" values of all but regular
files.

=back

=head1 DETAILS

The generated output consists of the following for each input file:

 +-------------+------/.../------+
 | cpio Header | file data bytes |
 +-------------+------/.../------+

followed by the trailer and zero or more zero pad bytes:

 +--------------+-------/.../-------+
 | cpio Trailer | zero byte padding | <-- 512-byte boundary
 +--------------+-------/.../-------+

The "cpio Trailer" is just a "cpio Header" with all fields set to
ASCII "0"s except:

 c_magic                        "070707" (usual c_magic value)
 c_nlink                        "000001" (1 link)
 c_namesize                     "000013" (length 11)
 c_name                         "TRAILER!!!\0"

The "cpio Header" has the following format:

 c_magic         6              ASCII 6-byte string "070707"
 c_dev           6              ASCII octal value of st_dev
 c_ino           6              ASCII octal value of st_ino
 c_mode          6              ASCII octal value of st_mode (see below)
 c_uid           6              ASCII octal value of st_uid
 c_gid           6              ASCII octal value of st_gid
 c_nlink         6              ASCII octal value of st_nlink
 c_rdev          6              ASCII octal value of st_rdev
 c_mtime        11              ASCII octal UNIX epoch seconds st_mtime
 c_namesize      6              ASCII octal length (including NUL) of c_name
 c_filesize     11              ASCII octal length of c_filedata
 c_name         c_namesize      NUL-terminated pathname string
 c_filedata     c_filesize      file or symlink data

For the C<c_mode> field, the least-significant 12 BITS (least-significant
4 octal digits) have the following meanings (in any bit-or'd combination):

  004000        Set uid
  002000        Set gid
  001000        The "sticky"/"text" bit

  000400        Readable by owner
  000200        Writable by owner
  000100        Executable by owner

  000040        Readable by group
  000020        Writable by group
  000010        Executable by group

  000004        Readable by other
  000002        Writable by other
  000001        Executable by other

The most-significant 6 BITS (most-significant 2 octal digits) have these
defined meanings exclusively and MUST NOT be combined (i.e. bit or'd together),
but may be bit-or'd with any combination of the least-significant 12 BITS above:

 0010000        FIFO / named pipe (i.e. "mkfifo")
 0020000        Character special device
 0040000        Directory
 0060000        Block special device
 0100000        Regular file
 0110000        Reserved (C_ISCTG)
 0120000        Symbolic link
 0140000        Socket (e.g. "socket(AF_UNIX,SOCK_STREAM,0)")

Any other value for the most-significant 2 octal digits has undefined behavior
(except, of course, in the "TRAILER!!!" where all ASCII "0"s are expected for
the c_mode field.)

=head1 LIMITATIONS

The `pax` option `-P` (physical) is always in effect.  In other words,
symbolic links are I<NEVER> followed.  (The `-P` option is the default
for the `pax` utility.)

If there are more than 68719476735 items in the archive, using option B<-b>
will result in a failure when the 68719476736th item is encountered.

When not using option B<-b> or option B<-r>, a failure will occur
when the 68719476736th unique (i.e. not a hard-link of an already
seen file) item is encountered.  Additionally, memory will be used
for an internal hard-link hash table that keeps track of all regular
files with an st_nlink value greater than 1 which will only be a
problem if there are an absurdly large number of hard-linked files!

The "st_rdev" value is always stored as 0 for regular files, directories
and symbolic links.  Otherwise it's always truncated.  Systems that use
a 32-bit value for "st_rdev" and store the major device number in the
high-order 8 bits will always record a truncated (and therefore useless)
"c_rdev" value unless the major device number happens to be 0.

The "st_nlink" value will be pinned to 0777777 if it exceeds that value.
But symbolic links always have their "c_nlink" value set to 1.

The "st_uid" and "st_gid" values are always truncated to 16 bits (default)
or 18 bits (with B<-w>).  But they can be overridden (B<-U> and/or B<-G>)
or substituted only when they overflow (B<-u> and/or B<-g>).

When storing subsequent copies of a hard-linked file and neither
option B<-b> nor option B<-r> has been used, while there is a fatal
error if the any of the subsequent copies of a hard-linked file
differ in size, there's no detection of differing modification times or
actual content.

Negative values for st_mtime are forced to 0.

Values for st_mtime > 077777777777 (037777777777 for 32-bit platforms)
are pinned to 077777777777 (037777777777 for 32-bit platforms).

The maximum supported file size is 077777777777 (037777777777 for 32-bit
platforms) in other words 8GiB - 1 (4GiB - 1 for 32-bit platforms) bytes.

Some 32-bit platform consumers of values in the range 020000000000 -
037777777777 may misbehave unless they are fully prepared to handle
values in that range.  This is especially true for the "c_mtime" field
when consumed by 32-bit platforms and may also apply to the "c_filesize"
field as well.

While an unsigned 31-bit number (0 .. 0x7FFFFFFF) can represent dates in
the range 1970-01-01T00:00:00Z .. 2038-01-19T03:14:07Z, an unsigned 32-bit
number can represent dates through 2106-02-07T06:28:15Z and an unsigned
33-bit number can represent dates through 2242-03-16T12:56:31Z.

If perl's "lstat" function returns the correct positive value for dates
after 2038-01-19T03:14:07Z then 32-bit perl platforms will correctly
store cpio "c_mtime" values through 2106-02-07T06:28:15Z and 64-bit
perl platforms will correctly store through 2242-03-16T12:56:31Z.

However, "st_mtime" values less than 0 (i.e. before 1970-01-01T00:00:00Z)
are always stored as 0 (meaning 1970-01-01T00:00:00Z).
Note that some platforms may interpret a "0" "st_mtime" value on a file as
meaning that it does not have a last modified timestamp available.

=head1 SEE ALSO

=over

=item POSIX.1-2001 IEEE Std 1003.1-2001 SUSv3 cpio Interchange Format

=item L<https://pubs.opengroup.org/onlinepubs/009695399/utilities/pax.html#tag_04_100_13_07>

=back

=head1 COPYRIGHT AND LICENSE

=over

=item Copyright (C) 2020 Kyle J. McKay

=item All rights reserved.

=back

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

=cut

#!/bin/sh
#
# update-all-hooks - Update all out-of-date hooks
# The old update-all-hooks.sh has been replaced by update-all-hooks.pl;
# it now simply runs that.

exec @basedir@/toolbox/update-all-hooks.pl "$@"

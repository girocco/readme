#!/usr/bin/perl

# create_projects_bom.pl - generate list of files to backup
# Copyright (C) 2020 Kyle J. McKay.  All rights reserved.
# License GPLv2+: GNU GPL version 2 or later.
# www.gnu.org/licenses/gpl-2.0.html
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.

use strict;
use warnings;
use vars qw($VERSION);
BEGIN {*VERSION = \'1.0.0'}
use lib "__BASEDIR__";
use Girocco::Config;
use Girocco::CLIUtil;
use Girocco::Project;

exit(&main(@ARGV)||0);

my @projparts;
BEGIN {@projparts = qw(
	.bypass
	.bypass_fetch
	.last_refresh
	.no_blob_plain
	.noconfig
	.nofetch
	.nogc
	.nohooks
	HEAD
	README.dat
	README.html
	config
	ctags
	description
	info
	info/lastactivity
	mob
	mob/HEAD
	mob/config
	mob/description
	mob/hooks
	mob/info
	mob/objects
	mob/packed-refs
	mob/refs
	mob/refs/heads
	mob/refs/mob
	objects
	objects/info
	objects/info/alternates
	packed-refs
	private
	private/HEAD
	private/config
	private/gc.pid
	private/objects
	private/packed-refs
	private/refs
	refs
)}

sub list_files($$) {
	my ($rdir, $pdir) = @_;
	my $odir = "$rdir/$pdir";
	opendir LISTDIR, "$odir" or return;
	my @tags = grep { !/^\./ && !/[,\s\/\\]/ && ! -l "$odir/$_" && -f "$odir/$_" } readdir(LISTDIR);
	closedir LISTDIR;
	print "$pdir/$_\n" foreach sort({lc($a) cmp lc($b) || $a cmp $b} @tags);
}

sub bom_for_project($) {
	my $rroot = $Girocco::Config::reporoot;
	my $proj = shift;
	print "$proj.git\n";
	-l "$rroot/$proj.git" and return;

	# For each entry from @projparts that exists, output
	# a line.  BUT, if it exists AND it's a symlink, SKIP
	# outputting a line for anything beneath it that's also
	# in @projparts.

	my %links = ();
	my $test = "$rroot/$proj.git";
	foreach (@projparts) {
		my $idx = index($_, "/");
		next if $idx > 0 && $links{substr($_, 0, $idx)};
		if (-l "$test/$_") {
			$links{$_} = 1;
			print "$proj.git/$_\n";
			next;
		} elsif ($_ eq "packed-refs") {
			next;
		} elsif (-e "$test/$_") {
			print "$proj.git/$_\n";
			list_files($rroot, "$proj.git/$_") if $_ eq "ctags";
		}
	}
}

sub main {
	local *ARGV = \@_;
	my %projects = map({$_ => 1} Girocco::Project::get_full_list());
	my @names;
	my $rroot = $Girocco::Config::reporoot;
	if (@ARGV) {
		my @list = ();
		foreach (@ARGV) {
			s/\.git$//i;
			$_ ne "" or next;
			$projects{$_} || (-l "$rroot/$_.git" && -d "$rroot/$_.git") or
				die "no such project: $_\n";
			push(@list, $_);
		}
		my %list = map({$_ => 1} @list);
		@names = sort({lc($a) cmp lc($b) || $a cmp $b} keys(%list));
	} else {
		if (opendir REPOROOT, $rroot) {
			my @links = grep {
				/^[^._]/ && s/\.git$//i &&
				-l "$rroot/$_.git" && -d "$rroot/$_.git"
			} readdir(REPOROOT);
			closedir REPOROOT;
			$projects{$_} = 1 foreach @links;
		}
		@names = sort({lc($a) cmp lc($b) || $a cmp $b} keys(%projects));
	}
	@names or die "no projects specified\n";
	#print map("$_\n", @names);
	bom_for_project($_) foreach @names;
	exit 0;
}

#!/bin/sh
#
# clear-all-htmlcache
# Remove all files present in the htmlcache subdirectory of each project.
# A count of projects that had files to be removed is displayed.

# The old clear-all-htmlcache.sh has been replaced by clear-all-htmlcache.pl;
# it now simply runs that.

exec @basedir@/toolbox/clear-all-htmlcache.pl "$@"

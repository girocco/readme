#!/usr/bin/perl

# sendmail.pl - sendmail to SMTP bridge
# Copyright (C) 2014,2015,2018,2021 Kyle J. McKay.
# All rights reserved.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;
use bytes;

use IPC::Open2;
use Net::Domain qw();

exit(&main()||0);

our $VERSION;
my $VERSIONMSG;
my $HELP;
my $USAGE;

BEGIN {
  *VERSION = \'1.0.4';
  $VERSIONMSG = "sendmail.pl version $VERSION\n" .
    "Copyright (C) 2014,2015,2018,2021 Kyle J. McKay.  All rights reserved.\n" .
    "License GPLv2+: GNU GPL version 2 or later.\n" .
    "http://gnu.org/licenses/gpl.html\n" .
    "This is free software: you are free to change and redistribute it.\n" .
    "There is NO WARRANTY, to the extent permitted by law.\n";
  my @a; /^(.*)$/s && push(@a, $1) foreach @ARGV; @ARGV=@a;
  $USAGE = <<USAGE;
Usage: sendmail.pl [-V] [-v] [-h] [-i] [-f addr] [-t] [recipient ...]
(Use sendmail.pl -v -h for extended help)
USAGE
  $HELP = <<HELP;
NAME
       sendmail.pl -- sendmail to SMTP bridge

SYNOPSIS
       sendmail.pl [-V] [-v] [-h] [-i] [-f addr] [-t] [recipient ...]

DESCRIPTION
       sendmail.pl provides semantics similar to the common sendmail executable
       and reads an incoming message to be delivered and then connects to
       the specified SMTP server to deliver it with the help of netcat.

       Only the most basic sendmail options are supported along with the most
       basic header support (when -t is given).

       The nc (netcat) executable used and options passed to it can be
       controlled via environment variables.

OPTIONS
       -V
              Show the sendmail.pl version.

       -v
              Verbose output.  If given before -h this help will be shown.

       -h
              Show basic usage help.

       -i
              Do not treat a line consisting of a single '.' as ending the
              input.

       -f addr
              Set the envelope header address.  This is the address that will
              be passed to the SMTP "MAIL FROM:" command.  If this option is
              not specified then the value of the LOGNAME environment variable
              will be used instead.  Some SMTP servers may perform validation
              on this address requiring a specific value here for the mail
              delivery to be successful.

       -t
              Read the list of recipients from the message's To:, Cc: and Bcc:
              headers.  If a Bcc: header is found, it's removed after picking
              up the destination addresses.

       -m
              Ignored for compatibility.

       --
              Signals the end of the options.  Anything following will be taken
              as a recipient address even if it starts with a '-'.

       recipient ...
              Zero or more recipient addresses to deliver the message to.  Each
              of these addresses will be passed to an SMTP "RCPT TO:" command.
              If both '-t' and one or more recipient addresses are given then
              the '-t' addresses will be added after the explicitly listed
              recipients.  Multiple recipients concatenated together using ','s
              and passed as a single recipient argument will be handled
              correctly.  At least one recipient must be given either
              explicitly or via the '-t' option.

ENVIRONMENT
       SENDMAIL_PL_NCBIN
              The netcat binary to use.  Must understand the -w secs option.
              If this is not set then "nc" is expected to be in the PATH.

       SENDMAIL_PL_NCOPT
              Additional options to pass to nc (netcat).  No other options
              besides -w are passed by default.  Should be a space-separated
              list of options complete with leading '-'.  For example:
                     export SENDMAIL_PL_NCOPT='-4'
              to force use of IPv4 addresses.

       SENDMAIL_PL_HOST
              The SMTP host to connect to.  If not set then "localhost" will
              be used.

       SENDMAIL_PL_PORT
              The SMTP host port to connect to.  If not set then 25 will be
              used.

BUGS
       The header parsing provided by the '-t' option may fail to pick up the
       correct recipient addresses if they use anything more than basic address
       syntax and/or any of the header lines are wrapped.

       Using environment variables to configure some of the settings may be a
       less common technique for tools of this sort.

VERSION
       sendmail.pl version $VERSION
       Copyright (c) 2014 Kyle J. McKay.  All rights reserved.
       License GPLv2+: GNU GPL version 2 or later.
       http://gnu.org/licenses/gpl.html
       This is free software: you are free to change and redistribute it.
       There is NO WARRANTY, to the extent permitted by law.

HELP
}

sub protect($)
{
  my $line = shift;
  return $line unless $line && $line =~ /^[.]/;
  return '.' . $line;
}

sub cleanup($)
{
  my $line = shift;
  defined($line) or $line = '';
  $line =~ s/(?:\r\n|\r|\n)$//os;
  return $line;
}

sub sanehost()
{
  my $tryhost = Net::Domain::hostname;
  $tryhost =~ s/\.$//;
  $tryhost =~ tr/A-Z/a-z/;
  return $tryhost if $tryhost =~ /[^.]\.local$/;
  $tryhost = Net::Domain::hostfqdn;
  $tryhost =~ s/\.$//;
  return $tryhost
}

sub main
{
  $ENV{'PATH'} =~ /^(.*)$/ and $ENV{'PATH'} = $1;
  my $done = 0;
  my $opt_v = 0;
  my $opt_i = 0;
  my $opt_t = 0;
  my $opt_f = $ENV{'LOGNAME'} || 'nobody';
  $opt_f =~ /^(.*)$/ and $opt_f = $1;
  my $hn = sanehost;
  my @rcpts = ();
  while (@ARGV && $ARGV[0] =~ /^-/) {
    my $opt = shift @ARGV;
    last if $opt eq '--';
    next if $opt eq '-m' || $opt eq '-om';
    print($VERSIONMSG), exit 0 if $opt eq '-V' || $opt eq '--version';
    $opt_v = 1, next if $opt eq '-v' || $opt eq '--verbose';
    print(($opt_v?$HELP:$USAGE),"\n"), exit 0 if $opt eq '-h' || $opt eq '--help';
    die "$USAGE\n" if $opt eq '-f' && !@ARGV;
    $opt_f = shift @ARGV, next if $opt eq '-f';
    $opt_f = $1, next if $opt =~ /^-f(.+)$/;
    $opt_i = 1, next if $opt eq '-i' || $opt eq '-oi' || $opt eq '-oitrue';
    $opt_t = 1, next if $opt eq '-t';
    $opt_i = $opt_t = 1, next if $opt eq '-ti' || $opt eq '-it';
    die "Unknown option: $opt\n$USAGE\n";
  }
  $opt_f =~ s/^[ \t]*<//; $opt_f =~ s/>[ \t]*$//;
  $opt_f =~ s/^[ \t]+//; $opt_f =~ s/[ \t]+$//;
  $opt_f .= '@'.$hn if $opt_f && $opt_f !~ /\@/; # some servers require @domain
  $opt_f = '<' . $opt_f . '>';
  foreach my $rcpt (split(/,/, join(',', @ARGV))) {
    $rcpt =~ s/^[ \t]*<//; $rcpt =~ s/>[ \t]*$//;
    $rcpt =~ s/^[ \t]+//; $rcpt =~ s/[ \t]+$//;
    $rcpt .= '@'.$hn if $rcpt && $rcpt !~ /\@/; # some servers require @domain
    push(@rcpts, '<' . $rcpt . '>') if $rcpt;
  }
  @ARGV = ();

  die "sendmail.pl: error: no recipients specified\n$USAGE\n"
    unless @rcpts || $opt_t;

  my @headers = ();
  my $lasthdr = '';
  my $extraline = '';
  for (;;) {
    my $line = <>;
    $line = undef if !$opt_i && $line =~ /^[.][\r\n]*$/;
    $done = 1, last unless defined($line);
    $line =~ s/(?:\r\n|\r|\n)$//os;
    $line =~ s/[ \t]+$//;
    if ($lasthdr && $line =~ /^[ \t]+(.*)$/) {
      # Unfold
      $lasthdr .= ' ' . $1;
      next;
    }
    push(@headers, $lasthdr) if $lasthdr;
    $lasthdr = '';
    if ($line =~ /^[\x21-\x39\x3b-\x7e]+:/) {
      $lasthdr = $line;
      next;
    }
    $extraline = $line;
    last;
  }
  push(@headers, $lasthdr) if $lasthdr;

  if ($opt_t) {
    foreach my $hdr (@headers) {
      if ($hdr =~ /^(?:To|Cc|Bcc):[ \t]*(.*)$/osi) {
        my $alist = $1;
        # Very crude parsing here
        $alist =~ s/[(].*?[)]//go; # Dump comments
        $alist =~ s/["].*?["]//go; # Dump quoted
        $alist =~ s/[ \t]+,/,/go;  # Kill extra
        $alist =~ s/,[ \t]+/,/go;  # spaces
        foreach my $adr (split(/,/, $alist)) {
          my $rcpt = '';
          if ($adr =~ /<([^ \t>]+)>/) {
            $rcpt = $1;
          } elsif ($adr =~ /^([^ \t]+)$/) {
            $rcpt = $1;
          }
          $rcpt .= '@'.$hn if $rcpt && $rcpt !~ /\@/; # some servers require @domain
          push(@rcpts, '<' . $rcpt . '>') if $rcpt;
        }
      }
    }
  }

  my $ncbin = $ENV{'SENDMAIL_PL_NCBIN'} || 'nc';
  $ncbin =~ /^(.*)$/ and $ncbin = $1;
  my $ncopt = $ENV{'SENDMAIL_PL_NCOPT'} || '';
  $ncopt =~ /^(.*)$/ and $ncopt = $1;
  my @ncopts = ();
  @ncopts = split(' ', $ncopt) if $ncopt;
  my $nchost = $ENV{'SENDMAIL_PL_HOST'} || 'localhost';
  $nchost =~ /^(.*)$/ and $nchost = $1;
  my $ncport = $ENV{'SENDMAIL_PL_PORT'} || '25';
  $ncport =~ /^(.*)$/ and $ncport = $1;
  my @cmd = ();
  push(@cmd, $ncbin, '-w', '30', @ncopts, $nchost, $ncport);

  die "sendmail.pl: error: no recipients specified\n" unless @rcpts;

  my ($send, $recv);
  (my $pid = open2($recv, $send, @cmd))
    or die "sendmail.pl: error: nc failed: $!\n";

  my $resp;
  defined($resp = <$recv>) && $resp =~ /^220 /
    or die "sendmail.pl: error: failed to receive initial SMTP 220 response\n";
  print $send "HELO localhost\r\n";
  defined($resp = <$recv>) && $resp =~ /^250 /
    or die "sendmail.pl: error: failed to receive SMTP HELO 250 response\n";

  print $send "MAIL FROM: $opt_f\r\n";
  defined($resp = <$recv>) && $resp =~ /^250 /
    or die "sendmail.pl: error: SMTP MAIL FROM: $opt_f failed\n";
  foreach my $rcpt (@rcpts) {
    print $send "RCPT TO: $rcpt\r\n";
    defined($resp = <$recv>) && $resp =~ /^250 /
      or die "sendmail.pl: error: SMTP RCPT TO: $rcpt failed\n";
  }

  print $send "DATA\r\n";
  defined($resp = <$recv>) && $resp =~ /^354 /
    or die "sendmail.pl: error: SMTP DATA failed\n";
  foreach my $hdr (@headers) {
    print $send "$hdr\r\n" unless $opt_t && $hdr =~ /^Bcc:/i;
  }
  print $send "\r\n";
  print $send protect($extraline), "\r\n" if $extraline;

  if (!$done) {
    while (my $line = <>) {
      $line =~ s/(?:\r\n|\r|\n)$//os;
      last if !$opt_i && $line =~ /^[.]$/;
      print $send protect($line), "\r\n";
    }
  }

  print $send ".\r\n";
  defined($resp = <$recv>) && $resp =~ /^250 /
    or die "sendmail.pl: error: SMTP message not accepted (@{[cleanup($resp)]})\n";

  print $send "QUIT\r\n";
  $resp = <$recv>; # Should be /^221 / for goodbye, but we don't really care
  close $send;
  close $recv;
  exit 0;
}

#!/bin/sh

set -e

. @basedir@/shlib.sh

base="${1#$cfg_reporoot}"
base="${base#/}"
base="${base%/}"
base="${cfg_gitweburl%/}/$base"
base="${base#*://}"
base="/${base#*/}"
fp="${base%/}"
img="${base%/}/blob_plain/HEAD:"
base="${base%/}/blob/HEAD:"
max="${cfg_max_readme_size}"

PATH="$var_git_exec_path:$cfg_basedir/bin:/usr/bin:$PATH"
export PATH

exec "$cfg_basedir/bin/format-readme" -e ${max:+-m} $max -b "$fp" -p "$base" -i "$img" "$@"

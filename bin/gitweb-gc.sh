#!/bin/sh
# Allow gitweb.cgi to be run from a fastcgi server that does
# not really support periodic "garbage collection" exits
# (even on an exit status of 0) without losing fastcgi requests.
# Signals are forwarded in both directions.
cgiroot=@cgiroot@
bg=
handle_sig() { [ -z "$bg" ] || kill -$1 "$bg" 2>/dev/null; }
# backgrounding the cgi with '&' causes it to ignore SIGINT and SIGQUIT
# therefore we redirect INT to TERM and QUIT to ABRT when forwarding those
trap 'handle_sig HUP' HUP
trap 'handle_sig ABRT' ABRT QUIT
trap 'handle_sig PIPE' PIPE
trap 'handle_sig ALRM' ALRM
trap 'handle_sig TERM' TERM INT
trap 'handle_sig USR1' USR1
trap 'handle_sig USR2' USR2
while :; do
	exec 3<&0
	(exec 0<&3 3<&-; exec "$cgiroot/gitweb.cgi" "$@")&
	bg="$!"
	exec 3<&-
	while :; do
		ec=0
		wait "$bg" || ec="$?"
		kill -0 "$bg" 2>/dev/null || break
		# was a signal to self that's now been forwarded
		# go around the loop again
	done
	bg=
	[ "${ec:-0}" = "0" ] || break
	# was a gitweb "garbage collection" exit
	# go around the loop again and restart gitweb
done
trap - HUP INT QUIT ABRT PIPE ALRM TERM USR1 USR2
if [ "${ec:-0}" -gt 128 ] && [ "${ec:-0}" -lt 160 ]; then
	# self terminate
	kill -$(($ec-128)) $$
fi
exit "$ec"

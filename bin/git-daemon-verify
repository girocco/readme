#!/bin/sh
#
# Abort any fetch early if the fetch is invalid or times out.
# This avoids unnecessary traffic and unpacked object pollution.
#
# This script is called for fetch, archive and push.
# Push requests are outright rejected and so are paths starting with ~.

set -e

. @basedir@/shlib.sh

unset GIT_USER_AGENT
unset GIT_HTTP_USER_AGENT
if [ -n "$defined_cfg_git_server_ua" ]; then
        GIT_USER_AGENT="$cfg_git_server_ua"
        export GIT_USER_AGENT
fi

[ -z "$GIT_DAEMON_BIN" ] || cfg_git_daemon_bin="$GIT_DAEMON_BIN"
[ -n "$cfg_git_daemon_bin" ] ||
	cfg_git_daemon_bin="$var_git_exec_path/git-daemon"

fromip=
logmsg()
{
	[ "${cfg_suppress_git_ssh_logging:-0}" = "0" ] || return 0
	_msg="$(LC_ALL=C tr -c '\040-\176' '[?*]' <<EOT
$fromip$*
EOT
	)" && _msg="${_msg%[?]}" &&
	logger -t "${0##*/}[$$]" <<EOT
$_msg
EOT
}

errormsg()
{
	_l="$*"
	printf '%04xERR %s' $(( 8 + ${#_l} )) "$_l"
}

invalbaderr()
{
	errormsg "invalid or incomplete request"
}

invalerr()
{
	errormsg "invalid or unsupported request"
}

denied()
{
	errormsg "access denied or no such repository"
}

internalerr()
{
	printf '%s\n' "git-daemon-verify: $*" >&2
	errormsg "internal server error"
}

# A quick sanity check
if [ -z "$cfg_git_daemon_bin" ] || ! [ -x "$cfg_git_daemon_bin" ]; then
	internalerr "bad cfg_git_daemon_bin: $cfg_git_daemon_bin"
	exit 1
fi
case "$cfg_reporoot" in /?*) :;; *)
	internalerr "bad reporoot: $cfg_reporoot"
	exit 1
esac

PATH="$(dirname "$cfg_git_daemon_bin"):$PATH"
export PATH

if ! request="$("$cfg_basedir/bin/peek_packet")" || [ -z "$request" ]; then
	invalbaderr
	exit 1
fi

_IFS="$IFS"
IFS='
'
set -- $request
IFS="$_IFS"
[ $# -ge 1 ] || { invalidbaderr; exit 1; }
request="$1"
shift

# Extract host and port now
REMOTE_ADDR=
REMOTE_PORT=
SERVER_ADDR=
SERVER_PORT=
[ "${hnam+set}" != "set" ] || unset hnam
[ "${pnum+set}" != "set" ] || unset pnum
[ "${hostport+set}" != "set" ] || unset hostport
for extra in "$@"; do
	case "$extra" in
	"host="*) hnam="${extra#host=}";;
	"port="*) pnum="${extra#port=}";;
	"server_addr="*) SERVER_ADDR="${extra#server_addr=}";;
	"server_port="*) SERVER_PORT="${extra#server_port=}";;
	"remote_addr="*) REMOTE_ADDR="${extra#remote_addr=}";;
	"remote_port="*) REMOTE_PORT="${extra#remote_port=}";;
	esac
done
# Make nice hostport variable for later use
fromip="$REMOTE_ADDR"
fromip="${fromip:+$fromip }"
if [ "${hnam+set}" = "set" ]; then
	case "$hnam" in
	*":"*)	hostport="[$hnam]";;
	*)	hostport="$hnam";;
	esac
	[ "${pnum+set}" != "set" ] || hostport="$hostport:$pnum"
fi

# Validate the host name if requested
if [ -z "$cfg_git_daemon_any_host" ] && [ -n "$cfg_git_daemon_host_list" ]; then
	case " $cfg_git_daemon_host_list " in *" $hnam "*);;*)
		logmsg "denied ${request#git-}${hostport+ host=$hostport}"
		denied
		exit 1
	esac
fi

# The request should look like one of the following
#
#   git-upload-pack /dir
#   git-upload-pack ~name/dir
#   git-upload-archive /dir
#   git-upload-archive ~name/dir
#   git-receive-pack /dir
#   git-receive-pack ~name/dir
#
# Where the '~' forms are relative to a user's home directory.
# A trailing '/' is optional as well as a final '.git'.
# git-receive-pack and paths starting with '~' are rejected outright.

type=
dir=
case "$request" in
	"git-upload-pack "*)    type='upload-pack';    dir="${request#git-upload-pack }";;
	"git-upload-archive "*) type='upload-archive'; dir="${request#git-upload-archive }";;
	"git-receive-pack "*)	type='receive-pack';   dir="${request#git-receive-pack }";;
	*)
		invalerr
		exit 1
esac
if [ "$type" = 'receive-pack' ]; then
	logmsg "denied $type $dir${hostport+ host=$hostport}"
	invalerr
	exit 1
fi
case "$dir" in /*) :;; *)
	logmsg "denied $type $dir${hostport+ host=$hostport}"
	invalerr
	exit 1
esac

# remove extraneous '/' chars
proj="${dir#/}"
proj="${proj%/}"
# add a missing .git
case "$proj" in
	*.git) :;;
	*)
		proj="$proj.git"
esac

# Reject any project names that start with _ or contain ..
case "$proj" in _*|*..*)
	logmsg "denied $type $dir${hostport+ host=$hostport}"
	denied
	exit 1
esac

odir="$dir"
reporoot="$cfg_reporoot"
dir="$reporoot/$proj"

# Valid project names never end in .git (we add that automagically), so a valid
# fork can never have .git at the end of any path component except the last.
# We check this to avoid a situation where a certain collection of pushed refs
# could be mistaken for a GIT_DIR.  Git would ultimately complain, but some
# undesirable things could happen along the way.

# Remove the leading $reporoot and trailing .git to get a test string
testpath="${dir#$reporoot/}"
testpath="${testpath%.git}"
case "$testpath/" in *.[Gg][Ii][Tt]/*|_*)
	logmsg "denied $type $odir${hostport+ host=$hostport}"
	denied
	exit 1
esac

if ! [ -d "$dir" ] || ! [ -f "$dir/HEAD" ] || ! [ -d "$dir/objects" ]; then
	logmsg "denied $type $odir${hostport+ host=$hostport}"
	denied
	exit 1
fi

[ -z "$var_upload_window" ] || [ "$type" != "upload-pack" ] ||
	git_add_config "pack.window=$var_upload_window"

if [ "${cfg_fetch_stash_refs:-0}" = "0" ]; then
	git_add_config "uploadpack.hiderefs=refs/stash"
	git_add_config "uploadpack.hiderefs=refs/tgstash"
fi

logmsg "accepted $type $odir${hostport+ host=$hostport}"
exec "$cfg_git_daemon_bin" --inetd --verbose --export-all --enable=upload-archive --base-path="$cfg_reporoot"
internalerr "exec failed: $cfg_git_daemon_bin"
exit 1

#!/bin/sh

# snapshot.cgi -- throttle snapshot requests
# Copyright (C) 2015,2017 Kyle J. McKay.  All rights reserved.
# License GPLv2+: GNU GPL version 2 or later.
# www.gnu.org/licenses/gpl-2.0.html
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.

set -e

. @basedir@/shlib.sh

# Set to non-empty to throttle if the initial throttle service connect fails
throttle_on_connect_fail=1

# Supplemental message to be included in the throttle result
throttle_msg=\
'Ravenous roving robots are probably greedily chowing down on our services
right now.

We are valiantly trying to fight them off to improve service availability.'

hdrout()
{
	_kw="$1"
	shift
	printf '%s: %s\r\n' "$_kw" "$*"
}

errorhdrsct()
{
	_ct="$1"; shift
	printf '%s\r\n' "Status: $1 $2"
	printf '%s\r\n' "Expires: Fri, 01 Jan 1980 00:00:00 GMT"
	printf '%s\r\n' "Pragma: no-cache"
	printf '%s\r\n' "Cache-Control: no-cache,max-age=0,must-revalidate"
	[ -z "$3" ] || printf '%s\r\n' "$3"
	printf '%s\r\n' "Content-Type: $_ct"
	printf '\r\n'
}

errorhdrs()
{
	errorhdrsct 'text/plain; charset=utf-8; format=fixed' "$@"
}

msglines()
{
	[ $# -le 0 ] || printf '%s\n' "$@"
}

methodnotallowed()
{
	errorhdrs 405 "Method Not Allowed" "Allow: GET"
	[ $# -gt 0 ] || set -- "Method Not Allowed"
	msglines "$@"
	exit 0
}

forbidden()
{
	errorhdrs 403 Forbidden
	[ $# -gt 0 ] || set -- "Forbidden"
	msglines "$@"
	exit 0
}

notfound()
{
	errorhdrs 404 "Not Found"
	[ $# -gt 0 ] || set -- "Not Found"
	msglines "$@"
	exit 0
}

# Snapshots are too expensive to allow HEAD
[ "$REQUEST_METHOD" = "GET" ] || methodnotallowed

# The project must be valid
suffix="${PATH_INFO#*.git/}"
project="${PATH_INFO%/"$suffix"}"
project="${project#/}"
[ -n "$project" ] || forbidden
case "$suffix" in snapshot|snapshot/*) :;; *) forbidden; esac
suffix="${suffix#snapshot}"
suffix="${suffix#/}"

# Perform some basic sanity checking
if [ -z "$suffix" ]; then
	# Must have an "h=" argument
	case "&$QUERY_STRING&" in *[\&\;]"h="[!\&\;]*) :;; *) forbidden; esac
fi
case "$suffix" in [!A-Za-z0-9_]*) forbidden; esac
case "/$project/" in *"/../"*|*"/./"*|*"/_"*|*"//"*) forbidden; esac
is_git_dir "$cfg_reporoot/$project" || notfound

# Give the 'bots indigestion
sleep 5

# Attempt to trigger a SIGPIPE if the connection has already been closed
hdrout "X-Project" "${project%.git}"
sleep 1
hdrout "X-Snapshot" "${suffix:-$QUERY_STRING}"
sleep 1

# Off to the races
projname="${project%.git}"
"$cfg_basedir/bin/throttle" ${throttle_on_connect_fail:+-t} -c snapshot \
	-d "$projname" -m "$throttle_msg" "$cfg_cgiroot/gitweb.cgi"
exit 0

#!/usr/bin/perl

# usercert.cgi -- user push authentication certificate retrieval
# Copyright (c) 2013 Kyle J. McKay.  All rights reserved.

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

# Version 1.2

use strict;
use warnings;

use lib "__BASEDIR__";
use Girocco::CGI;
use Girocco::Config;
use Girocco::User;
use Girocco::Util;
use Girocco::SSHUtil;

sub notfound
{
	my $mesg = shift || "<p>404 - Not found</p>";
	my $gcgi = Girocco::CGI->new('User HTTPS Push Authentication Certificate');
	print $mesg;
	exit;
}

sub mtime
{
	my $fname = shift;
	my (undef,undef,undef,undef,undef,undef,undef,undef,undef,$mtime) = stat $fname;
	return $mtime;
}

sub readfile
{
	my $fname = shift;
	local $/;
	open(FILE, '<', $fname) or return undef;
	my $contents = <FILE>;
	close(FILE);
	return $contents;
}

notfound unless $Girocco::Config::clientcert && $Girocco::Config::clientkey;
notfound unless -f $Girocco::Config::clientcert && -f $Girocco::Config::clientkey;
notfound unless -r $Girocco::Config::clientcert && -r $Girocco::Config::clientkey;

my $cgi = CGI->new;

notfound if $cgi->request_method() ne 'GET' && $cgi->request_method() ne 'HEAD';
notfound "<p>Go away, bot.</p>" if $cgi->param('mail');

my $nick = $Girocco::Config::nickname;

my $name = $cgi->param('name');
$name =~ s/^\s*(.*?)\s*$/$1/ if $name;
my $line = $cgi->param('line');
$line =~ s/^\s*(.*?)\s*$/$1/ if $line;
my $view = $cgi->param('view');
$view =~ s/^\s*(.*?)\s*$/$1/ if $view;

notfound if $cgi->path_info() && ($name || $line);

if ($cgi->path_info() =~ m,/([^/]+)/([^/]+)/([^/]+)$,) {
	my ($tn,$tl,$tp) = ($1,$2,$3);
	if ("${nick}_${tn}_user_$tl.pem" eq $tp) {
		$name = $tn;
		$line = $tl;
	}
}

notfound unless $name && $line && $line =~ /^[1-9][0-9]*$/;
$line += 0;

notfound unless Girocco::User::does_exist($name, 1);
my $user = Girocco::User->load($name) or notfound;

my @keys = split(/\r?\n/, $user->{keys});
$line <= @keys or notfound;
my ($type, $bits, $fingerprint, $comment) = sshpub_validate($keys[$line-1]);
$type && $type eq 'ssh-rsa' or notfound;

my $sshkeys = jailed_file($user->_sshkey_path);
my $kname = "${nick}_${name}_user_$line.pem";
my $sshcert = jailed_file("/etc/sshcerts/$kname");
my $sshkeysmtime = mtime($sshkeys);
notfound unless $sshkeysmtime;
my $sshcertmtime = mtime($sshcert);
if (!$sshcertmtime || $sshkeysmtime >= $sshcertmtime) {
	unlink $sshcert;
	my @args = (
		"$Girocco::Config::basedir/bin/CACreateCert", "--quiet", "--client",
		"--cert", $Girocco::Config::clientcert,
		"--key", $Girocco::Config::clientkey,
		"--out", $sshcert
		);
	push(@args, "--suffix", $Girocco::Config::clientcertsuffix)
		if $Girocco::Config::clientcertsuffix;
	push(@args, "--dnq", $user->{uuid}, $name);
	open(PIPE, "|-", @args) or notfound;
	print PIPE $keys[$line-1], "\n";
	close(PIPE) or notfound;
	chmod(0664, $sshcert);
}
notfound unless -r $sshcert;
my $certdata = readfile($sshcert);
notfound unless $certdata;
my $isviewonly = 0;
$isviewonly = 0 + $view if $view && $view =~ /^[01]$/;
if ($isviewonly) {
	print "Content-Type: text/plain; charset=utf-8; format=fixed\r\n"
} else {
	print "Content-Type: application/octet-stream\r\n";
	print "Content-Disposition: attachment; filename=\"$kname\"\r\n";
}
print "\r\n";
print $certdata unless $cgi->request_method() eq 'HEAD';

exit 0;

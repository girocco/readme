#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# GPLv2

use strict;
use warnings;

use lib "__BASEDIR__";
use Girocco::CGI;
use Girocco::Config;
use Girocco::Project;
use Girocco::Util;

my $gcgi = Girocco::CGI->new('Project Mirroring');
my $cgi = $gcgi->cgi;

my $name = $cgi->param('name');
$name =~ s#\.git$## if $name; #

unless (defined $name) {
	print "<p>I need the project name as an argument now.</p>\n";
	exit;
}

if (!Girocco::Project::does_exist($name,1) && !Girocco::Project::valid_name($name)) {
	print "<p>Invalid project name. Go away, sorcerer.</p>\n";
	exit;
}

if (!Girocco::Project::does_exist($name,1)) {
	print "<p>Sorry but the project $name does not exist. Now, how did you <em>get</em> here?!</p>\n";
	exit;
}

if (my $romsg=check_readonly(1)) {
	print "<p>$romsg</p>\n";
	exit;
}

my $proj = Girocco::Project->load($name);
if (!$proj) {
	print "<p>not found project $name, that's really weird!</p>\n";
	exit;
}
my $escname = $name;
$escname =~ s/[+]/%2B/g;

if (!$proj->{mirror}) {
	print "<p>This project is not a mirror to be cloned.</p>\n";
	exit;
}


$| = 1;

if (!$proj->{clone_logged} or $proj->{clone_failed}) {
	# Kick off the clone since it is not running yet
	print "<p>Initiated mirroring of ".$proj->{url}." to $Girocco::Config::name project ".
		"<a href=\"@{[url_path($Girocco::Config::gitweburl)]}/$name.git\">$name</a>.git:</p>\n";

} elsif ($proj->{clone_in_progress}) {
	print "<p>Mirroring of ".$proj->{url}." to $Girocco::Config::name project ".
		"<a href=\"@{[url_path($Girocco::Config::gitweburl)]}/$name.git\">$name</a>.git in progress:</p>\n";
} else {
	print "<p>Mirroring of ".$proj->{url}." to $Girocco::Config::name project ".
		"<a href=\"@{[url_path($Girocco::Config::gitweburl)]}/$name.git\">$name</a>.git completed:</p>\n";
}

my $retries = 7;
for (;;) {
	open LOG, '<', $proj->_clonelog_path() and last;
	if ($retries--) {
		sleep 1;
	} else {
		print "<p>Mirroring has not yet started, there may be a clone backlog.</p>\n";
		print "<p>Please <a href=\"@{[url_path($Girocco::Config::webadmurl)]}/mirrorproj.cgi?name=$escname\">reload this page</a> again later.</p>\n";
		exit 0;
	}
}

print "<pre>\n";
tailf: for (;;) {
	my $curpos;
	for ($curpos = tell(LOG); <LOG>; $curpos = tell(LOG)) {
		chomp;
		$_ eq '@OVER@' and last tailf;
		print "$_\n";
	}
	sleep 1;
	seek(LOG, $curpos, 0);  # seek to where we had been
}
print "</pre>\n";
close LOG;

$proj = Girocco::Project->load($name);
$proj or die "not found project $name on second load, that's _REALLY_ weird!";

if ($proj->{clone_failed}) {
	print <<EOT;
<p><strong>Mirroring failed!</strong> Please <a
href="@{[url_path($Girocco::Config::webadmurl)]}/editproj.cgi?name=$escname"
>revisit the project settings</a>.</p>
EOT
}

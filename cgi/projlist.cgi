#!/usr/bin/perl

# projlist.cgi -- support for viewing a single owner's projects
# Copyright (c) 2013 Kyle J. McKay.  All rights reserved.
# Portions (c) Petr Baudis <pasky@suse.cz> and (c) Jan Krueger <jk@jk.gs>
# License GPLv2+: GNU GPL version 2 or later.
# www.gnu.org/licenses/gpl-2.0.html
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.

use strict;
use warnings;

use lib "__BASEDIR__";
use Girocco::CGI;
use Girocco::Config;
use Girocco::Project;
use Girocco::Util;
use Digest::MD5 qw(md5_hex);
binmode STDOUT, ':utf8';

my $style = <<'EOT';
<style type="text/css">
span.obfu:before {
	content: "\40";
}
</style>
EOT
my $gcgi = Girocco::CGI->new('Project List', undef, $style);
my $cgi = $gcgi->cgi;

my $name = $gcgi->wparam('name');

unless (defined $name) {
	print "<p>I need the owner name as an argument now.</p>\n";
	exit;
}

if ($cgi->param('mail')) {
	print "<p>Go away, bot.</p>";
	exit;
}

if (!valid_email($name) && $name !~ /^[0-9a-fA-F]{32}$/) {
	print "<p>Invalid owner name. Go away, sorcerer.</p>\n";
	exit;
}

if ($name =~ /^[0-9a-fA-F]{32}$/) {
	$name =~ tr/A-F/a-f/;
} else {
	$name = md5_hex(lc($name));
}
my $displayname;
my @projects = filedb_grep($Girocco::Config::projlist_cache_dir.'/gitproj.list',
	sub {
		chomp;
		my ($proj, $hash, $owner) = split(/ /, $_, 3);
		if ($hash eq $name) {
			$displayname = $owner unless $displayname;
			$proj;
		}
	}
);
if (!@projects) {
	print "<p>Invalid owner name. Go away, sorcerer.</p>\n";
	exit;
}

$displayname =~ s,@,<span class="obfu"><span class="none"> </span></span>,;
my $projectlist = projects_html_list({emptyok=>1, sizecol=>1, typecol=>1, changed=>1}, @projects);
print <<EOT;
<p>The owner '$displayname' has the following projects registered at this site:</p>
$projectlist
EOT

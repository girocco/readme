#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# GPLv2

use strict;
use warnings;

use lib "__BASEDIR__";
use Girocco::CGI;
use Girocco::Config;
use Girocco::Project;
use Girocco::Util;
use POSIX qw(strftime);

my $script = <<'EOT';
<script type="text/javascript">
// <![CDATA[
function adduseritem(elem)
{
	var inp = document.createElement('input');
	inp.type = 'text';
	inp.name = 'user';
	var li = document.createElement('li');
	li.appendChild(document.createTextNode('Add user: '));
	li.appendChild(inp);
	elem.parentNode.insertBefore(li, elem);
}
// ]]>
</script>
EOT
my $gcgi = Girocco::CGI->new('Project Settings', undef, $script);
my $cgi = $gcgi->cgi;

my $name = $cgi->param('name');
$name =~ s#\.git$## if $name; #

unless (defined $name) {
	print "<p>I need the project name as an argument now.</p>\n";
	exit;
}

if (!Girocco::Project::does_exist($name,1) && !Girocco::Project::valid_name($name)) {
	print "<p>Invalid project name. Go away, sorcerer.</p>\n";
	exit;
}

if (!Girocco::Project::does_exist($name,1)) {
	print "<p>Sorry but the project $name does not exist. Now, how did you <em>get</em> here?!</p>\n";
	exit;
}

if (my $romsg=check_readonly(1)) {
	print "<p>$romsg</p>\n";
	exit;
}

my $proj = Girocco::Project->load($name);
if (!$proj) {
	print "<p>not found project $name, that's really weird!</p>\n";
	exit;
}
push(@Girocco::Config::project_fields, "notifycia")
	if $proj->{notifycia} && !grep(/notifycia/, @Girocco::Config::project_fields);
my $escname = $name;
$escname =~ s/[+]/%2B/g;

my $tzoffset = $cgi->param('tzoffset') || 0;
$tzoffset =~ /^[-+]?\d{1,5}$/ or $tzoffset = 0;
$tzoffset = 0 + $tzoffset;
$tzoffset >= -43200 && $tzoffset <= 43200 or $tzoffset = 0;

sub format_epoch_ts {
	my $es = shift;
	defined($es) or $es = time();
	$es += $tzoffset;
	my $str = strftime("%Y-%m-%d %H:%M:%S ", (gmtime($es))[0..5], -1, -1, -1);
	if ($tzoffset) {
		my $moff = int(abs($tzoffset) / 60);
		$str .= sprintf("%s%02d%02d",
			($tzoffset >= 0 ? "+" : "-"),
			int($moff / 60),
			$moff - 60 * int($moff / 60));
	} else {
		$str .= "UTC";
	}
	return $str;
}

my $y0 = $cgi->param('y0') || '';
my $tok = $cgi->param('token') || '';
my $protok = 0;
if (($y0 eq 'Update' || $y0 eq 'View All' || $y0 eq 'Restart Mirroring') && $cgi->request_method eq 'POST') {{
	my $ts = "<span class=\"timestamp\">" . format_epoch_ts() . "</span>";
	$gcgi->err_prelude("<p class=\"failed\">Project update failed at $ts.</p>\n") unless $y0 eq 'View All';
	# Check for token validity
	if (!check_timed_token($tok, "projedit", $proj->{name}, $Girocco::Config::project_edit_timeout)) {
		$gcgi->err("Session has timed out or is invalid, please try again.");
	}
	# submitted, let's see
	my $fillok = $proj->cgi_fill($gcgi, 1);
	my $authok = $proj->authenticate($gcgi);
	$authok and $protok = 1;
	$y0 eq 'View All' and last;
	if (!$gcgi->err_check && $authok) {
		$proj->update or last;
		print "<p class=\"updated\">Project successfully updated at $ts.</p>\n";
		if ($proj->{clone_failed}) {
			$proj->clone;
			print "<p>Please <a href=\"@{[url_path($Girocco::Config::webadmurl)]}".
				"/mirrorproj.cgi?name=$escname\">pass onwards</a>.</p>\n";
			print "<script language=\"javascript\">document.location=".
				"'@{[url_path($Girocco::Config::webadmurl)]}/mirrorproj.cgi?name=$escname'</script>\n";
			exit;
		}
	}
}}

# $proj may be insane now but that's actually good for us since we'll let the
# user fix the invalid values he or she entered
my %h = $proj->form_defaults;

print <<EOT;
<p>Here you may adjust the settings of project $h{name}. Go wild.
EOT
if ($proj->{mirror}) {
	print <<EOT;
Since this is a mirrored project, you may opt to remove it from the site as well.
Just <a href="@{[url_path($Girocco::Config::webadmurl)]}/delproj.cgi?name=$escname">remove it</a>.</p>
EOT
} else {
	if ($proj->is_empty) {
		print <<EOT;
Since this is an empty project, you may opt to remove it from the site as well.
Just <a href="@{[url_path($Girocco::Config::webadmurl)]}/delproj.cgi?name=$escname">remove it</a>.</p>
EOT
	} else {
		print <<EOT;
You may
<a href="@{[url_path($Girocco::Config::webadmurl)]}/delproj.cgi?name=$escname">request an authorization code</a> in order
to remove this project from the site.</p>
EOT
	}
	print <<EOT;
<p>Use the <b>+</b> button to enable access for more than a single user at a time.</p>
EOT
}

my $button_label = $proj->{clone_failed} ? 'Restart Mirroring' : 'Update';
my $showstatusopt = $proj->{mirror} && !$proj->{clone_failed} && !$proj->{clone_in_progress};
my $statuschecked = $proj->{statusupdates} ? 'checked="checked"' : '';
my $tokauth = get_token_field("projedit", $h{name}, $Girocco::Config::project_edit_timeout);
$tokauth and $tokauth = "\n".$tokauth;

print <<EOT;
<form method="post" action="@{[url_path($Girocco::Config::webadmurl)]}/editproj.cgi">$tokauth
<input type="hidden" name="tzoffset" value="0" />
<table class="form">
<tr><td class="formlabel">Project name:</td><td class="formdata"><a
	href="@{[url_path($Girocco::Config::gitweburl)]}/$h{name}.git">$h{name}</a>.git
	<input type="hidden" name="name" value="$h{name}" /></td></tr>
EOT
if ($Girocco::Config::project_passwords) {
	my $vab = '';
	$protok || !%{$Girocco::Config::protect_fields} or
		$vab = '&#160;&#160;<input type="submit" name="y0" value="View All" />';
	print <<EOT;
<tr><td class="formlabel"><strong>Admin password:</strong></td><td>
	<input type="password" name="cpwd" /> <sup class="sup"><span><a
	href="@{[url_path($Girocco::Config::webadmurl)]}/pwproj.cgi?name=$escname"
	class="ctxaction">(forgot password?)</a></span></sup>$vab</td></tr>
<tr><td class="formlabel">New admin password (twice):<br />
	<em>(leave empty to keep it the same)</em></td><td>
	<input type="password" name="pwd" /><br /><input type="password" name="pwd2" /><br />
	</td></tr>
EOT
}
if ($Girocco::Config::project_owners eq 'email') {
	print <<EOT;
<tr><td class="formlabel">E-mail contact:</td><td><input type="text" name="email" value="$h{email}" /></td></tr>
EOT
}

if ($proj->{mirror}) {
	print "<tr><td class=\"formlabel\">Repository URL:</td><td><input type=\"text\" name=\"url\" value=\"$h{url}\" /></td></tr>\n";
	print '<tr><td class="formlabel">Mirror refs:</td><td class="formdatatd">'.
			'<label title="Unchecking this will mirror the entire refs namespace which is usually unnecessary.  '.
			'Non-git sources always mirror the entire refs namespace regardless of this setting.">'.
			'<input type="checkbox" name="cleanmirror" value="1" '.($h{'cleanmirror'} ? 'checked="checked" ' : '').
			'style="vertical-align:middle" /><span style="vertical-align:middle; margin-left:0.5ex">'.
			'Only mirror <code>refs/heads/*</code>, <code>refs/tags/*</code> and <code>refs/notes/*</code></span></label></td></tr>'."\n"
		if grep(/cleanmirror/, @Girocco::Config::project_fields);
} else {
	print <<EOT;
<tr><td class="formlabel" style="vertical-align:middle">Users:</td><td>
<ul>
EOT
	$Girocco::Config::manage_users and print "<p>Only <a href=\"".
		"@{[url_path($Girocco::Config::webadmurl)]}/reguser.cgi\">registered users</a> may push.</p>";
	if ($Girocco::Config::mob and not grep { $_ eq $Girocco::Config::mob } @{$h{users}}) {
		print "<p><em>(Please consider adding the <tt>$Girocco::Config::mob</tt> user.\n";
		print "<sup class=\"sup\"><span><a href=\"@{[url_path($Girocco::Config::htmlurl)]}/mob.html\">(learn more)</a></span></sup>)\n";
		print "</em></p>\n";
	}
	foreach my $user (@{$h{users}}) {
		my $mlm = '';
		$mlm = " <sup class=\"sup\"><span><a href=\"@{[url_path($Girocco::Config::htmlurl)]}/mob.html\">(learn more)</a></span></sup>"
			if $Girocco::Config::mob && $user eq $Girocco::Config::mob;
		print "<li><input type=\"checkbox\" name=\"user\" value=\"$user\" checked=\"1\" /> $user$mlm</li>\n";
	}
	print <<EOT;
<li>Add user: <input type="text" name="user" /></li>
<button type="button" onclick="adduseritem(this)"><b>+</b></button>
</ul>
</td></tr>
EOT
}

print '<tr><td class="formlabel">Default branch:</td><td class="formdatatd"><select size="1" name="HEAD">';
for ($proj->get_heads) {
	my $selected = $proj->{HEAD} eq $_ ? ' selected="selected"' : '';
	print "<option$selected>".Girocco::CGI::html_esc($_)."</option>";
}
print '</select></td></tr>
';

print '<tr><td class="formlabel">Tags (select to delete):</td><td>';
print '<select size="6" name="tags" multiple="multiple">';
for ($proj->get_ctag_names) {
	print '<option>'.Girocco::CGI::html_esc($_).'</option>';
}
print '</select></td></tr>
';


$gcgi->print_form_fields($Girocco::Project::metadata_fields,
			 $protok?{}:$Girocco::Config::protect_fields,
			 \%h, @Girocco::Config::project_fields);
print <<EOT if $showstatusopt;
<tr><td class="formlabel">Enable status update emails:</td>
<td class="formdatatd"
><input type="hidden" name="setstatusupdates" value="1"
/><input type="checkbox" name="statusupdates" value="1" $statuschecked /></td></tr>
EOT
print <<EOT;
<tr><td class="formlabel"></td><td><input type="submit" name="y0" value="$button_label" /></td></tr>
</table>
</form>
<script type="text/javascript">
// <![CDATA[
(function () {
	var tzoffset = (new Date).getTimezoneOffset() * -60;
	var form0 = document.forms[0];
	if (form0 && form0.tzoffset) {
		form0.tzoffset.value = tzoffset;
	}
})();
// ]]>
</script>
EOT
